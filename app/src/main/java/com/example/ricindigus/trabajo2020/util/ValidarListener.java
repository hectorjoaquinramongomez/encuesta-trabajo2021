package com.example.ricindigus.trabajo2020.util;

public interface ValidarListener {
    String validar(String valor);
}
