package com.example.ricindigus.trabajo2020.fragments.modulo4;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo4;
import com.example.ricindigus.trabajo2020.modelo.pojos.Residente;
import com.example.ricindigus.trabajo2020.modelo.pojos.Ubigeo;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP007P011 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;
    Ubigeo ubigeo;

    //No se muy bien para que lo llama?
    Spinner informanteSpinner;

    LinearLayout layout_d_p007_p008,layout_d_p007_p008_subpregunta,layout_d_p009,layout_d_p010,layout_d_p011;
    RadioGroup rg_d_p007;
    AutoCompleteTextView autotv_d_p008;
    TextView tv_d_p008_distrito;
    TextView tv_d_p008_provincia;
    TextView tv_d_p008_departamento;
    RadioGroup rg_d_P009;
    RadioGroup rg_d_P010;
    RadioGroup rg_d_P011;


    //nuevos
    private String  inf_300;
    private String  p307;
    private String  p308a;
    private String  p308a_cod;
    private String  p308b;
    private String  p308b_cod;
    private String  p308c;
    private String  p308c_cod;
    private String  p308d;
    private String  p308d_cod;
    private String  p309;
    private String  p310;
    private String  p311;

    public FragmentP007P011() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP007P011(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
        Data data = new Data(context);
        data.open();
        Residente residente = data.getResidente(idEncuestado);
        idHogar = residente.getId_hogar();
        idVivienda = residente.getId_vivienda();
        idInformante = "";
        data.close();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_d_p007_p011, container, false);

        //nuevos
        layout_d_p007_p008 = rootView.findViewById(R.id.layout_d_p007_p008);
        layout_d_p007_p008_subpregunta = rootView.findViewById(R.id.mod5_507_layout_subpregunta);
        layout_d_p009 = rootView.findViewById(R.id.layout_d_p009);
        layout_d_p010 = rootView.findViewById(R.id.layout_d_p010);
        layout_d_p011 = rootView.findViewById(R.id.layout_d_p011);

        rg_d_p007 = rootView.findViewById(R.id.seccion_d_p007_radiogroup);
        autotv_d_p008 = rootView.findViewById(R.id.mod5_507_autocompletetextview);
        tv_d_p008_distrito = rootView.findViewById(R.id.seccion_d_p007_txtDistrito);
        tv_d_p008_provincia = rootView.findViewById(R.id.seccion_d_p007_txtProvincia);
        tv_d_p008_departamento = rootView.findViewById(R.id.seccion_d_p007_txtDepartamento);
        rg_d_P009 = rootView.findViewById(R.id.radiogroup_d_P009);
        rg_d_P010 = rootView.findViewById(R.id.radiogroup_d_P010);
        rg_d_P011 = rootView.findViewById(R.id.radiogroup_d_P011);



        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //inico nuevos

        rg_d_p007.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int pos = radioGroup.indexOfChild(rg_d_p007.findViewById(rg_d_p007.getCheckedRadioButtonId()));
                if (pos == 2){
                    layout_d_p007_p008_subpregunta.setVisibility(View.VISIBLE);
                    layout_d_p009.setVisibility(View.VISIBLE);
                    layout_d_p010.setVisibility(View.GONE);
                    layout_d_p011.setVisibility(View.GONE);
                }else if (pos==1 || pos==3){
                    layout_d_p007_p008_subpregunta.setVisibility(View.GONE);
                    layout_d_p009.setVisibility(View.VISIBLE);
                    layout_d_p010.setVisibility(View.GONE);
                    layout_d_p011.setVisibility(View.GONE);
                    tv_d_p008_distrito.setText("");
                    tv_d_p008_provincia.setText("");
                    tv_d_p008_departamento.setText("");
                }
            }
        });

        rg_d_P009.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int pos = radioGroup.indexOfChild(rg_d_P009.findViewById(rg_d_P009.getCheckedRadioButtonId()));
                if (pos == 1){
                    layout_d_p010.setVisibility(View.GONE);
                    layout_d_p011.setVisibility(View.GONE);
                }else if (pos==2){
                    layout_d_p010.setVisibility(View.VISIBLE);
                    layout_d_p011.setVisibility(View.GONE);
                }
            }
        });


        rg_d_P010.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int pos = radioGroup.indexOfChild(rg_d_P010.findViewById(rg_d_P010.getCheckedRadioButtonId()));
                if (pos == 1) {
                    layout_d_p011.setVisibility(View.GONE);
                }else if (pos==2){
                    layout_d_p011.setVisibility(View.VISIBLE);
                }
            }
        });

        rg_d_P011.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int pos = radioGroup.indexOfChild(rg_d_P011.findViewById(rg_d_P011.getCheckedRadioButtonId()));
                if (pos == 1) {
                    mostrarMensaje("Si P311 = 1 Entonces pase a P314A");
                }else if (pos==2){
                    mostrarMensaje("Si P311 = 2 Entonces pase a P312_1");
                }
            }
        });


        //fin de los nuevos

        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo4_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_d_p307,p307);
        contentValues.put(SQLConstantes.seccion_d_p308a,p308a);
        contentValues.put(SQLConstantes.seccion_d_p308a_cod,p308a_cod);
        contentValues.put(SQLConstantes.seccion_d_p308b,p308b);
        contentValues.put(SQLConstantes.seccion_d_p308b_cod,p308b_cod);
        contentValues.put(SQLConstantes.seccion_d_p308c,p308c);
        contentValues.put(SQLConstantes.seccion_d_p308c_cod,p308c_cod);
        contentValues.put(SQLConstantes.seccion_d_p308d,p308d);
        contentValues.put(SQLConstantes.seccion_d_p308d_cod,p308d_cod);
        contentValues.put(SQLConstantes.seccion_d_p309,p309);
        contentValues.put(SQLConstantes.seccion_d_p310,p310);
        contentValues.put(SQLConstantes.seccion_d_p311,p311);

        if(!data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo4 modulo4 = new Modulo4();
            modulo4.set_id(idEncuestado);
            modulo4.setIdHogar(idHogar);
            modulo4.setIdVivienda(idVivienda);
            data.insertarElemento(getNombreTabla(),modulo4.toValues());
        }
        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        //Ya valido y guardo correctamente el fragment, ahora actualizamos el valor de la cobertura del fragment a correcto(1)
        data.actualizarValor(SQLConstantes.tablacoberturafragments,SQLConstantes.cobertura_fragments_cp401p404,"1",idEncuestado);
        //verificamos la cobertura del capitulo y actualizamos su valor de cobertura.
        if (verificarCoberturaCapitulo()) data.actualizarValor(getNombreTabla(),SQLConstantes.modulo4_COB400,"1",idEncuestado);
        else data.actualizarValor(getNombreTabla(),SQLConstantes.modulo4_COB400,"0",idEncuestado);
        data.actualizarValor(SQLConstantes.tablaresidentes,SQLConstantes.residentes_encuestado_cobertura,"0",idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {


        //nuevos

        /*
         p307=rg_d_p007.indexOfChild(rg_d_p007.findViewById(rg_d_p007.getCheckedRadioButtonId()))+"";

        p308a_cod= ubigeo.getCod_departamento();
        p308b_cod=ubigeo.getCod_provincia();
        p308b= ubigeo.getNom_provincia();
        p308c_cod= ubigeo.getCod_distrito();
        p308c=ubigeo.getNom_distrito();
         */




        p309 = rg_d_P009.indexOfChild(rg_d_P009.findViewById(rg_d_P009.getCheckedRadioButtonId()))+"";

        p310 = rg_d_P010.indexOfChild(rg_d_P010.findViewById(rg_d_P010.getCheckedRadioButtonId()))+"";

        p311 = rg_d_P011.indexOfChild(rg_d_P011.findViewById(rg_d_P011.getCheckedRadioButtonId()))+"";



        //fin de nuevos

    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo4 modulo4 = data.getModulo4(idEncuestado);

            //nuevos
            if(!modulo4.getP309().equals("-1") && !modulo4.getP309().equals(""))((RadioButton)rg_d_P009.getChildAt(Integer.parseInt(modulo4.getP309()))).setChecked(true);

            if(!modulo4.getP310().equals("-1") && !modulo4.getP310().equals(""))((RadioButton)rg_d_P010.getChildAt(Integer.parseInt(modulo4.getP310()))).setChecked(true);

            if(!modulo4.getP311().equals("-1") && !modulo4.getP311().equals(""))((RadioButton)rg_d_P011.getChildAt(Integer.parseInt(modulo4.getP311()))).setChecked(true);
            //fin de nuevos
        }
        data.close();

    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();

        //nuevos

       // if (p309.equals("-1")){mostrarMensaje("PREGUNTA 309: DEBE MARCAR UNA OPCIÓN"); return false;}
       // if (p310.equals("-1")){mostrarMensaje("PREGUNTA 310: DEBE MARCAR UNA OPCIÓN"); return false;}
      //  if (p311.equals("-1")){mostrarMensaje("PREGUNTA 311: DEBE MARCAR UNA OPCIÓN"); return false;}

        //fin de nuevos


        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo4;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }
}
