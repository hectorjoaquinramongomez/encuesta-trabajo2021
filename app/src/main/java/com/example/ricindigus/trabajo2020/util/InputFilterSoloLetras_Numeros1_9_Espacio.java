package com.example.ricindigus.trabajo2020.util;

import android.text.InputFilter;
import android.text.Spanned;

public class InputFilterSoloLetras_Numeros1_9_Espacio implements InputFilter {
    @Override
    public CharSequence filter(CharSequence src, int start,
                               int end, Spanned dst, int dstart, int dend) {
        return src.toString().replaceAll("[^A-Za-z1-9 ]", "");
    }
}