package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo1V {
    private String _id;

    private String COB100A;
    //nuevos
   // private String  id;
    //private String id_vivienda;
   // private String numero_hogar;
    private String p101;
    private String p101_o;
    private String p102;
    private String p102_o;
    private String p103;
    private String p103_o;
    private String p103a;
    private String p104;
    private String p104_o;
    private String p105_1;
    private String p105_2;
    private String p105_3;
    private String p105_4;
    private String p105_5;
    private String p105_5_o;
    private String p105_6;
    private String p106_1;
    private String p106_2;
    private String p106_3;
    private String p106_4;
    private String p106_5;
    private String p106_6;
    private String p106_7;
    private String p106_8;
    private String p106_9;
    private String p106_10;
    private String p106_11;
    private String p106_11_o;
    private String p106_12;


    public Modulo1V() {
        _id="";
        COB100A = "2";
        //nuevos
       // id="";
        //id_vivienda="";
        //numero_hogar="";
        p101="";
        p101_o="";
        p102="";
        p102_o="";
        p103="";
        p103_o="";
        p103a="";
        p104="";
        p104_o="";
        p105_1="";
        p105_2="";
        p105_3="";
        p105_4="";
        p105_5="";
        p105_5_o="";
        p105_6="";
        p106_1="";
        p106_2="";
        p106_3="";
        p106_4="";
        p106_5="";
        p106_6="";
        p106_7="";
        p106_8="";
        p106_9="";
        p106_10="";
        p106_11="";
        p106_11_o="";
        p106_12="";


    }

    public String getCOB100A() {
        return COB100A;
    }

    public void setCOB100A(String COB100A) {
        this.COB100A = COB100A;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }


    public String getP101() {
        return p101;
    }

    public void setP101(String p101) {
        this.p101 = p101;
    }

    public String getP101_o() {
        return p101_o;
    }

    public void setP101_o(String p101_o) {
        this.p101_o = p101_o;
    }

    public String getP102() {
        return p102;
    }

    public void setP102(String p102) {
        this.p102 = p102;
    }

    public String getP102_o() {
        return p102_o;
    }

    public void setP102_o(String p102_o) {
        this.p102_o = p102_o;
    }

    public String getP103() {
        return p103;
    }

    public void setP103(String p103) {
        this.p103 = p103;
    }

    public String getP103_o() {
        return p103_o;
    }

    public void setP103_o(String p103_o) {
        this.p103_o = p103_o;
    }

    public String getP103a() {
        return p103a;
    }

    public void setP103a(String p103a) {
        this.p103a = p103a;
    }

    public String getP104() {
        return p104;
    }

    public void setP104(String p104) {
        this.p104 = p104;
    }

    public String getP104_o() {
        return p104_o;
    }

    public void setP104_o(String p104_o) {
        this.p104_o = p104_o;
    }

    public String getP105_1() {
        return p105_1;
    }

    public void setP105_1(String p105_1) {
        this.p105_1 = p105_1;
    }

    public String getP105_2() {
        return p105_2;
    }

    public void setP105_2(String p105_2) {
        this.p105_2 = p105_2;
    }

    public String getP105_3() {
        return p105_3;
    }

    public void setP105_3(String p105_3) {
        this.p105_3 = p105_3;
    }

    public String getP105_4() {
        return p105_4;
    }

    public void setP105_4(String p105_4) {
        this.p105_4 = p105_4;
    }

    public String getP105_5() {
        return p105_5;
    }

    public void setP105_5(String p105_5) {
        this.p105_5 = p105_5;
    }

    public String getP105_5_o() {
        return p105_5_o;
    }

    public void setP105_5_o(String p105_5_o) {
        this.p105_5_o = p105_5_o;
    }

    public String getP105_6() {
        return p105_6;
    }

    public void setP105_6(String p105_6) {
        this.p105_6 = p105_6;
    }

    public String getP106_1() {
        return p106_1;
    }

    public void setP106_1(String p106_1) {
        this.p106_1 = p106_1;
    }

    public String getP106_2() {
        return p106_2;
    }

    public void setP106_2(String p106_2) {
        this.p106_2 = p106_2;
    }

    public String getP106_3() {
        return p106_3;
    }

    public void setP106_3(String p106_3) {
        this.p106_3 = p106_3;
    }

    public String getP106_4() {
        return p106_4;
    }

    public void setP106_4(String p106_4) {
        this.p106_4 = p106_4;
    }

    public String getP106_5() {
        return p106_5;
    }

    public void setP106_5(String p106_5) {
        this.p106_5 = p106_5;
    }

    public String getP106_6() {
        return p106_6;
    }

    public void setP106_6(String p106_6) {
        this.p106_6 = p106_6;
    }

    public String getP106_7() {
        return p106_7;
    }

    public void setP106_7(String p106_7) {
        this.p106_7 = p106_7;
    }

    public String getP106_8() {
        return p106_8;
    }

    public void setP106_8(String p106_8) {
        this.p106_8 = p106_8;
    }

    public String getP106_9() {
        return p106_9;
    }

    public void setP106_9(String p106_9) {
        this.p106_9 = p106_9;
    }

    public String getP106_10() {
        return p106_10;
    }

    public void setP106_10(String p106_10) {
        this.p106_10 = p106_10;
    }

    public String getP106_11() {
        return p106_11;
    }

    public void setP106_11(String p106_11) {
        this.p106_11 = p106_11;
    }

    public String getP106_11_o() {
        return p106_11_o;
    }

    public void setP106_11_o(String p106_11_o) {
        this.p106_11_o = p106_11_o;
    }

    public String getP106_12() {
        return p106_12;
    }

    public void setP106_12(String p106_12) {
        this.p106_12 = p106_12;
    }



    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo1_v_id,_id);
        contentValues.put(SQLConstantes.modulo1_v_COB100A,COB100A);

        contentValues.put(SQLConstantes.seccion_a_p101, p101);
        contentValues.put(SQLConstantes.seccion_a_p101_o, p101_o);
        contentValues.put(SQLConstantes.seccion_a_p102, p102);
        contentValues.put(SQLConstantes.seccion_a_p102_o, p102_o);
        contentValues.put(SQLConstantes.seccion_a_p103, p103);
        contentValues.put(SQLConstantes.seccion_a_p103_o, p103_o);
        contentValues.put(SQLConstantes.seccion_a_p103a, p103a);
        contentValues.put(SQLConstantes.seccion_a_p104, p104);
        contentValues.put(SQLConstantes.seccion_a_p104_o, p104_o);
        contentValues.put(SQLConstantes.seccion_a_p105_1, p105_1);
        contentValues.put(SQLConstantes.seccion_a_p105_2, p105_2);
        contentValues.put(SQLConstantes.seccion_a_p105_3, p105_3);
        contentValues.put(SQLConstantes.seccion_a_p105_4, p105_4);
        contentValues.put(SQLConstantes.seccion_a_p105_5, p105_5);
        contentValues.put(SQLConstantes.seccion_a_p105_5_o, p105_5_o);
        contentValues.put(SQLConstantes.seccion_a_p105_6, p105_6);
        contentValues.put(SQLConstantes.seccion_a_p106_1, p106_1);
        contentValues.put(SQLConstantes.seccion_a_p106_2, p106_2);
        contentValues.put(SQLConstantes.seccion_a_p106_3, p106_3);
        contentValues.put(SQLConstantes.seccion_a_p106_4, p106_4);
        contentValues.put(SQLConstantes.seccion_a_p106_5, p106_5);
        contentValues.put(SQLConstantes.seccion_a_p106_6, p106_6);
        contentValues.put(SQLConstantes.seccion_a_p106_7, p106_7);
        contentValues.put(SQLConstantes.seccion_a_p106_8, p106_8);
        contentValues.put(SQLConstantes.seccion_a_p106_9, p106_9);
        contentValues.put(SQLConstantes.seccion_a_p106_10, p106_10);
        contentValues.put(SQLConstantes.seccion_a_p106_11, p106_11);
        contentValues.put(SQLConstantes.seccion_a_p106_11_o, p106_11_o);
        contentValues.put(SQLConstantes.seccion_a_p106_12, p106_12);

        return contentValues;
    }
}
