package com.example.ricindigus.trabajo2020.util;

public class TipoFragmentEncuestado {
//    public static final int P301P305 = 1;
//    public static final int P306P308 = 2;

    public static final int P001P003 = 1;
    public static final int P004P006 = 2;
    public static final int P007P011 = 3;
    public static final int P012 = 4;
    public static final int P013P014 = 5;
    public static final int P015 = 6;
    public static final int P016 = 7;
    public static final int P017P020 = 8;
    public static final int P021P025 = 9;
    public static final int P026P029 = 10;
    public static final int P030P033 = 11;
    public static final int P034P036 = 12;
    public static final int P038P039 = 13;
    public static final int P040 = 14;
    public static final int P041P043 = 15;
    public static final int P044P046 = 16;
    public static final int P047P048 = 17;
    public static final int P049P051 = 18;
    public static final int P052P053 = 19;
    public static final int P054P056 = 20;
    public static final int P057P059 = 21;



    public static final int P309 = 3;
    public static final int P310P312 = 4;
    public static final int P313P317 = 5;
    public static final int P318 = 6;
    public static final int P401P404 = 7;
    public static final int P405P407 = 8;
    public static final int P408P410 = 9;
    public static final int P411P416 = 10;
    public static final int P501P505 = 11;
    public static final int P506P507 = 12;
    public static final int P508P511 = 13;
    public static final int P512P513 = 14;
    public static final int P601P604 = 15;
    public static final int P605P608 = 16;
    public static final int P609P612 = 17;
    public static final int P613P618 = 18;
    public static final int P619P622 = 19;
    public static final int P623P625 = 20;
    public static final int P626P629 = 21;
    public static final int P630 = 22;
    public static final int P701P705 = 23;
    public static final int P706P709 = 24;
    public static final int P801P804 = 25;
    public static final int P805P808 = 26;
    public static final int P809P812 = 27;
    public static final int P813P816 = 28;
    public static final int P817P820 = 29;
    public static final int P821P823 = 30;
//    public static final int P047P048 = 31;
//    public static final int P049P051 = 32;
//    public static final int P052P053 = 33;
//    public static final int P054P056 = 34;
//    public static final int P057P059 = 35;
}
