package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

public class Modulos5 {
    private String id;
    private String id_vivienda;
    private String numero_hogar;
    private String numero_residente;
    private String inf_300;
    private String p315_1;
    private String p315_2;
    private String p315_3;
    private String p315_4;
    private String p315_5;
    private String p315_6;
    private String p315_7;
    private String p315_8;
    private String p315_9;
    private String p315_10;
    private String p315_11;
    private String p315_11_o;
    private String p316;
    private String p316_1;
    private String p316a_1;
    private String p316a_2;
    private String p316a_3;
    private String p316a_4;
    private String p316a_5;
    private String p316a_6;
    private String p316a_7;
    private String p316a_8;
    private String p317;
    private String p318;
    private String p319;
    private String p319a;
    private String p319a_o;
    private String p320;
    private String p320_o;
    private String p321;
    private String p322_m;
    private String p322_a;
    private String p322_n;
    private String p323_m;
    private String p323_a;
    private String p323_t;
    private String p323_n;
    private String p324;
    private String p324a;
    private String p324a_cod;
    private String p324b;
    private String p324b_cod;
    private String p324c;
    private String p324c_cod;
    private String p324d;
    private String p324d_cod;
    private String p325_d;
    private String p325_h;
    private String obs_e;


    public Modulos5(){
        id="";
        id_vivienda="";
        numero_hogar="";
        numero_residente="";
        inf_300="";
        p315_1="";
        p315_2="";
        p315_3="";
        p315_4="";
        p315_5="";
        p315_6="";
        p315_7="";
        p315_8="";
        p315_9="";
        p315_10="";
        p315_11="";
        p315_11_o="";
        p316="";
        p316_1="";
        p316a_1="";
        p316a_2="";
        p316a_3="";
        p316a_4="";
        p316a_5="";
        p316a_6="";
        p316a_7="";
        p316a_8="";
        p317="";
        p318="";
        p319="";
        p319a="";
        p319a_o="";
        p320="";
        p320_o="";
        p321="";
        p322_m="";
        p322_a="";
        p322_n="";
        p323_m="";
        p323_a="";
        p323_t="";
        p323_n="";
        p324="";
        p324a="";
        p324a_cod="";
        p324b="";
        p324b_cod="";
        p324c="";
        p324c_cod="";
        p324d="";
        p324d_cod="";
        p325_d="";
        p325_h="";
        obs_e="";

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_vivienda() {
        return id_vivienda;
    }

    public void setId_vivienda(String id_vivienda) {
        this.id_vivienda = id_vivienda;
    }

    public String getNumero_hogar() {
        return numero_hogar;
    }

    public void setNumero_hogar(String numero_hogar) {
        this.numero_hogar = numero_hogar;
    }

    public String getNumero_residente() {
        return numero_residente;
    }

    public void setNumero_residente(String numero_residente) {
        this.numero_residente = numero_residente;
    }

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP315_1() {
        return p315_1;
    }

    public void setP315_1(String p315_1) {
        this.p315_1 = p315_1;
    }

    public String getP315_2() {
        return p315_2;
    }

    public void setP315_2(String p315_2) {
        this.p315_2 = p315_2;
    }

    public String getP315_3() {
        return p315_3;
    }

    public void setP315_3(String p315_3) {
        this.p315_3 = p315_3;
    }

    public String getP315_4() {
        return p315_4;
    }

    public void setP315_4(String p315_4) {
        this.p315_4 = p315_4;
    }

    public String getP315_5() {
        return p315_5;
    }

    public void setP315_5(String p315_5) {
        this.p315_5 = p315_5;
    }

    public String getP315_6() {
        return p315_6;
    }

    public void setP315_6(String p315_6) {
        this.p315_6 = p315_6;
    }

    public String getP315_7() {
        return p315_7;
    }

    public void setP315_7(String p315_7) {
        this.p315_7 = p315_7;
    }

    public String getP315_8() {
        return p315_8;
    }

    public void setP315_8(String p315_8) {
        this.p315_8 = p315_8;
    }

    public String getP315_9() {
        return p315_9;
    }

    public void setP315_9(String p315_9) {
        this.p315_9 = p315_9;
    }

    public String getP315_10() {
        return p315_10;
    }

    public void setP315_10(String p315_10) {
        this.p315_10 = p315_10;
    }

    public String getP315_11() {
        return p315_11;
    }

    public void setP315_11(String p315_11) {
        this.p315_11 = p315_11;
    }

    public String getP315_11_o() {
        return p315_11_o;
    }

    public void setP315_11_o(String p315_11_o) {
        this.p315_11_o = p315_11_o;
    }

    public String getP316() {
        return p316;
    }

    public void setP316(String p316) {
        this.p316 = p316;
    }

    public String getP316_1() {
        return p316_1;
    }

    public void setP316_1(String p316_1) {
        this.p316_1 = p316_1;
    }

    public String getP316a_1() {
        return p316a_1;
    }

    public void setP316a_1(String p316a_1) {
        this.p316a_1 = p316a_1;
    }

    public String getP316a_2() {
        return p316a_2;
    }

    public void setP316a_2(String p316a_2) {
        this.p316a_2 = p316a_2;
    }

    public String getP316a_3() {
        return p316a_3;
    }

    public void setP316a_3(String p316a_3) {
        this.p316a_3 = p316a_3;
    }

    public String getP316a_4() {
        return p316a_4;
    }

    public void setP316a_4(String p316a_4) {
        this.p316a_4 = p316a_4;
    }

    public String getP316a_5() {
        return p316a_5;
    }

    public void setP316a_5(String p316a_5) {
        this.p316a_5 = p316a_5;
    }

    public String getP316a_6() {
        return p316a_6;
    }

    public void setP316a_6(String p316a_6) {
        this.p316a_6 = p316a_6;
    }

    public String getP316a_7() {
        return p316a_7;
    }

    public void setP316a_7(String p316a_7) {
        this.p316a_7 = p316a_7;
    }

    public String getP316a_8() {
        return p316a_8;
    }

    public void setP316a_8(String p316a_8) {
        this.p316a_8 = p316a_8;
    }

    public String getP317() {
        return p317;
    }

    public void setP317(String p317) {
        this.p317 = p317;
    }

    public String getP318() {
        return p318;
    }

    public void setP318(String p318) {
        this.p318 = p318;
    }

    public String getP319() {
        return p319;
    }

    public void setP319(String p319) {
        this.p319 = p319;
    }

    public String getP319a() {
        return p319a;
    }

    public void setP319a(String p319a) {
        this.p319a = p319a;
    }

    public String getP319a_o() {
        return p319a_o;
    }

    public void setP319a_o(String p319a_o) {
        this.p319a_o = p319a_o;
    }

    public String getP320() {
        return p320;
    }

    public void setP320(String p320) {
        this.p320 = p320;
    }

    public String getP320_o() {
        return p320_o;
    }

    public void setP320_o(String p320_o) {
        this.p320_o = p320_o;
    }

    public String getP321() {
        return p321;
    }

    public void setP321(String p321) {
        this.p321 = p321;
    }

    public String getP322_m() {
        return p322_m;
    }

    public void setP322_m(String p322_m) {
        this.p322_m = p322_m;
    }

    public String getP322_a() {
        return p322_a;
    }

    public void setP322_a(String p322_a) {
        this.p322_a = p322_a;
    }

    public String getP322_n() {
        return p322_n;
    }

    public void setP322_n(String p322_n) {
        this.p322_n = p322_n;
    }

    public String getP323_m() {
        return p323_m;
    }

    public void setP323_m(String p323_m) {
        this.p323_m = p323_m;
    }

    public String getP323_a() {
        return p323_a;
    }

    public void setP323_a(String p323_a) {
        this.p323_a = p323_a;
    }

    public String getP323_t() {
        return p323_t;
    }

    public void setP323_t(String p323_t) {
        this.p323_t = p323_t;
    }

    public String getP323_n() {
        return p323_n;
    }

    public void setP323_n(String p323_n) {
        this.p323_n = p323_n;
    }

    public String getP324() {
        return p324;
    }

    public void setP324(String p324) {
        this.p324 = p324;
    }

    public String getP324a() {
        return p324a;
    }

    public void setP324a(String p324a) {
        this.p324a = p324a;
    }

    public String getP324a_cod() {
        return p324a_cod;
    }

    public void setP324a_cod(String p324a_cod) {
        this.p324a_cod = p324a_cod;
    }

    public String getP324b() {
        return p324b;
    }

    public void setP324b(String p324b) {
        this.p324b = p324b;
    }

    public String getP324b_cod() {
        return p324b_cod;
    }

    public void setP324b_cod(String p324b_cod) {
        this.p324b_cod = p324b_cod;
    }

    public String getP324c() {
        return p324c;
    }

    public void setP324c(String p324c) {
        this.p324c = p324c;
    }

    public String getP324c_cod() {
        return p324c_cod;
    }

    public void setP324c_cod(String p324c_cod) {
        this.p324c_cod = p324c_cod;
    }

    public String getP324d() {
        return p324d;
    }

    public void setP324d(String p324d) {
        this.p324d = p324d;
    }

    public String getP324d_cod() {
        return p324d_cod;
    }

    public void setP324d_cod(String p324d_cod) {
        this.p324d_cod = p324d_cod;
    }

    public String getP325_d() {
        return p325_d;
    }

    public void setP325_d(String p325_d) {
        this.p325_d = p325_d;
    }

    public String getP325_h() {
        return p325_h;
    }

    public void setP325_h(String p325_h) {
        this.p325_h = p325_h;
    }

    public String getObs_e() {
        return obs_e;
    }

    public void setObs_e(String obs_e) {
        this.obs_e = obs_e;
    }


    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();

        return contentValues;
    }



}
