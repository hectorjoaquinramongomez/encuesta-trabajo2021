package com.example.ricindigus.trabajo2020.util;

import android.text.InputFilter;
import android.text.Spanned;

public class InputFilterSoloNumeros1_9 implements InputFilter {
    @Override
    public CharSequence filter(CharSequence src, int start,
                               int end, Spanned dst, int dstart, int dend) {
        return src.toString().replaceAll("[^1-9]", "");
    }
}