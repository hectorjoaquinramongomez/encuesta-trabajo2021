package com.example.ricindigus.trabajo2020.fragments.modulo5;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo5;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;


public class FragmentP016 extends FragmentPagina {
    String idEncuestado;
    Context context;
    String idInformante;
    String idHogar;
    String idVivienda;

    LinearLayout layout_e_p016;
    LinearLayout layout_e_p016a;

    RadioGroup rg_e_P016;
    EditText   et_e_P016_o;
    EditText   et_e_p016a_1;
    EditText   et_e_p016a_2;
    EditText   et_e_p016a_3;
    EditText   et_e_p016a_4;
    EditText   et_e_p016a_5;
    EditText   et_e_p016a_6;
    EditText   et_e_p016a_7;
    EditText   et_e_p016a_8;

   String p016;
   String p016_1;
   String p016a_1;
   String p016a_2;
   String p016a_3;
   String p016a_4;
   String p016a_5;
   String p016a_6;
   String p016a_7;
   String p016a_8;


    public FragmentP016() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP016(String idEncuestado, Context context) {
        this.idEncuestado = idEncuestado;
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_e_p016, container, false);
        layout_e_p016 = (LinearLayout) rootView.findViewById(R.id.layout_e_p016);
        layout_e_p016a = (LinearLayout) rootView.findViewById(R.id.layout_e_p016_a);
        rg_e_P016    = rootView.findViewById(R.id.rg_e_p016);
        et_e_P016_o  = rootView.findViewById(R.id.et_e_p016);

        et_e_p016a_1 = rootView.findViewById(R.id.et_e_p016a_1);
        et_e_p016a_2 = rootView.findViewById(R.id.et_e_p016a_2);
        et_e_p016a_3 = rootView.findViewById(R.id.et_e_p016a_3);
        et_e_p016a_4 = rootView.findViewById(R.id.et_e_p016a_4);
        et_e_p016a_5 = rootView.findViewById(R.id.et_e_p016a_5);
        et_e_p016a_6 = rootView.findViewById(R.id.et_e_p016a_6);
        et_e_p016a_7 = rootView.findViewById(R.id.et_e_p016a_7);
        et_e_p016a_8 = rootView.findViewById(R.id.et_e_p016a_8);
        
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rg_e_P016.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                controlarEspecifiqueRadio(radioGroup,i,2,et_e_P016_o);
            }
        });


        cargarDatos();

        llenarVista();
        cargarDatos();
    }



    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo5_id_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_e_p316,p016);
        contentValues.put(SQLConstantes.seccion_e_p316_1,p016_1);
        contentValues.put(SQLConstantes.seccion_e_p316a_1,p016a_1);
        contentValues.put(SQLConstantes.seccion_e_p316a_2,p016a_2);
        contentValues.put(SQLConstantes.seccion_e_p316a_3,p016a_3);
        contentValues.put(SQLConstantes.seccion_e_p316a_4,p016a_4);
        contentValues.put(SQLConstantes.seccion_e_p316a_5,p016a_5);
        contentValues.put(SQLConstantes.seccion_e_p316a_6,p016a_6);
        contentValues.put(SQLConstantes.seccion_e_p316a_7,p016a_7);
        contentValues.put(SQLConstantes.seccion_e_p316a_8,p016a_8);
        data.actualizarElemento(getNombreTabla(),contentValues,idEncuestado);
        data.close();
    }

    @Override
    public void llenarVariables() {
        p016 = rg_e_P016.indexOfChild(rg_e_P016.findViewById(rg_e_P016.getCheckedRadioButtonId()))+"";
        p016_1 = et_e_P016_o.getText().toString();
        p016a_1 = et_e_p016a_1.getText().toString();
        p016a_2 = et_e_p016a_2.getText().toString();
        p016a_3 = et_e_p016a_3.getText().toString();
        p016a_4 = et_e_p016a_4.getText().toString();
        p016a_5 = et_e_p016a_5.getText().toString();
        p016a_6 = et_e_p016a_6.getText().toString();
        p016a_7 = et_e_p016a_7.getText().toString();
        p016a_8 = et_e_p016a_8.getText().toString();
    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idEncuestado)){
            Modulo5 modulo5 = data.getModulo5(idEncuestado);
            if(!modulo5.getP316().equals("-1") && !modulo5.getP316().equals(""))((RadioButton)rg_e_P016.getChildAt(Integer.parseInt(modulo5.getP316()))).setChecked(true);
            et_e_P016_o.setText(modulo5.getP316_1());
            et_e_p016a_1.setText(modulo5.getP316a_1());
            et_e_p016a_2.setText(modulo5.getP316a_2());
            et_e_p016a_3.setText(modulo5.getP316a_3());
            et_e_p016a_4.setText(modulo5.getP316a_4());
            et_e_p016a_5.setText(modulo5.getP316a_5());
            et_e_p016a_6.setText(modulo5.getP316a_6());
            et_e_p016a_7.setText(modulo5.getP316a_7());
            et_e_p016a_8.setText(modulo5.getP316a_8());
        }
        data.close();
    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();
        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo5;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    public void ocultarOtrosLayouts(){

    }

    public boolean verificarCoberturaCapitulo(){

        return true;
    }
}
