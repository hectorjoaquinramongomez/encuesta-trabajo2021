package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo5 {
    private String _id;
    private String idInformante;
    private String idHogar;
    private String idVivienda;
    /*
     private String c5_p501;
    private String c5_p502_c;
    private String c5_p502;
    private String c5_p502_eleccion;
    private String c5_p502_o;
    private String c5_p503;
    private String c5_p504;
    private String c5_p505;
    private String c5_p506_1;
    private String c5_p506_2;
    private String c5_p506_3;
    private String c5_p506_4;
    private String c5_p507;
    private String c5_p507_dist;
    private String c5_p507_prov;
    private String c5_p507_dep;
    private String c5_p508_1;
    private String c5_p508_2;
    private String c5_p508_3;
    private String c5_p508_4;
    private String c5_p508_5;
    private String c5_p508_6;
    private String c5_p508_7;
    private String c5_p508_8;
    private String c5_p508_9;
    private String c5_p508_10;
    private String c5_p508_11;
    private String c5_p508_o;
    private String c5_p509;
    private String c5_p510;
    private String c5_p511;
    private String c5_p511_o;
    private String c5_p512;
    private String c5_p512_o;
    private String c5_p513;
    private String c5_p513_o;
     */
    //nuevos
    //private String id;
    //private String id_vivienda;
    // private String numero_hogar;
    // private String numero_residente;
    private String inf_300;
    private String p315_1;
    private String p315_2;
    private String p315_3;
    private String p315_4;
    private String p315_5;
    private String p315_6;
    private String p315_7;
    private String p315_8;
    private String p315_9;
    private String p315_10;
    private String p315_11;
    private String p315_11_o;
    private String p316;
    private String p316_1;
    private String p316a_1;
    private String p316a_2;
    private String p316a_3;
    private String p316a_4;
    private String p316a_5;
    private String p316a_6;
    private String p316a_7;
    private String p316a_8;
    private String p317;
    private String p318;
    private String p319;
    private String p319a;
    private String p319a_o;
    private String p320;
    private String p320_o;
    private String p321;
    private String p322_m;
    private String p322_a;
    private String p322_n;
    private String p323_m;
    private String p323_a;
    private String p323_t;
    private String p323_n;
    private String p324;
    private String p324a;
    private String p324a_cod;
    private String p324b;
    private String p324b_cod;
    private String p324c;
    private String p324c_cod;
    private String p324d;
    private String p324d_cod;
    private String p325_d;
    private String p325_h;
    private String obs_e;
    private String COB500;

    public Modulo5() {
        _id = "";
        idHogar = "";
        idVivienda = "";
        idInformante= "";
        /*
        c5_p501= "";
        c5_p502_c= "";
        c5_p502= "";
        c5_p502_eleccion= "";
        c5_p502_o= "";
        c5_p503= "";
        c5_p504= "";
        c5_p505= "";
        c5_p506_1= "";
        c5_p506_2= "";
        c5_p506_3= "";
        c5_p506_4= "";
        c5_p507= "";
        c5_p507_dist= "";
        c5_p507_prov= "";
        c5_p507_dep= "";
        c5_p508_1= "";
        c5_p508_2= "";
        c5_p508_3= "";
        c5_p508_4= "";
        c5_p508_5= "";
        c5_p508_6= "";
        c5_p508_7= "";
        c5_p508_8= "";
        c5_p508_9= "";
        c5_p508_10= "";
        c5_p508_11= "";
        c5_p508_o= "";
        c5_p509= "";
        c5_p510= "";
        c5_p511= "";
        c5_p511_o= "";
        c5_p512= "";
        c5_p512_o= "";
        c5_p513= "";
        c5_p513_o= "";
        obs_cap5= "";
         */
        //nuevos
        inf_300= "";
        p315_1= "";
        p315_2= "";
        p315_3= "";
        p315_4= "";
        p315_5= "";
        p315_6= "";
        p315_7= "";
        p315_8= "";
        p315_9= "";
        p315_10= "";
        p315_11= "";
        p315_11_o= "";
        p316= "";
        p316_1= "";
        p316a_1= "";
        p316a_2= "";
        p316a_3= "";
        p316a_4= "";
        p316a_5= "";
        p316a_6= "";
        p316a_7= "";
        p316a_8= "";
        p317= "";
        p318= "";
        p319= "";
        p319a= "";
        p319a_o= "";
        p320= "";
        p320_o= "";
        p321= "";
        p322_m= "";
        p322_a= "";
        p322_n= "";
        p323_m= "";
        p323_a= "";
        p323_t= "";
        p323_n= "";
        p324= "";
        p324a= "";
        p324a_cod= "";
        p324b= "";
        p324b_cod= "";
        p324c= "";
        p324c_cod= "";
        p324d= "";
        p324d_cod= "";
        p325_d= "";
        p325_h= "";
        obs_e= "";
        COB500= "0";
    }


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdInformante() {
        return idInformante;
    }

    public void setIdInformante(String idInformante) {
        this.idInformante = idInformante;
    }

    public String getIdHogar() {
        return idHogar;
    }

    public void setIdHogar(String idHogar) {
        this.idHogar = idHogar;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }

    /*
     public String getC5_p501() {
        return c5_p501;
    }

    public void setC5_p501(String c5_p501) {
        this.c5_p501 = c5_p501;
    }

    public String getC5_p502_c() {
        return c5_p502_c;
    }

    public void setC5_p502_c(String c5_p502_c) {
        this.c5_p502_c = c5_p502_c;
    }

    public String getC5_p502() {
        return c5_p502;
    }

    public void setC5_p502(String c5_p502) {
        this.c5_p502 = c5_p502;
    }

    public String getC5_p502_eleccion() {
        return c5_p502_eleccion;
    }

    public void setC5_p502_eleccion(String c5_p502_eleccion) {
        this.c5_p502_eleccion = c5_p502_eleccion;
    }

    public String getC5_p502_o() {
        return c5_p502_o;
    }

    public void setC5_p502_o(String c5_p502_o) {
        this.c5_p502_o = c5_p502_o;
    }

    public String getC5_p503() {
        return c5_p503;
    }

    public void setC5_p503(String c5_p503) {
        this.c5_p503 = c5_p503;
    }

    public String getC5_p504() {
        return c5_p504;
    }

    public void setC5_p504(String c5_p504) {
        this.c5_p504 = c5_p504;
    }

    public String getC5_p505() {
        return c5_p505;
    }

    public void setC5_p505(String c5_p505) {
        this.c5_p505 = c5_p505;
    }

    public String getC5_p506_1() {
        return c5_p506_1;
    }

    public void setC5_p506_1(String c5_p506_1) {
        this.c5_p506_1 = c5_p506_1;
    }

    public String getC5_p506_2() {
        return c5_p506_2;
    }

    public void setC5_p506_2(String c5_p506_2) {
        this.c5_p506_2 = c5_p506_2;
    }

    public String getC5_p506_3() {
        return c5_p506_3;
    }

    public void setC5_p506_3(String c5_p506_3) {
        this.c5_p506_3 = c5_p506_3;
    }

    public String getC5_p506_4() {
        return c5_p506_4;
    }

    public void setC5_p506_4(String c5_p506_4) {
        this.c5_p506_4 = c5_p506_4;
    }

    public String getC5_p507() {
        return c5_p507;
    }

    public void setC5_p507(String c5_p507) {
        this.c5_p507 = c5_p507;
    }

    public String getC5_p507_dist() {
        return c5_p507_dist;
    }

    public void setC5_p507_dist(String c5_p507_dist) {
        this.c5_p507_dist = c5_p507_dist;
    }

    public String getC5_p507_prov() {
        return c5_p507_prov;
    }

    public void setC5_p507_prov(String c5_p507_prov) {
        this.c5_p507_prov = c5_p507_prov;
    }

    public String getC5_p507_dep() {
        return c5_p507_dep;
    }

    public void setC5_p507_dep(String c5_p507_dep) {
        this.c5_p507_dep = c5_p507_dep;
    }

    public String getC5_p508_1() {
        return c5_p508_1;
    }

    public void setC5_p508_1(String c5_p508_1) {
        this.c5_p508_1 = c5_p508_1;
    }

    public String getC5_p508_2() {
        return c5_p508_2;
    }

    public void setC5_p508_2(String c5_p508_2) {
        this.c5_p508_2 = c5_p508_2;
    }

    public String getC5_p508_3() {
        return c5_p508_3;
    }

    public void setC5_p508_3(String c5_p508_3) {
        this.c5_p508_3 = c5_p508_3;
    }

    public String getC5_p508_4() {
        return c5_p508_4;
    }

    public void setC5_p508_4(String c5_p508_4) {
        this.c5_p508_4 = c5_p508_4;
    }

    public String getC5_p508_5() {
        return c5_p508_5;
    }

    public void setC5_p508_5(String c5_p508_5) {
        this.c5_p508_5 = c5_p508_5;
    }

    public String getC5_p508_6() {
        return c5_p508_6;
    }

    public void setC5_p508_6(String c5_p508_6) {
        this.c5_p508_6 = c5_p508_6;
    }

    public String getC5_p508_7() {
        return c5_p508_7;
    }

    public void setC5_p508_7(String c5_p508_7) {
        this.c5_p508_7 = c5_p508_7;
    }

    public String getC5_p508_8() {
        return c5_p508_8;
    }

    public void setC5_p508_8(String c5_p508_8) {
        this.c5_p508_8 = c5_p508_8;
    }

    public String getC5_p508_9() {
        return c5_p508_9;
    }

    public void setC5_p508_9(String c5_p508_9) {
        this.c5_p508_9 = c5_p508_9;
    }

    public String getC5_p508_10() {
        return c5_p508_10;
    }

    public void setC5_p508_10(String c5_p508_10) {
        this.c5_p508_10 = c5_p508_10;
    }

    public String getC5_p508_11() {
        return c5_p508_11;
    }

    public void setC5_p508_11(String c5_p508_11) {
        this.c5_p508_11 = c5_p508_11;
    }

    public String getC5_p508_o() {
        return c5_p508_o;
    }

    public void setC5_p508_o(String c5_p508_o) {
        this.c5_p508_o = c5_p508_o;
    }

    public String getC5_p509() {
        return c5_p509;
    }

    public void setC5_p509(String c5_p509) {
        this.c5_p509 = c5_p509;
    }

    public String getC5_p510() {
        return c5_p510;
    }

    public void setC5_p510(String c5_p510) {
        this.c5_p510 = c5_p510;
    }

    public String getC5_p511() {
        return c5_p511;
    }

    public void setC5_p511(String c5_p511) {
        this.c5_p511 = c5_p511;
    }

    public String getC5_p511_o() {
        return c5_p511_o;
    }

    public void setC5_p511_o(String c5_p511_o) {
        this.c5_p511_o = c5_p511_o;
    }

    public String getC5_p512() {
        return c5_p512;
    }

    public void setC5_p512(String c5_p512) {
        this.c5_p512 = c5_p512;
    }

    public String getC5_p512_o() {
        return c5_p512_o;
    }

    public void setC5_p512_o(String c5_p512_o) {
        this.c5_p512_o = c5_p512_o;
    }

    public String getC5_p513() {
        return c5_p513;
    }

    public void setC5_p513(String c5_p513) {
        this.c5_p513 = c5_p513;
    }

    public String getC5_p513_o() {
        return c5_p513_o;
    }

    public void setC5_p513_o(String c5_p513_o) {
        this.c5_p513_o = c5_p513_o;
    }

    public String getObs_cap5() {
        return obs_cap5;
    }

    public void setObs_cap5(String obs_cap5) {
        this.obs_cap5 = obs_cap5;
    }

     */

    public String getCOB500() {
        return COB500;
    }

    public void setCOB500(String COB500) {
        this.COB500 = COB500;
    }

    public String getInf_300() {
        return inf_300;
    }

    public void setInf_300(String inf_300) {
        this.inf_300 = inf_300;
    }

    public String getP315_1() {
        return p315_1;
    }

    public void setP315_1(String p315_1) {
        this.p315_1 = p315_1;
    }

    public String getP315_2() {
        return p315_2;
    }

    public void setP315_2(String p315_2) {
        this.p315_2 = p315_2;
    }

    public String getP315_3() {
        return p315_3;
    }

    public void setP315_3(String p315_3) {
        this.p315_3 = p315_3;
    }

    public String getP315_4() {
        return p315_4;
    }

    public void setP315_4(String p315_4) {
        this.p315_4 = p315_4;
    }

    public String getP315_5() {
        return p315_5;
    }

    public void setP315_5(String p315_5) {
        this.p315_5 = p315_5;
    }

    public String getP315_6() {
        return p315_6;
    }

    public void setP315_6(String p315_6) {
        this.p315_6 = p315_6;
    }

    public String getP315_7() {
        return p315_7;
    }

    public void setP315_7(String p315_7) {
        this.p315_7 = p315_7;
    }

    public String getP315_8() {
        return p315_8;
    }

    public void setP315_8(String p315_8) {
        this.p315_8 = p315_8;
    }

    public String getP315_9() {
        return p315_9;
    }

    public void setP315_9(String p315_9) {
        this.p315_9 = p315_9;
    }

    public String getP315_10() {
        return p315_10;
    }

    public void setP315_10(String p315_10) {
        this.p315_10 = p315_10;
    }

    public String getP315_11() {
        return p315_11;
    }

    public void setP315_11(String p315_11) {
        this.p315_11 = p315_11;
    }

    public String getP315_11_o() {
        return p315_11_o;
    }

    public void setP315_11_o(String p315_11_o) {
        this.p315_11_o = p315_11_o;
    }

    public String getP316() {
        return p316;
    }

    public void setP316(String p316) {
        this.p316 = p316;
    }

    public String getP316_1() {
        return p316_1;
    }

    public void setP316_1(String p316_1) {
        this.p316_1 = p316_1;
    }

    public String getP316a_1() {
        return p316a_1;
    }

    public void setP316a_1(String p316a_1) {
        this.p316a_1 = p316a_1;
    }

    public String getP316a_2() {
        return p316a_2;
    }

    public void setP316a_2(String p316a_2) {
        this.p316a_2 = p316a_2;
    }

    public String getP316a_3() {
        return p316a_3;
    }

    public void setP316a_3(String p316a_3) {
        this.p316a_3 = p316a_3;
    }

    public String getP316a_4() {
        return p316a_4;
    }

    public void setP316a_4(String p316a_4) {
        this.p316a_4 = p316a_4;
    }

    public String getP316a_5() {
        return p316a_5;
    }

    public void setP316a_5(String p316a_5) {
        this.p316a_5 = p316a_5;
    }

    public String getP316a_6() {
        return p316a_6;
    }

    public void setP316a_6(String p316a_6) {
        this.p316a_6 = p316a_6;
    }

    public String getP316a_7() {
        return p316a_7;
    }

    public void setP316a_7(String p316a_7) {
        this.p316a_7 = p316a_7;
    }

    public String getP316a_8() {
        return p316a_8;
    }

    public void setP316a_8(String p316a_8) {
        this.p316a_8 = p316a_8;
    }

    public String getP317() {
        return p317;
    }

    public void setP317(String p317) {
        this.p317 = p317;
    }

    public String getP318() {
        return p318;
    }

    public void setP318(String p318) {
        this.p318 = p318;
    }

    public String getP319() {
        return p319;
    }

    public void setP319(String p319) {
        this.p319 = p319;
    }

    public String getP319a() {
        return p319a;
    }

    public void setP319a(String p319a) {
        this.p319a = p319a;
    }

    public String getP319a_o() {
        return p319a_o;
    }

    public void setP319a_o(String p319a_o) {
        this.p319a_o = p319a_o;
    }

    public String getP320() {
        return p320;
    }

    public void setP320(String p320) {
        this.p320 = p320;
    }

    public String getP320_o() {
        return p320_o;
    }

    public void setP320_o(String p320_o) {
        this.p320_o = p320_o;
    }

    public String getP321() {
        return p321;
    }

    public void setP321(String p321) {
        this.p321 = p321;
    }

    public String getP322_m() {
        return p322_m;
    }

    public void setP322_m(String p322_m) {
        this.p322_m = p322_m;
    }

    public String getP322_a() {
        return p322_a;
    }

    public void setP322_a(String p322_a) {
        this.p322_a = p322_a;
    }

    public String getP322_n() {
        return p322_n;
    }

    public void setP322_n(String p322_n) {
        this.p322_n = p322_n;
    }

    public String getP323_m() {
        return p323_m;
    }

    public void setP323_m(String p323_m) {
        this.p323_m = p323_m;
    }

    public String getP323_a() {
        return p323_a;
    }

    public void setP323_a(String p323_a) {
        this.p323_a = p323_a;
    }

    public String getP323_t() {
        return p323_t;
    }

    public void setP323_t(String p323_t) {
        this.p323_t = p323_t;
    }

    public String getP323_n() {
        return p323_n;
    }

    public void setP323_n(String p323_n) {
        this.p323_n = p323_n;
    }

    public String getP324() {
        return p324;
    }

    public void setP324(String p324) {
        this.p324 = p324;
    }

    public String getP324a() {
        return p324a;
    }

    public void setP324a(String p324a) {
        this.p324a = p324a;
    }

    public String getP324a_cod() {
        return p324a_cod;
    }

    public void setP324a_cod(String p324a_cod) {
        this.p324a_cod = p324a_cod;
    }

    public String getP324b() {
        return p324b;
    }

    public void setP324b(String p324b) {
        this.p324b = p324b;
    }

    public String getP324b_cod() {
        return p324b_cod;
    }

    public void setP324b_cod(String p324b_cod) {
        this.p324b_cod = p324b_cod;
    }

    public String getP324c() {
        return p324c;
    }

    public void setP324c(String p324c) {
        this.p324c = p324c;
    }

    public String getP324c_cod() {
        return p324c_cod;
    }

    public void setP324c_cod(String p324c_cod) {
        this.p324c_cod = p324c_cod;
    }

    public String getP324d() {
        return p324d;
    }

    public void setP324d(String p324d) {
        this.p324d = p324d;
    }

    public String getP324d_cod() {
        return p324d_cod;
    }

    public void setP324d_cod(String p324d_cod) {
        this.p324d_cod = p324d_cod;
    }

    public String getP325_d() {
        return p325_d;
    }

    public void setP325_d(String p325_d) {
        this.p325_d = p325_d;
    }

    public String getP325_h() {
        return p325_h;
    }

    public void setP325_h(String p325_h) {
        this.p325_h = p325_h;
    }

    public String getObs_e() {
        return obs_e;
    }

    public void setObs_e(String obs_e) {
        this.obs_e = obs_e;
    }

    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo5_id,_id);
        contentValues.put(SQLConstantes.modulo5_id_informante,idInformante);
        contentValues.put(SQLConstantes.modulo5_id_hogar,idHogar);
        contentValues.put(SQLConstantes.modulo5_id_vivienda,idVivienda);
        /*
        contentValues.put(SQLConstantes.modulo5_c5_p501,c5_p501);
        contentValues.put(SQLConstantes.modulo5_c5_p502_c,c5_p502_c);
        contentValues.put(SQLConstantes.modulo5_c5_p502,c5_p502);
        contentValues.put(SQLConstantes.modulo5_c5_p502_eleccion,c5_p502_eleccion);
        contentValues.put(SQLConstantes.modulo5_c5_p502_o,c5_p502_o);
        contentValues.put(SQLConstantes.modulo5_c5_p503,c5_p503);
        contentValues.put(SQLConstantes.modulo5_c5_p504,c5_p504);
        contentValues.put(SQLConstantes.modulo5_c5_p505,c5_p505);
        contentValues.put(SQLConstantes.modulo5_c5_p506_1,c5_p506_1);
        contentValues.put(SQLConstantes.modulo5_c5_p506_2,c5_p506_2);
        contentValues.put(SQLConstantes.modulo5_c5_p506_3,c5_p506_3);
        contentValues.put(SQLConstantes.modulo5_c5_p506_4,c5_p506_4);
        contentValues.put(SQLConstantes.modulo5_c5_p507,c5_p507);
        contentValues.put(SQLConstantes.modulo5_c5_p507_dist,c5_p507_dist);
        contentValues.put(SQLConstantes.modulo5_c5_p507_prov,c5_p507_prov);
        contentValues.put(SQLConstantes.modulo5_c5_p507_dep,c5_p507_dep);
        contentValues.put(SQLConstantes.modulo5_c5_p508_1,c5_p508_1);
        contentValues.put(SQLConstantes.modulo5_c5_p508_2,c5_p508_2);
        contentValues.put(SQLConstantes.modulo5_c5_p508_3,c5_p508_3);
        contentValues.put(SQLConstantes.modulo5_c5_p508_4,c5_p508_4);
        contentValues.put(SQLConstantes.modulo5_c5_p508_5,c5_p508_5);
        contentValues.put(SQLConstantes.modulo5_c5_p508_6,c5_p508_6);
        contentValues.put(SQLConstantes.modulo5_c5_p508_7,c5_p508_7);
        contentValues.put(SQLConstantes.modulo5_c5_p508_8,c5_p508_8);
        contentValues.put(SQLConstantes.modulo5_c5_p508_9,c5_p508_9);
        contentValues.put(SQLConstantes.modulo5_c5_p508_10,c5_p508_10);
        contentValues.put(SQLConstantes.modulo5_c5_p508_11,c5_p508_11);
        contentValues.put(SQLConstantes.modulo5_c5_p508_o,c5_p508_o);
        contentValues.put(SQLConstantes.modulo5_c5_p509,c5_p509);
        contentValues.put(SQLConstantes.modulo5_c5_p510,c5_p510);
        contentValues.put(SQLConstantes.modulo5_c5_p511,c5_p511);
        contentValues.put(SQLConstantes.modulo5_c5_p511_o,c5_p511_o);
        contentValues.put(SQLConstantes.modulo5_c5_p512,c5_p512);
        contentValues.put(SQLConstantes.modulo5_c5_p512_o,c5_p512_o);
        contentValues.put(SQLConstantes.modulo5_c5_p513,c5_p513);
        contentValues.put(SQLConstantes.modulo5_c5_p513_o,c5_p513_o);
        contentValues.put(SQLConstantes.modulo5_obs_cap5,obs_cap5);
         */
        contentValues.put(SQLConstantes.seccion_e_inf_300,inf_300);
        contentValues.put(SQLConstantes.seccion_e_p315_1,p315_1);
        contentValues.put(SQLConstantes.seccion_e_p315_2,p315_2);
        contentValues.put(SQLConstantes.seccion_e_p315_3,p315_3);
        contentValues.put(SQLConstantes.seccion_e_p315_4,p315_4);
        contentValues.put(SQLConstantes.seccion_e_p315_5,p315_5);
        contentValues.put(SQLConstantes.seccion_e_p315_6,p315_6);
        contentValues.put(SQLConstantes.seccion_e_p315_7,p315_7);
        contentValues.put(SQLConstantes.seccion_e_p315_8,p315_8);
        contentValues.put(SQLConstantes.seccion_e_p315_9,p315_9);
        contentValues.put(SQLConstantes.seccion_e_p315_10,p315_10);
        contentValues.put(SQLConstantes.seccion_e_p315_11,p315_11);
        contentValues.put(SQLConstantes.seccion_e_p315_11_o,p315_11_o);
        contentValues.put(SQLConstantes.seccion_e_p316,p316);
        contentValues.put(SQLConstantes.seccion_e_p316_1,p316_1);
        contentValues.put(SQLConstantes.seccion_e_p316a_1,p316a_1);
        contentValues.put(SQLConstantes.seccion_e_p316a_2,p316a_2);
        contentValues.put(SQLConstantes.seccion_e_p316a_3,p316a_3);
        contentValues.put(SQLConstantes.seccion_e_p316a_4,p316a_4);
        contentValues.put(SQLConstantes.seccion_e_p316a_5,p316a_5);
        contentValues.put(SQLConstantes.seccion_e_p316a_6,p316a_6);
        contentValues.put(SQLConstantes.seccion_e_p316a_7,p316a_7);
        contentValues.put(SQLConstantes.seccion_e_p316a_8,p316a_8);
        contentValues.put(SQLConstantes.seccion_e_p317,p317);
        contentValues.put(SQLConstantes.seccion_e_p318,p318);
        contentValues.put(SQLConstantes.seccion_e_p319,p319);
        contentValues.put(SQLConstantes.seccion_e_p319a,p319a);
        contentValues.put(SQLConstantes.seccion_e_p319a_o,p319a_o);
        contentValues.put(SQLConstantes.seccion_e_p320,p320);
        contentValues.put(SQLConstantes.seccion_e_p320_o,p320_o);
        contentValues.put(SQLConstantes.seccion_e_p321,p321);
        contentValues.put(SQLConstantes.seccion_e_p322_m,p322_m);
        contentValues.put(SQLConstantes.seccion_e_p322_a,p322_a);
        contentValues.put(SQLConstantes.seccion_e_p322_n,p322_n);
        contentValues.put(SQLConstantes.seccion_e_p323_m,p323_m);
        contentValues.put(SQLConstantes.seccion_e_p323_a,p323_a);
        contentValues.put(SQLConstantes.seccion_e_p323_t,p323_t);
        contentValues.put(SQLConstantes.seccion_e_p323_n,p323_n);
        contentValues.put(SQLConstantes.seccion_e_p324,p324);
        contentValues.put(SQLConstantes.seccion_e_p324a,p324a);
        contentValues.put(SQLConstantes.seccion_e_p324a_cod,p324a_cod);
        contentValues.put(SQLConstantes.seccion_e_p324b,p324b);
        contentValues.put(SQLConstantes.seccion_e_p324b_cod,p324b_cod);
        contentValues.put(SQLConstantes.seccion_e_p324c,p324c);
        contentValues.put(SQLConstantes.seccion_e_p324c_cod,p324c_cod);
        contentValues.put(SQLConstantes.seccion_e_p324d,p324d);
        contentValues.put(SQLConstantes.seccion_e_p324d_cod,p324d_cod);
        contentValues.put(SQLConstantes.seccion_e_p325_d,p325_d);
        contentValues.put(SQLConstantes.seccion_e_p325_h,p325_h);
        contentValues.put(SQLConstantes.seccion_e_obs_e,obs_e);
        contentValues.put(SQLConstantes.modulo5_COB500,COB500);
        return contentValues;
    }
}
