package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo9 {
    private String _id;
    private String idInformante;
    private String idHogar;
    private String idVivienda;
    private String p347_1;
    private String p347_2;
    private String p347_3;
    private String p347_4;
    private String p347_5;
    private String p348_1;
    private String p348_2;
    private String p348_3;
    private String p348_4;
    private String p348_5;
    private String p348_6;
    private String p348_7;
    private String p348_8;
    private String p348_9;
    private String p348_10;
    private String p348_11;
    private String p348_11_o;
    private String p348_12;
    private String p348_13;
    private String p348_14;
    private String p348_i;
    private String p349;
    private String p350_1;
    private String p350_2;
    private String p350_3;
    private String p350_4;
    private String p350_5;
    private String p350_5_o;
    private String p351;
    private String p351a_1;
    private String p351a_2;
    private String p351a_3;
    private String p351a_4;
    private String p351a_5;
    private String p351a_5_o;
    private String p352;
    private String p352_o;
    private String p353_1;
    private String p353_2;
    private String p353_3;
    private String p353_4;
    private String p353_5;
    private String p353_6;
    private String p353_6_o;
    private String p354_1;
    private String p355_1;
    private String p355_1_o;
    private String p354_2;
    private String p355_2;
    private String p355_2_o;
    private String p354_3;
    private String p355_3;
    private String p355_3_o;
    private String p354_4;
    private String p355_4;
    private String p355_4_o;
    private String p354_5;
    private String p355_5;
    private String p355_5_o;
    private String p354_6;
    private String p354_6_o;
    private String p355_6;
    private String p355_6_o;
    private String p356;
    private String p357_1;
    private String p357_2;
    private String p357_3;
    private String p357_4;
    private String p357_5;
    private String p357_6;
    private String p357_7;
    private String p357_8;
    private String p357_8_o;
    private String p357_9;
    private String p358;
    private String p359;
    private String obs_i;


    public Modulo9() {
        _id = "";
        idHogar = "";
        idVivienda = "";
        idInformante= "";
         p347_1 = "";
         p347_2 = "";
         p347_3 = "";
         p347_4 = "";
         p347_5 = "";
         p348_1 = "";
         p348_2 = "";
         p348_3 = "";
         p348_4 = "";
         p348_5 = "";
         p348_6 = "";
         p348_7 = "";
         p348_8 = "";
         p348_9 = "";
         p348_10 = "";
         p348_11 = "";
         p348_11_o = "";
         p348_12 = "";
         p348_13 = "";
         p348_14 = "";
         p348_i = "";
         p349 = "";
         p350_1 = "";
         p350_2 = "";
         p350_3 = "";
         p350_4 = "";
         p350_5 = "";
         p350_5_o = "";
         p351 = "";
         p351a_1 = "";
         p351a_2 = "";
         p351a_3 = "";
         p351a_4 = "";
         p351a_5 = "";
         p351a_5_o = "";
         p352 = "";
         p352_o = "";
         p353_1 = "";
         p353_2 = "";
         p353_3 = "";
         p353_4 = "";
         p353_5 = "";
         p353_6 = "";
         p353_6_o = "";
         p354_1 = "";
         p355_1 = "";
         p355_1_o = "";
         p354_2 = "";
         p355_2 = "";
         p355_2_o = "";
         p354_3 = "";
         p355_3 = "";
         p355_3_o = "";
         p354_4 = "";
         p355_4 = "";
         p355_4_o = "";
         p354_5 = "";
         p355_5 = "";
         p355_5_o = "";
         p354_6 = "";
         p354_6_o = "";
         p355_6 = "";
         p355_6_o = "";
         p356 = "";
         p357_1 = "";
         p357_2 = "";
         p357_3 = "";
         p357_4 = "";
         p357_5 = "";
         p357_6 = "";
         p357_7 = "";
         p357_8 = "";
         p357_8_o = "";
         p357_9 = "";
         p358 = "";
         p359 = "";
         obs_i = "";
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdInformante() {
        return idInformante;
    }

    public void setIdInformante(String idInformante) {
        this.idInformante = idInformante;
    }

    public String getIdHogar() {
        return idHogar;
    }

    public void setIdHogar(String idHogar) {
        this.idHogar = idHogar;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }

    public String getP347_1() {
        return p347_1;
    }

    public void setP347_1(String p347_1) {
        this.p347_1 = p347_1;
    }

    public String getP347_2() {
        return p347_2;
    }

    public void setP347_2(String p347_2) {
        this.p347_2 = p347_2;
    }

    public String getP347_3() {
        return p347_3;
    }

    public void setP347_3(String p347_3) {
        this.p347_3 = p347_3;
    }

    public String getP347_4() {
        return p347_4;
    }

    public void setP347_4(String p347_4) {
        this.p347_4 = p347_4;
    }

    public String getP347_5() {
        return p347_5;
    }

    public void setP347_5(String p347_5) {
        this.p347_5 = p347_5;
    }

    public String getP348_1() {
        return p348_1;
    }

    public void setP348_1(String p348_1) {
        this.p348_1 = p348_1;
    }

    public String getP348_2() {
        return p348_2;
    }

    public void setP348_2(String p348_2) {
        this.p348_2 = p348_2;
    }

    public String getP348_3() {
        return p348_3;
    }

    public void setP348_3(String p348_3) {
        this.p348_3 = p348_3;
    }

    public String getP348_4() {
        return p348_4;
    }

    public void setP348_4(String p348_4) {
        this.p348_4 = p348_4;
    }

    public String getP348_5() {
        return p348_5;
    }

    public void setP348_5(String p348_5) {
        this.p348_5 = p348_5;
    }

    public String getP348_6() {
        return p348_6;
    }

    public void setP348_6(String p348_6) {
        this.p348_6 = p348_6;
    }

    public String getP348_7() {
        return p348_7;
    }

    public void setP348_7(String p348_7) {
        this.p348_7 = p348_7;
    }

    public String getP348_8() {
        return p348_8;
    }

    public void setP348_8(String p348_8) {
        this.p348_8 = p348_8;
    }

    public String getP348_9() {
        return p348_9;
    }

    public void setP348_9(String p348_9) {
        this.p348_9 = p348_9;
    }

    public String getP348_10() {
        return p348_10;
    }

    public void setP348_10(String p348_10) {
        this.p348_10 = p348_10;
    }

    public String getP348_11() {
        return p348_11;
    }

    public void setP348_11(String p348_11) {
        this.p348_11 = p348_11;
    }

    public String getP348_11_o() {
        return p348_11_o;
    }

    public void setP348_11_o(String p348_11_o) {
        this.p348_11_o = p348_11_o;
    }

    public String getP348_12() {
        return p348_12;
    }

    public void setP348_12(String p348_12) {
        this.p348_12 = p348_12;
    }

    public String getP348_13() {
        return p348_13;
    }

    public void setP348_13(String p348_13) {
        this.p348_13 = p348_13;
    }

    public String getP348_14() {
        return p348_14;
    }

    public void setP348_14(String p348_14) {
        this.p348_14 = p348_14;
    }

    public String getP348_i() {
        return p348_i;
    }

    public void setP348_i(String p348_i) {
        this.p348_i = p348_i;
    }

    public String getP349() {
        return p349;
    }

    public void setP349(String p349) {
        this.p349 = p349;
    }

    public String getP350_1() {
        return p350_1;
    }

    public void setP350_1(String p350_1) {
        this.p350_1 = p350_1;
    }

    public String getP350_2() {
        return p350_2;
    }

    public void setP350_2(String p350_2) {
        this.p350_2 = p350_2;
    }

    public String getP350_3() {
        return p350_3;
    }

    public void setP350_3(String p350_3) {
        this.p350_3 = p350_3;
    }

    public String getP350_4() {
        return p350_4;
    }

    public void setP350_4(String p350_4) {
        this.p350_4 = p350_4;
    }

    public String getP350_5() {
        return p350_5;
    }

    public void setP350_5(String p350_5) {
        this.p350_5 = p350_5;
    }

    public String getP350_5_o() {
        return p350_5_o;
    }

    public void setP350_5_o(String p350_5_o) {
        this.p350_5_o = p350_5_o;
    }

    public String getP351() {
        return p351;
    }

    public void setP351(String p351) {
        this.p351 = p351;
    }

    public String getP351a_1() {
        return p351a_1;
    }

    public void setP351a_1(String p351a_1) {
        this.p351a_1 = p351a_1;
    }

    public String getP351a_2() {
        return p351a_2;
    }

    public void setP351a_2(String p351a_2) {
        this.p351a_2 = p351a_2;
    }

    public String getP351a_3() {
        return p351a_3;
    }

    public void setP351a_3(String p351a_3) {
        this.p351a_3 = p351a_3;
    }

    public String getP351a_4() {
        return p351a_4;
    }

    public void setP351a_4(String p351a_4) {
        this.p351a_4 = p351a_4;
    }

    public String getP351a_5() {
        return p351a_5;
    }

    public void setP351a_5(String p351a_5) {
        this.p351a_5 = p351a_5;
    }

    public String getP351a_5_o() {
        return p351a_5_o;
    }

    public void setP351a_5_o(String p351a_5_o) {
        this.p351a_5_o = p351a_5_o;
    }

    public String getP352() {
        return p352;
    }

    public void setP352(String p352) {
        this.p352 = p352;
    }

    public String getP352_o() {
        return p352_o;
    }

    public void setP352_o(String p352_o) {
        this.p352_o = p352_o;
    }

    public String getP353_1() {
        return p353_1;
    }

    public void setP353_1(String p353_1) {
        this.p353_1 = p353_1;
    }

    public String getP353_2() {
        return p353_2;
    }

    public void setP353_2(String p353_2) {
        this.p353_2 = p353_2;
    }

    public String getP353_3() {
        return p353_3;
    }

    public void setP353_3(String p353_3) {
        this.p353_3 = p353_3;
    }

    public String getP353_4() {
        return p353_4;
    }

    public void setP353_4(String p353_4) {
        this.p353_4 = p353_4;
    }

    public String getP353_5() {
        return p353_5;
    }

    public void setP353_5(String p353_5) {
        this.p353_5 = p353_5;
    }

    public String getP353_6() {
        return p353_6;
    }

    public void setP353_6(String p353_6) {
        this.p353_6 = p353_6;
    }

    public String getP353_6_o() {
        return p353_6_o;
    }

    public void setP353_6_o(String p353_6_o) {
        this.p353_6_o = p353_6_o;
    }

    public String getP354_1() {
        return p354_1;
    }

    public void setP354_1(String p354_1) {
        this.p354_1 = p354_1;
    }

    public String getP355_1() {
        return p355_1;
    }

    public void setP355_1(String p355_1) {
        this.p355_1 = p355_1;
    }

    public String getP355_1_o() {
        return p355_1_o;
    }

    public void setP355_1_o(String p355_1_o) {
        this.p355_1_o = p355_1_o;
    }

    public String getP354_2() {
        return p354_2;
    }

    public void setP354_2(String p354_2) {
        this.p354_2 = p354_2;
    }

    public String getP355_2() {
        return p355_2;
    }

    public void setP355_2(String p355_2) {
        this.p355_2 = p355_2;
    }

    public String getP355_2_o() {
        return p355_2_o;
    }

    public void setP355_2_o(String p355_2_o) {
        this.p355_2_o = p355_2_o;
    }

    public String getP354_3() {
        return p354_3;
    }

    public void setP354_3(String p354_3) {
        this.p354_3 = p354_3;
    }

    public String getP355_3() {
        return p355_3;
    }

    public void setP355_3(String p355_3) {
        this.p355_3 = p355_3;
    }

    public String getP355_3_o() {
        return p355_3_o;
    }

    public void setP355_3_o(String p355_3_o) {
        this.p355_3_o = p355_3_o;
    }

    public String getP354_4() {
        return p354_4;
    }

    public void setP354_4(String p354_4) {
        this.p354_4 = p354_4;
    }

    public String getP355_4() {
        return p355_4;
    }

    public void setP355_4(String p355_4) {
        this.p355_4 = p355_4;
    }

    public String getP355_4_o() {
        return p355_4_o;
    }

    public void setP355_4_o(String p355_4_o) {
        this.p355_4_o = p355_4_o;
    }

    public String getP354_5() {
        return p354_5;
    }

    public void setP354_5(String p354_5) {
        this.p354_5 = p354_5;
    }

    public String getP355_5() {
        return p355_5;
    }

    public void setP355_5(String p355_5) {
        this.p355_5 = p355_5;
    }

    public String getP355_5_o() {
        return p355_5_o;
    }

    public void setP355_5_o(String p355_5_o) {
        this.p355_5_o = p355_5_o;
    }

    public String getP354_6() {
        return p354_6;
    }

    public void setP354_6(String p354_6) {
        this.p354_6 = p354_6;
    }

    public String getP354_6_o() {
        return p354_6_o;
    }

    public void setP354_6_o(String p354_6_o) {
        this.p354_6_o = p354_6_o;
    }

    public String getP355_6() {
        return p355_6;
    }

    public void setP355_6(String p355_6) {
        this.p355_6 = p355_6;
    }

    public String getP355_6_o() {
        return p355_6_o;
    }

    public void setP355_6_o(String p355_6_o) {
        this.p355_6_o = p355_6_o;
    }

    public String getP356() {
        return p356;
    }

    public void setP356(String p356) {
        this.p356 = p356;
    }

    public String getP357_1() {
        return p357_1;
    }

    public void setP357_1(String p357_1) {
        this.p357_1 = p357_1;
    }

    public String getP357_2() {
        return p357_2;
    }

    public void setP357_2(String p357_2) {
        this.p357_2 = p357_2;
    }

    public String getP357_3() {
        return p357_3;
    }

    public void setP357_3(String p357_3) {
        this.p357_3 = p357_3;
    }

    public String getP357_4() {
        return p357_4;
    }

    public void setP357_4(String p357_4) {
        this.p357_4 = p357_4;
    }

    public String getP357_5() {
        return p357_5;
    }

    public void setP357_5(String p357_5) {
        this.p357_5 = p357_5;
    }

    public String getP357_6() {
        return p357_6;
    }

    public void setP357_6(String p357_6) {
        this.p357_6 = p357_6;
    }

    public String getP357_7() {
        return p357_7;
    }

    public void setP357_7(String p357_7) {
        this.p357_7 = p357_7;
    }

    public String getP357_8() {
        return p357_8;
    }

    public void setP357_8(String p357_8) {
        this.p357_8 = p357_8;
    }

    public String getP357_8_o() {
        return p357_8_o;
    }

    public void setP357_8_o(String p357_8_o) {
        this.p357_8_o = p357_8_o;
    }

    public String getP357_9() {
        return p357_9;
    }

    public void setP357_9(String p357_9) {
        this.p357_9 = p357_9;
    }

    public String getP358() {
        return p358;
    }

    public void setP358(String p358) {
        this.p358 = p358;
    }

    public String getP359() {
        return p359;
    }

    public void setP359(String p359) {
        this.p359 = p359;
    }

    public String getObs_i() {
        return obs_i;
    }

    public void setObs_i(String obs_i) {
        this.obs_i = obs_i;
    }

    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();

        contentValues.put(SQLConstantes.seccion_i_id,_id);
        contentValues.put(SQLConstantes.seccion_i_informante,idInformante);
        contentValues.put(SQLConstantes.seccion_i_numero_hogar,idHogar);
        contentValues.put(SQLConstantes.seccion_i_id_vivienda,idVivienda);
        contentValues.put(SQLConstantes.seccion_i_p347_1,p347_1);
        contentValues.put(SQLConstantes.seccion_i_p347_2,p347_2);
        contentValues.put(SQLConstantes.seccion_i_p347_3,p347_3);
        contentValues.put(SQLConstantes.seccion_i_p347_4,p347_4);
        contentValues.put(SQLConstantes.seccion_i_p347_5,p347_5);
        contentValues.put(SQLConstantes.seccion_i_p348_1,p348_1);
        contentValues.put(SQLConstantes.seccion_i_p348_2,p348_2);
        contentValues.put(SQLConstantes.seccion_i_p348_3,p348_3);
        contentValues.put(SQLConstantes.seccion_i_p348_4,p348_4);
        contentValues.put(SQLConstantes.seccion_i_p348_5,p348_5);
        contentValues.put(SQLConstantes.seccion_i_p348_6,p348_6);
        contentValues.put(SQLConstantes.seccion_i_p348_7,p348_7);
        contentValues.put(SQLConstantes.seccion_i_p348_8,p348_8);
        contentValues.put(SQLConstantes.seccion_i_p348_9,p348_9);
        contentValues.put(SQLConstantes.seccion_i_p348_10,p348_10);
        contentValues.put(SQLConstantes.seccion_i_p348_11,p348_11);
        contentValues.put(SQLConstantes.seccion_i_p348_11_o,p348_11_o);
        contentValues.put(SQLConstantes.seccion_i_p348_12,p348_12);
        contentValues.put(SQLConstantes.seccion_i_p348_13,p348_13);
        contentValues.put(SQLConstantes.seccion_i_p348_14,p348_14);
        contentValues.put(SQLConstantes.seccion_i_p348_i,p348_i);
        contentValues.put(SQLConstantes.seccion_i_p349,p349);
        contentValues.put(SQLConstantes.seccion_i_p350_1,p350_1);
        contentValues.put(SQLConstantes.seccion_i_p350_2,p350_2);
        contentValues.put(SQLConstantes.seccion_i_p350_3,p350_3);
        contentValues.put(SQLConstantes.seccion_i_p350_4,p350_4);
        contentValues.put(SQLConstantes.seccion_i_p350_5,p350_5);
        contentValues.put(SQLConstantes.seccion_i_p350_5_o,p350_5_o);
        contentValues.put(SQLConstantes.seccion_i_p351,p351);
        contentValues.put(SQLConstantes.seccion_i_p351a_1,p351a_1);
        contentValues.put(SQLConstantes.seccion_i_p351a_2,p351a_2);
        contentValues.put(SQLConstantes.seccion_i_p351a_3,p351a_3);
        contentValues.put(SQLConstantes.seccion_i_p351a_4,p351a_4);
        contentValues.put(SQLConstantes.seccion_i_p351a_5,p351a_5);
        contentValues.put(SQLConstantes.seccion_i_p351a_5_o,p351a_5_o);
        contentValues.put(SQLConstantes.seccion_i_p352,p352);
        contentValues.put(SQLConstantes.seccion_i_p352_o,p352_o);
        contentValues.put(SQLConstantes.seccion_i_p353_1,p353_1);
        contentValues.put(SQLConstantes.seccion_i_p353_2,p353_2);
        contentValues.put(SQLConstantes.seccion_i_p353_3,p353_3);
        contentValues.put(SQLConstantes.seccion_i_p353_4,p353_4);
        contentValues.put(SQLConstantes.seccion_i_p353_5,p353_5);
        contentValues.put(SQLConstantes.seccion_i_p353_6,p353_6);
        contentValues.put(SQLConstantes.seccion_i_p353_6_o,p353_6_o);
        contentValues.put(SQLConstantes.seccion_i_p354_1,p354_1);
        contentValues.put(SQLConstantes.seccion_i_p355_1,p355_1);
        contentValues.put(SQLConstantes.seccion_i_p355_1_o,p355_1_o);
        contentValues.put(SQLConstantes.seccion_i_p354_2,p354_2);
        contentValues.put(SQLConstantes.seccion_i_p355_2,p355_2);
        contentValues.put(SQLConstantes.seccion_i_p355_2_o,p355_2_o);
        contentValues.put(SQLConstantes.seccion_i_p354_3,p354_3);
        contentValues.put(SQLConstantes.seccion_i_p355_3,p355_3);
        contentValues.put(SQLConstantes.seccion_i_p355_3_o,p355_3_o);
        contentValues.put(SQLConstantes.seccion_i_p354_4,p354_4);
        contentValues.put(SQLConstantes.seccion_i_p355_4,p355_4);
        contentValues.put(SQLConstantes.seccion_i_p355_4_o,p355_4_o);
        contentValues.put(SQLConstantes.seccion_i_p354_5,p354_5);
        contentValues.put(SQLConstantes.seccion_i_p355_5,p355_5);
        contentValues.put(SQLConstantes.seccion_i_p355_5_o,p355_5_o);
        contentValues.put(SQLConstantes.seccion_i_p354_6,p354_6);
        contentValues.put(SQLConstantes.seccion_i_p354_6_o,p354_6_o);
        contentValues.put(SQLConstantes.seccion_i_p355_6,p355_6);
        contentValues.put(SQLConstantes.seccion_i_p355_6_o,p355_6_o);
        contentValues.put(SQLConstantes.seccion_i_p356,p356);
        contentValues.put(SQLConstantes.seccion_i_p357_1,p357_1);
        contentValues.put(SQLConstantes.seccion_i_p357_2,p357_2);
        contentValues.put(SQLConstantes.seccion_i_p357_3,p357_3);
        contentValues.put(SQLConstantes.seccion_i_p357_4,p357_4);
        contentValues.put(SQLConstantes.seccion_i_p357_5,p357_5);
        contentValues.put(SQLConstantes.seccion_i_p357_6,p357_6);
        contentValues.put(SQLConstantes.seccion_i_p357_7,p357_7);
        contentValues.put(SQLConstantes.seccion_i_p357_8,p357_8);
        contentValues.put(SQLConstantes.seccion_i_p357_8_o,p357_8_o);
        contentValues.put(SQLConstantes.seccion_i_p357_9,p357_9);
        contentValues.put(SQLConstantes.seccion_i_p358,p358);
        contentValues.put(SQLConstantes.seccion_i_p359,p359);
        contentValues.put(SQLConstantes.seccion_i_obs_i,obs_i);
        return contentValues;
    }
}
