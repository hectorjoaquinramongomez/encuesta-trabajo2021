package com.example.ricindigus.trabajo2020.modelo.pojos;

import android.content.ContentValues;

import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;

public class Modulo3 {
    private String _id;
    private String idInformante;
    private String idHogar;
    private String idVivienda;
    //agregado//
    private String p301;
    private String p301_203;
    private String p301_203_g;
    private String p301_ce;
    private String p302;
    private String p303_1;
    private String p303_2;
    private String p303_3;
    private String p303_4;
    private String p303_5;
    private String p303_6;
    private String p303_7;
    private String p303_8;
    private String p303_9;
    private String p303_10;
    private String p303_11;
    private String p303_11_O;
    private String p304;
    private String p304_c;
    private String p304_n;
    private String p304a_1;
    private String p304b_1;
    private String p304a_2;
    private String p304b_2;
    private String p305_1;
    private String p305_1a;
    private String p305_2;
    private String p305_2a;
    private String p305_3;
    private String p305_3a;
    private String p305_4;
    private String p305_4_o;
    private String p305_4a;
    private String p306_1;
    private String p306_2;
    private String OBS_C;

    private String COB300;

    public Modulo3(){
        _id = "";
        idHogar = "";
        idVivienda = "";
        idInformante = "";
        p301 = "";
        p301_203 = "";
        p301_203_g = "";
        p301_ce = "";
        p302 = "";
        p303_1 = "";
        p303_2 = "";
        p303_3 = "";
        p303_4 = "";
        p303_5 = "";
        p303_6 = "";
        p303_7 = "";
        p303_8 = "";
        p303_9 = "";
        p303_10 = "";
        p303_11 = "";
        p303_11_O = "";
        p304 = "";
        p304_c = "";
        p304_n = "";
        p304a_1 = "";
        p304b_1 = "";
        p304a_2 = "";
        p304b_2 = "";
        p305_1 = "";
        p305_1a = "";
        p305_2 = "";
        p305_2a = "";
        p305_3 = "";
        p305_3a = "";
        p305_4 = "";
        p305_4_o = "";
        p305_4a = "";
        p306_1 = "";
        p306_2 = "";
        OBS_C = "";
        COB300= "0";
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIdInformante() {
        return idInformante;
    }

    public void setIdInformante(String idInformante) {
        this.idInformante = idInformante;
    }

    public String getIdHogar() {
        return idHogar;
    }

    public void setIdHogar(String idHogar) {
        this.idHogar = idHogar;
    }

    public String getIdVivienda() {
        return idVivienda;
    }

    public void setIdVivienda(String idVivienda) {
        this.idVivienda = idVivienda;
    }

    /*
    public String getC3_p301_d() {
        return c3_p301_d;
    }

    public void setC3_p301_d(String c3_p301_d) {
        this.c3_p301_d = c3_p301_d;
    }

    public String getC3_p301_m() {
        return c3_p301_m;
    }

    public void setC3_p301_m(String c3_p301_m) {
        this.c3_p301_m = c3_p301_m;
    }

    public String getC3_p301_a() {
        return c3_p301_a;
    }

    public void setC3_p301_a(String c3_p301_a) {
        this.c3_p301_a = c3_p301_a;
    }

    public String getC3_p302() {
        return c3_p302;
    }

    public void setC3_p302(String c3_p302) {
        this.c3_p302 = c3_p302;
    }

    public String getC3_p303_m() {
        return c3_p303_m;
    }

    public void setC3_p303_m(String c3_p303_m) {
        this.c3_p303_m = c3_p303_m;
    }

    public String getC3_p303_a() {
        return c3_p303_a;
    }

    public void setC3_p303_a(String c3_p303_a) {
        this.c3_p303_a = c3_p303_a;
    }

    public String getC3_p303_no_nacio() {
        return c3_p303_no_nacio;
    }

    public void setC3_p303_no_nacio(String c3_p303_no_nacio) {
        this.c3_p303_no_nacio = c3_p303_no_nacio;
    }

    public String getC3_p304() {
        return c3_p304;
    }

    public void setC3_p304(String c3_p304) {
        this.c3_p304 = c3_p304;
    }

    public String getC3_p305() {
        return c3_p305;
    }

    public void setC3_p305(String c3_p305) {
        this.c3_p305 = c3_p305;
    }

    public String getC3_p305_o() {
        return c3_p305_o;
    }

    public void setC3_p305_o(String c3_p305_o) {
        this.c3_p305_o = c3_p305_o;
    }

    public String getC3_p306() {
        return c3_p306;
    }

    public void setC3_p306(String c3_p306) {
        this.c3_p306 = c3_p306;
    }

    public String getC3_p306_o() {
        return c3_p306_o;
    }

    public void setC3_p306_o(String c3_p306_o) {
        this.c3_p306_o = c3_p306_o;
    }

    public String getC3_p307_d() {
        return c3_p307_d;
    }

    public void setC3_p307_d(String c3_p307_d) {
        this.c3_p307_d = c3_p307_d;
    }

    public String getC3_p307_m() {
        return c3_p307_m;
    }

    public void setC3_p307_m(String c3_p307_m) {
        this.c3_p307_m = c3_p307_m;
    }

    public String getC3_p307_a() {
        return c3_p307_a;
    }

    public void setC3_p307_a(String c3_p307_a) {
        this.c3_p307_a = c3_p307_a;
    }

    public String getC3_p308_e() {
        return c3_p308_e;
    }

    public void setC3_p308_e(String c3_p308_e) {
        this.c3_p308_e = c3_p308_e;
    }

    public String getC3_p308_m() {
        return c3_p308_m;
    }

    public void setC3_p308_m(String c3_p308_m) {
        this.c3_p308_m = c3_p308_m;
    }

    public String getC3_p308_e_seleccion() {
        return c3_p308_e_seleccion;
    }

    public void setC3_p308_e_seleccion(String c3_p308_e_seleccion) {
        this.c3_p308_e_seleccion = c3_p308_e_seleccion;
    }

    public String getC3_p308_m_seleccion() {
        return c3_p308_m_seleccion;
    }

    public void setC3_p308_m_seleccion(String c3_p308_m_seleccion) {
        this.c3_p308_m_seleccion = c3_p308_m_seleccion;
    }

    public String getC3_p310_1() {
        return c3_p310_1;
    }

    public void setC3_p310_1(String c3_p310_1) {
        this.c3_p310_1 = c3_p310_1;
    }

    public String getC3_p310_2() {
        return c3_p310_2;
    }

    public void setC3_p310_2(String c3_p310_2) {
        this.c3_p310_2 = c3_p310_2;
    }

    public String getC3_p310_3() {
        return c3_p310_3;
    }

    public void setC3_p310_3(String c3_p310_3) {
        this.c3_p310_3 = c3_p310_3;
    }

    public String getC3_p310_4() {
        return c3_p310_4;
    }

    public void setC3_p310_4(String c3_p310_4) {
        this.c3_p310_4 = c3_p310_4;
    }

    public String getC3_p311() {
        return c3_p311;
    }

    public void setC3_p311(String c3_p311) {
        this.c3_p311 = c3_p311;
    }

    public String getC3_p312_dist() {
        return c3_p312_dist;
    }

    public void setC3_p312_dist(String c3_p312_dist) {
        this.c3_p312_dist = c3_p312_dist;
    }

    public String getC3_p312_prov() {
        return c3_p312_prov;
    }

    public void setC3_p312_prov(String c3_p312_prov) {
        this.c3_p312_prov = c3_p312_prov;
    }

    public String getC3_p312_dep() {
        return c3_p312_dep;
    }

    public void setC3_p312_dep(String c3_p312_dep) {
        this.c3_p312_dep = c3_p312_dep;
    }

    public String getC3_p313() {
        return c3_p313;
    }

    public void setC3_p313(String c3_p313) {
        this.c3_p313 = c3_p313;
    }

    public String getC3_p314() {
        return c3_p314;
    }

    public void setC3_p314(String c3_p314) {
        this.c3_p314 = c3_p314;
    }

    public String getC3_p314_o() {
        return c3_p314_o;
    }

    public void setC3_p314_o(String c3_p314_o) {
        this.c3_p314_o = c3_p314_o;
    }

    public String getC3_p315_1() {
        return c3_p315_1;
    }

    public void setC3_p315_1(String c3_p315_1) {
        this.c3_p315_1 = c3_p315_1;
    }

    public String getC3_p315_2() {
        return c3_p315_2;
    }

    public void setC3_p315_2(String c3_p315_2) {
        this.c3_p315_2 = c3_p315_2;
    }

    public String getC3_p315_3() {
        return c3_p315_3;
    }

    public void setC3_p315_3(String c3_p315_3) {
        this.c3_p315_3 = c3_p315_3;
    }

    public String getC3_p315_4() {
        return c3_p315_4;
    }

    public void setC3_p315_4(String c3_p315_4) {
        this.c3_p315_4 = c3_p315_4;
    }

    public String getC3_p315_5() {
        return c3_p315_5;
    }

    public void setC3_p315_5(String c3_p315_5) {
        this.c3_p315_5 = c3_p315_5;
    }

    public String getC3_p315_6() {
        return c3_p315_6;
    }

    public void setC3_p315_6(String c3_p315_6) {
        this.c3_p315_6 = c3_p315_6;
    }

    public String getC3_p315_7() {
        return c3_p315_7;
    }

    public void setC3_p315_7(String c3_p315_7) {
        this.c3_p315_7 = c3_p315_7;
    }

    public String getC3_p315_8() {
        return c3_p315_8;
    }

    public void setC3_p315_8(String c3_p315_8) {
        this.c3_p315_8 = c3_p315_8;
    }

    public String getC3_p315_9() {
        return c3_p315_9;
    }

    public void setC3_p315_9(String c3_p315_9) {
        this.c3_p315_9 = c3_p315_9;
    }

    public String getC3_p315_10() {
        return c3_p315_10;
    }

    public void setC3_p315_10(String c3_p315_10) {
        this.c3_p315_10 = c3_p315_10;
    }

    public String getC3_p315_10_o() {
        return c3_p315_10_o;
    }

    public void setC3_p315_10_o(String c3_p315_10_o) {
        this.c3_p315_10_o = c3_p315_10_o;
    }

    public String getC3_p316() {
        return c3_p316;
    }

    public void setC3_p316(String c3_p316) {
        this.c3_p316 = c3_p316;
    }

    public String getC3_p316_o() {
        return c3_p316_o;
    }

    public void setC3_p316_o(String c3_p316_o) {
        this.c3_p316_o = c3_p316_o;
    }

    public String getC3_p317() {
        return c3_p317;
    }

    public void setC3_p317(String c3_p317) {
        this.c3_p317 = c3_p317;
    }

    public String getC3_p318() {
        return c3_p318;
    }

    public void setC3_p318(String c3_p318) {
        this.c3_p318 = c3_p318;
    }

    public String getObs_cap3() {
        return obs_cap3;
    }

    public void setObs_cap3(String obs_cap3) {
        this.obs_cap3 = obs_cap3;
    }
     */

    public String getP301() {
        return p301;
    }

    public void setP301(String p301) {
        this.p301 = p301;
    }

    public String getP301_203() {
        return p301_203;
    }

    public void setP301_203(String p301_203) {
        this.p301_203 = p301_203;
    }

    public String getP301_203_g() {
        return p301_203_g;
    }

    public void setP301_203_g(String p301_203_g) {
        this.p301_203_g = p301_203_g;
    }

    public String getP301_ce() {
        return p301_ce;
    }

    public void setP301_ce(String p301_ce) {
        this.p301_ce = p301_ce;
    }

    public String getP302() {
        return p302;
    }

    public void setP302(String p302) {
        this.p302 = p302;
    }

    public String getP303_1() {
        return p303_1;
    }

    public void setP303_1(String p303_1) {
        this.p303_1 = p303_1;
    }

    public String getP303_2() {
        return p303_2;
    }

    public void setP303_2(String p303_2) {
        this.p303_2 = p303_2;
    }

    public String getP303_3() {
        return p303_3;
    }

    public void setP303_3(String p303_3) {
        this.p303_3 = p303_3;
    }

    public String getP303_4() {
        return p303_4;
    }

    public void setP303_4(String p303_4) {
        this.p303_4 = p303_4;
    }

    public String getP303_5() {
        return p303_5;
    }

    public void setP303_5(String p303_5) {
        this.p303_5 = p303_5;
    }

    public String getP303_6() {
        return p303_6;
    }

    public void setP303_6(String p303_6) {
        this.p303_6 = p303_6;
    }

    public String getP303_7() {
        return p303_7;
    }

    public void setP303_7(String p303_7) {
        this.p303_7 = p303_7;
    }

    public String getP303_8() {
        return p303_8;
    }

    public void setP303_8(String p303_8) {
        this.p303_8 = p303_8;
    }

    public String getP303_9() {
        return p303_9;
    }

    public void setP303_9(String p303_9) {
        this.p303_9 = p303_9;
    }

    public String getP303_10() {
        return p303_10;
    }

    public void setP303_10(String p303_10) {
        this.p303_10 = p303_10;
    }

    public String getP303_11() {
        return p303_11;
    }

    public void setP303_11(String p303_11) {
        this.p303_11 = p303_11;
    }

    public String getP303_11_O() {
        return p303_11_O;
    }

    public void setP303_11_O(String p303_11_O) {
        this.p303_11_O = p303_11_O;
    }

    public String getP304() {
        return p304;
    }

    public void setP304(String p304) {
        this.p304 = p304;
    }

    public String getP304_c() {
        return p304_c;
    }

    public void setP304_c(String p304_c) {
        this.p304_c = p304_c;
    }

    public String getP304_n() {
        return p304_n;
    }

    public void setP304_n(String p304_n) {
        this.p304_n = p304_n;
    }

    public String getP304a_1() {
        return p304a_1;
    }

    public void setP304a_1(String p304a_1) {
        this.p304a_1 = p304a_1;
    }

    public String getP304b_1() {
        return p304b_1;
    }

    public void setP304b_1(String p304b_1) {
        this.p304b_1 = p304b_1;
    }

    public String getP304a_2() {
        return p304a_2;
    }

    public void setP304a_2(String p304a_2) {
        this.p304a_2 = p304a_2;
    }

    public String getP304b_2() {
        return p304b_2;
    }

    public void setP304b_2(String p304b_2) {
        this.p304b_2 = p304b_2;
    }

    public String getP305_1() {
        return p305_1;
    }

    public void setP305_1(String p305_1) {
        this.p305_1 = p305_1;
    }

    public String getP305_1a() {
        return p305_1a;
    }

    public void setP305_1a(String p305_1a) {
        this.p305_1a = p305_1a;
    }

    public String getP305_2() {
        return p305_2;
    }

    public void setP305_2(String p305_2) {
        this.p305_2 = p305_2;
    }

    public String getP305_2a() {
        return p305_2a;
    }

    public void setP305_2a(String p305_2a) {
        this.p305_2a = p305_2a;
    }

    public String getP305_3() {
        return p305_3;
    }

    public void setP305_3(String p305_3) {
        this.p305_3 = p305_3;
    }

    public String getP305_3a() {
        return p305_3a;
    }

    public void setP305_3a(String p305_3a) {
        this.p305_3a = p305_3a;
    }

    public String getP305_4() {
        return p305_4;
    }

    public void setP305_4(String p305_4) {
        this.p305_4 = p305_4;
    }

    public String getP305_4_o() {
        return p305_4_o;
    }

    public void setP305_4_o(String p305_4_o) {
        this.p305_4_o = p305_4_o;
    }

    public String getP305_4a() {
        return p305_4a;
    }

    public void setP305_4a(String p305_4a) {
        this.p305_4a = p305_4a;
    }

    public String getP306_1() {
        return p306_1;
    }

    public void setP306_1(String p306_1) {
        this.p306_1 = p306_1;
    }

    public String getP306_2() {
        return p306_2;
    }

    public void setP306_2(String p306_2) {
        this.p306_2 = p306_2;
    }

    public String getOBS_C() {
        return OBS_C;
    }

    public void setOBS_C(String OBS_C) {
        this.OBS_C = OBS_C;
    }

    public String getCOB300() {
        return COB300;
    }

    public void setCOB300(String COB300) {
        this.COB300 = COB300;
    }

    public ContentValues toValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLConstantes.modulo3_id,_id);
        contentValues.put(SQLConstantes.modulo3_id_informante,idInformante);
        contentValues.put(SQLConstantes.modulo3_id_hogar,idHogar);
        contentValues.put(SQLConstantes.modulo3_id_vivienda,idVivienda);
        /*
         contentValues.put(SQLConstantes.modulo3_c3_p301_d,c3_p301_d);
        contentValues.put(SQLConstantes.modulo3_c3_p301_m,c3_p301_m);
        contentValues.put(SQLConstantes.modulo3_c3_p301_a,c3_p301_a);
        contentValues.put(SQLConstantes.modulo3_c3_p302,c3_p302);
        contentValues.put(SQLConstantes.modulo3_c3_p303_m, c3_p303_m);
        contentValues.put(SQLConstantes.modulo3_c3_p303_a, c3_p303_a);
        contentValues.put(SQLConstantes.modulo3_c3_p303_no_nacio, c3_p303_no_nacio);
        contentValues.put(SQLConstantes.modulo3_c3_p304,c3_p304);
        contentValues.put(SQLConstantes.modulo3_c3_p305,c3_p305);
        contentValues.put(SQLConstantes.modulo3_c3_p305_o,c3_p305_o);
        contentValues.put(SQLConstantes.modulo3_c3_p306,c3_p306);
        contentValues.put(SQLConstantes.modulo3_c3_p306_o,c3_p306_o);
        contentValues.put(SQLConstantes.modulo3_c3_p307_d,c3_p307_d);
        contentValues.put(SQLConstantes.modulo3_c3_p307_m,c3_p307_m);
        contentValues.put(SQLConstantes.modulo3_c3_p307_a,c3_p307_a);
        contentValues.put(SQLConstantes.modulo3_c3_p308_e,c3_p308_e);
        contentValues.put(SQLConstantes.modulo3_c3_p308_m,c3_p308_m);
        contentValues.put(SQLConstantes.modulo3_c3_p308_e_seleccion,c3_p308_e_seleccion);
        contentValues.put(SQLConstantes.modulo3_c3_p308_m_seleccion,c3_p308_m_seleccion);
        contentValues.put(SQLConstantes.modulo3_c3_p310_1,c3_p310_1);
        contentValues.put(SQLConstantes.modulo3_c3_p310_2,c3_p310_2);
        contentValues.put(SQLConstantes.modulo3_c3_p310_3,c3_p310_3);
        contentValues.put(SQLConstantes.modulo3_c3_p310_4,c3_p310_4);
        contentValues.put(SQLConstantes.modulo3_c3_p311,c3_p311);
        contentValues.put(SQLConstantes.modulo3_c3_p312_dist,c3_p312_dist);
        contentValues.put(SQLConstantes.modulo3_c3_p312_prov,c3_p312_prov);
        contentValues.put(SQLConstantes.modulo3_c3_p312_dep,c3_p312_dep);
        contentValues.put(SQLConstantes.modulo3_c3_p313,c3_p313);
        contentValues.put(SQLConstantes.modulo3_c3_p314,c3_p314);
        contentValues.put(SQLConstantes.modulo3_c3_p314_o,c3_p314_o);
        contentValues.put(SQLConstantes.modulo3_c3_p315_1,c3_p315_1);
        contentValues.put(SQLConstantes.modulo3_c3_p315_2,c3_p315_2);
        contentValues.put(SQLConstantes.modulo3_c3_p315_3,c3_p315_3);
        contentValues.put(SQLConstantes.modulo3_c3_p315_4,c3_p315_4);
        contentValues.put(SQLConstantes.modulo3_c3_p315_5,c3_p315_5);
        contentValues.put(SQLConstantes.modulo3_c3_p315_6,c3_p315_6);
        contentValues.put(SQLConstantes.modulo3_c3_p315_7,c3_p315_7);
        contentValues.put(SQLConstantes.modulo3_c3_p315_8,c3_p315_8);
        contentValues.put(SQLConstantes.modulo3_c3_p315_9,c3_p315_9);
        contentValues.put(SQLConstantes.modulo3_c3_p315_10,c3_p315_10);
        contentValues.put(SQLConstantes.modulo3_c3_p315_10_o,c3_p315_10_o);
        contentValues.put(SQLConstantes.modulo3_c3_p316,c3_p316);
        contentValues.put(SQLConstantes.modulo3_c3_p316_o,c3_p316_o);
        contentValues.put(SQLConstantes.modulo3_c3_p317,c3_p317);
        contentValues.put(SQLConstantes.modulo3_c3_p318,c3_p318);
        contentValues.put(SQLConstantes.modulo3_obs_cap3,obs_cap3);
         */
        //agregado//
        contentValues.put(SQLConstantes.seccion_c_p301_nivel,p301);
        contentValues.put(SQLConstantes.seccion_c_p301_anio,p301_203);
        contentValues.put(SQLConstantes.seccion_c_p301_grado,p301_ce);
        contentValues.put(SQLConstantes.seccion_c_p301a,p301_203_g);
        contentValues.put(SQLConstantes.seccion_c_p302,p302);
        contentValues.put(SQLConstantes.seccion_c_p303_1,p303_1);
        contentValues.put(SQLConstantes.seccion_c_p303_2,p303_2);
        contentValues.put(SQLConstantes.seccion_c_p303_3,p303_3);
        contentValues.put(SQLConstantes.seccion_c_p303_4,p303_4);
        contentValues.put(SQLConstantes.seccion_c_p303_5,p303_5);
        contentValues.put(SQLConstantes.seccion_c_p303_6,p303_6);
        contentValues.put(SQLConstantes.seccion_c_p303_7,p303_7);
        contentValues.put(SQLConstantes.seccion_c_p303_8,p303_8);
        contentValues.put(SQLConstantes.seccion_c_p303_9,p303_9);
        contentValues.put(SQLConstantes.seccion_c_p303_10,p303_10);
        contentValues.put(SQLConstantes.seccion_c_p303_11,p303_11);
        contentValues.put(SQLConstantes.seccion_c_p303_11_o,p303_11_O);

        contentValues.put(SQLConstantes.seccion_c_p304,p304);
        contentValues.put(SQLConstantes.seccion_c_p304_c,p304_c);
        contentValues.put(SQLConstantes.seccion_c_p304_n,p304_n);
        contentValues.put(SQLConstantes.seccion_c_p304a_1,p304a_1);
        contentValues.put(SQLConstantes.seccion_c_p304b_1,p304b_1);
        contentValues.put(SQLConstantes.seccion_c_p304a_2,p304a_2);
        contentValues.put(SQLConstantes.seccion_c_p304b_2,p304b_2);
        contentValues.put(SQLConstantes.seccion_c_p305_1,p305_1);
        contentValues.put(SQLConstantes.seccion_c_p305_1a,p305_1a);
        contentValues.put(SQLConstantes.seccion_c_p305_2,p305_2);
        contentValues.put(SQLConstantes.seccion_c_p305_2a,p305_2a);
        contentValues.put(SQLConstantes.seccion_c_p305_3,p305_3);
        contentValues.put(SQLConstantes.seccion_c_p305_3a,p305_3a);
        contentValues.put(SQLConstantes.seccion_c_p305_4,p305_4);
        contentValues.put(SQLConstantes.seccion_c_p305_4_o,p305_4_o);
        contentValues.put(SQLConstantes.seccion_c_p305_4a,p305_4a);
        contentValues.put(SQLConstantes.seccion_c_p306_1,p306_1);
        contentValues.put(SQLConstantes.seccion_c_p306_2,p306_2);
        contentValues.put(SQLConstantes.seccion_c_obs_c,OBS_C);
        contentValues.put(SQLConstantes.modulo3_COB300,COB300);
        return contentValues;
    }
}
