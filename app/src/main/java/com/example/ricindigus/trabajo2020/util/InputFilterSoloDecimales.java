package com.example.ricindigus.trabajo2020.util;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//https://stackoverflow.com/questions/5357455/limit-decimal-places-in-android-edittext/5368816
//https://stackoverflow.com/questions/36629541/input-filter-regex-for-two-decimals
//https://www.tutorialspoint.com/how-to-limit-decimal-places-in-android-edittext
public class InputFilterSoloDecimales implements InputFilter {

    Pattern mPattern;

    public InputFilterSoloDecimales(int digitsBeforeZero,int digitsAfterZero) {
        //mPattern=Pattern.compile("[0-9]{0," + (digitsBeforeZero-1) + "}+((\\.[0-9]{0," + (digitsAfterZero-1) + "})?)||(\\.)?");
        //mPattern=Pattern.compile("^[0-9]+([.][0-9]{0,2})?$");
        mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
    }

    @Override
    public CharSequence filter(CharSequence src, int start,
                               int end, Spanned dest, int dstart, int dend) {
        Matcher matcher = mPattern.matcher(dest);
        if (!matcher.matches())
            return "";
        return null;
    }
}