package com.example.ricindigus.trabajo2020.util;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

//Patrón decorator
public class EditTextDecorator {

    private final EditText componente;

    public EditTextDecorator(EditText componente) {
        this.componente = componente;
    }

    public void admitirSolo_LetrasMayusculas_Numeros_Espacio_Longitud(int longitud) {
        componente.setFilters(new InputFilter[]{new InputFilter.AllCaps()
                ,new InputFilter.LengthFilter(longitud)
                ,new InputFilterSoloLetras_Numeros1_9_Espacio()
        });
    }

    public void admitirSolo_Numeros_Longitud(int longitud) {
        componente.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)
                ,new InputFilterSoloNumeros1_9()
        });
    }

    public void admitirSolo_Decimales(int a, int b) {
        componente.setFilters(new InputFilter[]{new InputFilterSoloDecimales(a,b)});
    }

    public boolean validar(ValidarListener listener) {
        String msj = listener.validar(componente.getText().toString());
        if(msj!=null) {
            msj = msj.trim();
            if(!msj.isEmpty())
                mostrarMensaje(msj);
        }
        return msj==null;
    }

    private void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(componente.getContext());
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public boolean esVacio() {
        return componente.getText().toString().trim().isEmpty();
    }

    public void agregarRestriccion(final ValidarListener restriccion) {
        componente.addTextChangedListener(new TextWatcher() {
            private String s0;
            private String s1;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //Log.d("beforeTextChanged","d: "+s);
                s0 = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //Log.d("onTextChanged","d: "+s);
                s1 = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Log.d("afterTextChanged","d: "+s);
                if (!esVacio() && !s0.equals(s1) && !validar(restriccion))
                    componente.setText(s0.toString());
            }
        });
    }


}
