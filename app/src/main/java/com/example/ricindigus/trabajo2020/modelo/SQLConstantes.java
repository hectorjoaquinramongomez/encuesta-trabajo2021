package com.example.ricindigus.trabajo2020.modelo;

public class SQLConstantes {
    public static String DB_PATH = "/data/data/com.example.ricindigus.trabajo2020/databases/";
    public static String DB_NAME = "dbtrabajo.sqlite";

    public static String tablamarco = "marco";
    public static String tablausuario = "usuarios";
    public static String tablapaises = "paises";
    public static String tablarutas = "rutas";


    public static String tablacaratula = "caratula";
    public static String tablahogares = "hogares";
    public static String tablavisitasencuestador = "visitas_encuestador";
    public static String tablaresultadoencuestador = "resultado_encuestador";
    public static String tablaresultadosupervisor= "resultado_supervisor";
    public static String tablavisitassupervisor = "visitas_supervisor";
    public static String tablafuncionarios = "funcionarios";


    public static String tablamodulo1v = "modulo1v";
    public static String tablamodulo1h = "modulo1h";
    public static String tablaresidentes = "tablaresidentes";
    public static String tablamodulo3 = "modulo3";
    public static String tablam3p309rutas = "m3_p309_rutas";
    public static String tablam3p318personas = "m3_p318_personas";
    public static String tablamodulo4 = "modulo4";
    public static String tablamodulo5 = "modulo5";
    public static String tablamodulo6 = "modulo6";
    public static String tablamodulo7 = "modulo7";
    public static String tablamodulo8 = "modulo8";
    public static String tablamodulo9 = "modulo9";


    public static String tablamunicipios = "municipios";
    public static String tablaestados = "estados";
    public static String tablaubigeo = "ubigeo";
    public static String tablalayouts = "layouts";
    public static String tablafragments= "fragments";
    public static String tablacoberturafragments= "cobertura_fragments";
    public static String tablafragmentsvivienda= "fragments_vivienda";
    public static String tablafragmentshogar= "fragments_hogar";







    /**
     * TABLA MARCO
     * */
    public static final String marco_id = "_id";
    public static final String marco_anio = "anio";
    public static final String marco_mes = "mes";
    public static final String marco_periodo = "periodo";
    public static final String marco_zona = "zona";
    public static final String marco_ccdd = "ccdd";
    public static final String marco_departamento = "departamento";
    public static final String marco_ccpp = "ccpp";
    public static final String marco_provincia = "provincia";
    public static final String marco_ccdi = "ccdi";
    public static final String marco_distrito = "distrito";
    public static final String marco_codccpp = "codccpp";
    public static final String marco_nomccpp = "nomccpp";
    public static final String marco_conglomerado = "conglomerado";
    public static final String marco_norden = "norden";
    public static final String marco_manzana_id = "manzana_id";
    public static final String marco_tipvia = "tipvia";
    public static final String marco_nomvia = "nomvia";
    public static final String marco_nropta = "nropta";
    public static final String marco_lote = "lote";
    public static final String marco_piso = "piso";
    public static final String marco_mza = "mza";
    public static final String marco_block = "block";
    public static final String marco_interior = "interior";
    public static final String marco_usuario_id = "usuario_id";
    public static final String marco_usuario = "usuario";
    public static final String marco_nombre = "nombre";
    public static final String marco_dni = "dni";
    public static final String marco_usuario_sup_id = "usuario_sup_id";
    public static final String marco_estado = "estado";



    /**
     * TABLA UBIGEO
     * */

    public static String ubigeo_id = "_id";
    public static String ubigeo_cod_departamento = "cod_departamento";
    public static String ubigeo_nom_departamento = "nom_departamento";
    public static String ubigeo_cod_provincia = "cod_provincia";
    public static String ubigeo_nom_provincia = "nom_provincia";
    public static String ubigeo_cod_distrito = "cod_distrito";
    public static String ubigeo_nom_distrito = "nom_distrito";
    public static String ubigeo_descripcion = "descripcion";


    /**
     * TABLA MUNICIPIOS
     * */

    public static String municipios_id = "_id";
    public static String municipios_cod_municipio = "cod_municipio";
    public static String municipios_nom_municipio = "nom_municipio";
    public static String municipios_num_municipio = "num_municipio";
    public static String municipios_cod_estado = "cod_estado";
    public static String municipios_nom_estado = "nom_estado";

    /**
     * TABLA ESTADOS
     * */

    public static String estado_id = "_id";
    public static String estado_nombre = "nombre";
    public static String estado_numero = "numero";


    /**
     * TABLA USUARIOS
     * */

    public static String usuario_id = "_id";
    public static String usuario_usuario = "usuario";
    public static String usuario_clave = "clave";
    public static String usuario_dni = "dni";
    public static String usuario_nombre = "nombre";
    public static String usuario_cargo_id= "cargo_id";


    /**
     * TABLA PAISES
     * */

    public static String paises_id = "_id";
    public static String paises_numero = "numero";
    public static String paises_nombre = "nombre";

    /**
     * TABLA RUTAS PAISES
     * */

    public static String rutas_id = "_id";
    public static String rutas_numero = "numero";
    public static String rutas_nombre = "nombre";

    /**
     * TABLA CARATULA
     * */

    public static final String caratula_id = "_id";
    public static final String caratula_anio = "anio";
    public static final String caratula_mes = "mes";
    public static final String caratula_periodo = "periodo";
    public static final String caratula_conglomerado = "conglomerado";
    public static final String caratula_nom_dep = "nom_dep";
    public static final String caratula_nom_prov = "nom_prov";
    public static final String caratula_nom_dist = "nom_dist";
    public static final String caratula_nom_ccpp = "nom_ccpp";
    public static final String caratula_zona = "zona";
    public static final String caratula_manzana_id = "manzana_id";
    public static final String caratula_vivienda = "vivienda";
    public static final String caratula_latitud = "latitud";
    public static final String caratula_longitud = "longitud";
    public static final String caratula_tipvia = "tipvia";
    public static final String caratula_tipvia_o = "tipvia_o";
    public static final String caratula_nomvia = "nomvia";
    public static final String caratula_nropta = "nropta";
    public static final String caratula_block = "block";
    public static final String caratula_interior = "interior";
    public static final String caratula_piso = "piso";
    public static final String caratula_mza = "mza";
    public static final String caratula_lote = "lote";
    public static final String caratula_km = "km";
    public static final String caratula_telefono = "telefono";
    //public static final String caratula_telefono_check = "telefono_check";
    public static final String caratula_t_hogar = "t_hogar";
    public static final String caratula_usuario = "usuario";
    public static final String caratula_observaciones = "observaciones";
    public static final String caratula_cobertura = "cobertura";



    public static final String SQL_CREATE_TABLA_CARATULA =
            "CREATE TABLE " + tablacaratula + "(" +
                    caratula_id  + " INTEGER PRIMARY KEY, " +
                    caratula_anio + " TEXT," +
                    caratula_mes + " TEXT," +
                    caratula_periodo+ " TEXT," +
                    caratula_conglomerado + " TEXT," +
                    caratula_nom_dep + " TEXT," +
                    caratula_nom_prov  + " TEXT," +
                    caratula_nom_dist  + " TEXT," +
                    caratula_nom_ccpp  + " TEXT," +
                    caratula_zona  + " TEXT," +
                    caratula_manzana_id  + " TEXT," +
                    caratula_vivienda  + " TEXT," +
                    caratula_latitud + " TEXT," +
                    caratula_longitud  + " TEXT," +
                    caratula_tipvia  + " TEXT," +
                    caratula_tipvia_o  + " TEXT," +
                    caratula_nomvia  + " TEXT," +
                    caratula_nropta  + " TEXT," +
                    caratula_block  + " TEXT," +
                    caratula_interior + " TEXT," +
                    caratula_piso  + " TEXT," +
                    caratula_mza  + " TEXT," +
                    caratula_lote  + " TEXT," +
                    caratula_km  + " TEXT," +
                    caratula_telefono  + " TEXT," +
                    caratula_t_hogar  + " TEXT," +
                    caratula_usuario  + " TEXT," +
                    caratula_cobertura  + " TEXT," +
                    caratula_observaciones + " TEXT" + ");"
            ;

    /**
     * TABLA HOGARES
     * */

    public static final String hogar_id = "_id";
    public static final String hogar_id_vivienda = "id_vivienda";
    public static final String hogar_numero = "numero";
    public static final String hogar_nom_ape = "nom_ape";
    public static final String hogar_estado = "estado";
    public static final String hogar_nroviven = "nroviven";
    public static final String hogar_nropersonas = "nropersonas";
    public static final String hogar_vive = "vive";
    public static final String hogar_principal = "principal";
    public static final String hogar_cobertura = "cobertura";




    public static final String SQL_CREATE_TABLA_HOGARES =
            "CREATE TABLE " + tablahogares + "(" +
                    hogar_id  + " TEXT PRIMARY KEY," +
                    hogar_id_vivienda + " TEXT," +
                    hogar_numero + " TEXT," +
                    hogar_nom_ape + " TEXT," +
                    hogar_vive + " TEXT," +
                    hogar_nroviven + " TEXT," +
                    hogar_nropersonas + " TEXT," +
                    hogar_principal + " TEXT," +
                    hogar_cobertura + " TEXT," +
                    hogar_estado + " TEXT" + ");"
            ;

    /**
     * TABLA FRAGMENTS VIVIENDA
     * */

    public static final String fragments_vivienda_id = "_id";
    public static final String fragments_vivienda_caratula = "caratula";
    public static final String fragments_vivienda_hogares = "hogares";


    public static final String SQL_CREATE_TABLA_FRAGMENTS_VIVIENDA =
            "CREATE TABLE " + tablafragmentsvivienda + "(" +
                    fragments_vivienda_id  + " TEXT PRIMARY KEY," +
                    fragments_vivienda_caratula  + " TEXT," +
                    fragments_vivienda_hogares + " TEXT" + ");"
            ;

    /**
     * TABLA FRAGMENTS HOGARES
     * */

    public static final String fragments_hogar_id = "_id";
    public static final String fragments_hogar_id_vivienda = "id_vivienda";
    public static final String fragments_hogar_visitas_encuestador = "visitas_encuestador";
    public static final String fragments_hogar_visitas_supervisor = "visitas_supervisor";
    public static final String fragments_hogar_funcionarios = "funcionarios";
    public static final String fragments_hogar_p101p107= "p101p107";
    public static final String fragments_hogar_p108p113 = "p108p113";
    public static final String fragments_hogar_p201p207 = "p201p207";


    public static final String SQL_CREATE_TABLA_FRAGMENTS_HOGAR =
            "CREATE TABLE " + tablafragmentshogar + "(" +
                    fragments_hogar_id  + " TEXT PRIMARY KEY," +
                    fragments_hogar_id_vivienda  + " TEXT," +
                    fragments_hogar_visitas_encuestador  + " TEXT," +
                    fragments_hogar_visitas_supervisor  + " TEXT," +
                    fragments_hogar_funcionarios  + " TEXT," +
                    fragments_hogar_p101p107  + " TEXT," +
                    fragments_hogar_p108p113  + " TEXT," +
                    fragments_hogar_p201p207 + " TEXT" + ");"
            ;

    /**
     * TABLA FRAGMENTS ENCUESTADO
     * */

    public static final String fragments_id = "_id";
    public static final String fragments_id_vivienda = "id_vivienda";
    public static final String fragments_c_p001p003 = "p001p003";
    public static final String fragments_c_p004p006 = "p004p006";
    public static final String fragments_d_p007p011 = "p007p011";
    public static final String fragments_d_p012     = "p012";
    public static final String fragments_d_p013p014 = "p013p014";
    public static final String fragments_e_p015     = "p015";
    public static final String fragments_e_p016     = "p016";
    public static final String fragments_e_p017p020 = "p017p020";
    public static final String fragments_e_p021p025 = "p021p025";
    public static final String fragments_f_p026p029 = "p026p029";
    public static final String fragments_f_p030p033 = "p030p033";
    public static final String fragments_f_p034p036 = "p034p036";
    public static final String fragments_g_p038p039 = "p038p039";
    public static final String fragments_g_p040     = "p040";
    public static final String fragments_h_p041p043 = "p041p043";
    public static final String fragments_h_p044p046 = "p044p046";
    public static final String fragments_i_p047p048 = "p047p048";
    public static final String fragments_i_p049p051 = "p049p051";
    public static final String fragments_i_p052p053 = "p052p053";
    public static final String fragments_i_p054p056 = "p054p056";
    public static final String fragments_i_p057p059 = "p057p059";


    public static final String fragments_p301p305 = "p301p305";
    public static final String fragments_p306p308 = "p306p308";
    public static final String fragments_p309 = "p309";
    public static final String fragments_p310p312 = "p310p312";
    public static final String fragments_p313p317 = "p313p317";
    public static final String fragments_p318 = "p318";
    public static final String fragments_p401p404 = "p401p404";
    public static final String fragments_p405p407 = "p405p407";
    public static final String fragments_p408p410 = "p408p410";
    public static final String fragments_p411p416 = "p411p416";
    public static final String fragments_p501p505 = "p501p505";
    public static final String fragments_p506p507 = "p506p507";
    public static final String fragments_p508p511 = "p508p511";
    public static final String fragments_p512p513 = "p512p513";
    public static final String fragments_p601p604 = "p601p604";
    public static final String fragments_p605p608 = "p605p608";
    public static final String fragments_p609p612 = "p609p612";
    public static final String fragments_p613p617 = "p613p617";
    public static final String fragments_p618p621 = "p618p621";
    public static final String fragments_p622p625 = "p622p625";
    public static final String fragments_p626p629 = "p626p629";
    public static final String fragments_p630 = "p630";
    public static final String fragments_p701p705 = "p701p705";
    public static final String fragments_p706p709 = "p706p709";
    public static final String fragments_p801p804 = "p801p804";
    public static final String fragments_p805p808 = "p805p808";
    public static final String fragments_p809p812 = "p809p812";
    public static final String fragments_p813p816 = "p813p816";
    public static final String fragments_p817p820 = "p817p820";
    public static final String fragments_p821p823 = "p821p823";


    public static final String SQL_CREATE_TABLA_FRAGMENTS =
            "CREATE TABLE " + tablafragments + "(" +
                    fragments_id  + " TEXT PRIMARY KEY," +
                    fragments_id_vivienda  + " TEXT," +
                    fragments_c_p001p003  + " TEXT," +
                    fragments_c_p004p006  + " TEXT," +
                    fragments_d_p007p011  + " TEXT," +
                    fragments_d_p012  + " TEXT," +
                    fragments_d_p013p014  + " TEXT," +
                    fragments_e_p015  + " TEXT," +
                    fragments_e_p016  + " TEXT," +
                    fragments_e_p017p020  + " TEXT," +
                    fragments_e_p021p025  + " TEXT," +
                    fragments_f_p026p029  + " TEXT," +
                    fragments_f_p030p033  + " TEXT," +
                    fragments_f_p034p036  + " TEXT," +
                    fragments_g_p038p039  + " TEXT," +
                    fragments_g_p040  + " TEXT," +
                    fragments_h_p041p043  + " TEXT," +
                    fragments_h_p044p046  + " TEXT," +
                    fragments_i_p047p048  + " TEXT," +
                    fragments_i_p049p051  + " TEXT," +
                    fragments_i_p052p053  + " TEXT," +
                    fragments_i_p054p056  + " TEXT," +
                    fragments_i_p057p059  + " TEXT," +
                    fragments_p301p305  + " TEXT," +
                    fragments_p306p308  + " TEXT," +
                    fragments_p309  + " TEXT," +
                    fragments_p310p312  + " TEXT," +
                    fragments_p313p317  + " TEXT," +
                    fragments_p318  + " TEXT," +
                    fragments_p401p404  + " TEXT," +
                    fragments_p405p407  + " TEXT," +
                    fragments_p408p410  + " TEXT," +
                    fragments_p411p416  + " TEXT," +
                    fragments_p501p505  + " TEXT," +
                    fragments_p506p507  + " TEXT," +
                    fragments_p508p511  + " TEXT," +
                    fragments_p512p513  + " TEXT," +
                    fragments_p601p604  + " TEXT," +
                    fragments_p605p608  + " TEXT," +
                    fragments_p609p612  + " TEXT," +
                    fragments_p613p617 + " TEXT," +
                    fragments_p618p621 + " TEXT," +
                    fragments_p622p625 + " TEXT," +
                    fragments_p626p629  + " TEXT," +
                    fragments_p630  + " TEXT," +
                    fragments_p701p705  + " TEXT," +
                    fragments_p706p709  + " TEXT," +
                    fragments_p801p804  + " TEXT," +
                    fragments_p805p808  + " TEXT," +
                    fragments_p809p812  + " TEXT," +
                    fragments_p813p816  + " TEXT," +
                    fragments_p817p820  + " TEXT," +
                    fragments_p821p823 + " TEXT" + ");"
            ;

    /**
     * TABLA FRAGMENTS COBERTURA
     * */

    public static final String cobertura_fragments_id = "_id";
    public static final String cobertura_fragments_id_vivienda = "id_vivienda";
    public static final String cobertura_fragments_cp301p305 = "cp301p305";
    public static final String cobertura_fragments_cp306p308 = "cp306p308";
    public static final String cobertura_fragments_cp309 = "cp309";
    public static final String cobertura_fragments_cp310p312 = "cp310p312";
    public static final String cobertura_fragments_cp313p317 = "cp313p317";
    public static final String cobertura_fragments_cp318 = "cp318";
    public static final String cobertura_fragments_cp401p404 = "cp401p404";
    public static final String cobertura_fragments_cp405p407 = "cp405p407";
    public static final String cobertura_fragments_cp408p410 = "cp408p410";
    public static final String cobertura_fragments_cp411p416 = "cp411p416";
    public static final String cobertura_fragments_cp501p505 = "cp501p505";
    public static final String cobertura_fragments_cp506p507 = "cp506p507";
    public static final String cobertura_fragments_cp508p511 = "cp508p511";
    public static final String cobertura_fragments_cp512p513 = "cp512p513";
    public static final String cobertura_fragments_cp601p604 = "cp601p604";
    public static final String cobertura_fragments_cp605p608 = "cp605p608";
    public static final String cobertura_fragments_cp609p612 = "cp609p612";
    public static final String cobertura_fragments_cp613p617 = "cp613p617";
    public static final String cobertura_fragments_cp618p621 = "cp618p621";
    public static final String cobertura_fragments_cp622p625 = "cp622p625";
    public static final String cobertura_fragments_cp626p629 = "cp626p629";
    public static final String cobertura_fragments_cp630 = "cp630";
    public static final String cobertura_fragments_cp701p705 = "cp701p705";
    public static final String cobertura_fragments_cp706p709 = "cp706p709";
    public static final String cobertura_fragments_cp801p804 = "cp801p804";
    public static final String cobertura_fragments_cp805p808 = "cp805p808";
    public static final String cobertura_fragments_cp809p812 = "cp809p812";
    public static final String cobertura_fragments_cp813p816 = "cp813p816";
    public static final String cobertura_fragments_cp817p820 = "cp817p820";
    public static final String cobertura_fragments_cp821p823 = "cp821p823";


    public static final String SQL_CREATE_TABLA_COBERTURA_FRAGMENTS =
            "CREATE TABLE " + tablacoberturafragments + "(" +
                    cobertura_fragments_id  + " TEXT PRIMARY KEY," +
                    cobertura_fragments_id_vivienda  + " TEXT," +
                    cobertura_fragments_cp301p305  + " TEXT," +
                    cobertura_fragments_cp306p308  + " TEXT," +
                    cobertura_fragments_cp309  + " TEXT," +
                    cobertura_fragments_cp310p312  + " TEXT," +
                    cobertura_fragments_cp313p317  + " TEXT," +
                    cobertura_fragments_cp318  + " TEXT," +
                    cobertura_fragments_cp401p404  + " TEXT," +
                    cobertura_fragments_cp405p407  + " TEXT," +
                    cobertura_fragments_cp408p410  + " TEXT," +
                    cobertura_fragments_cp411p416  + " TEXT," +
                    cobertura_fragments_cp501p505  + " TEXT," +
                    cobertura_fragments_cp506p507  + " TEXT," +
                    cobertura_fragments_cp508p511  + " TEXT," +
                    cobertura_fragments_cp512p513  + " TEXT," +
                    cobertura_fragments_cp601p604  + " TEXT," +
                    cobertura_fragments_cp605p608  + " TEXT," +
                    cobertura_fragments_cp609p612  + " TEXT," +
                    cobertura_fragments_cp613p617 + " TEXT," +
                    cobertura_fragments_cp618p621 + " TEXT," +
                    cobertura_fragments_cp622p625 + " TEXT," +
                    cobertura_fragments_cp626p629  + " TEXT," +
                    cobertura_fragments_cp630  + " TEXT," +
                    cobertura_fragments_cp701p705  + " TEXT," +
                    cobertura_fragments_cp706p709  + " TEXT," +
                    cobertura_fragments_cp801p804  + " TEXT," +
                    cobertura_fragments_cp805p808  + " TEXT," +
                    cobertura_fragments_cp809p812  + " TEXT," +
                    cobertura_fragments_cp813p816  + " TEXT," +
                    cobertura_fragments_cp817p820  + " TEXT," +
                    cobertura_fragments_cp821p823 + " TEXT" + ");"
            ;

    /**
     * TABLA VISITA ENCUESTADOR
     * */

    public static final String visita_encuestador_id = "_id";
    public static final String visita_encuestador_id_vivienda = "id_vivienda";
    public static final String visita_encuestador_id_hogar = "id_hogar";
    public static final String visita_encuestador_numero = "numero";
    public static final String visita_encuestador_vis_fecha_dd = "vis_fecha_dd";
    public static final String visita_encuestador_vis_fecha_mm = "vis_fecha_mm";
    public static final String visita_encuestador_vis_fecha_aa = "vis_fecha_aa";
    public static final String visita_encuestador_vis_hor_ini = "vis_hor_ini";
    public static final String visita_encuestador_vis_min_ini = "vis_min_ini";
    public static final String visita_encuestador_vis_hor_fin = "vis_hor_fin";
    public static final String visita_encuestador_vis_min_fin = "vis_min_fin";
    public static final String visita_encuestador_prox_vis_fecha_dd = "prox_vis_fecha_dd";
    public static final String visita_encuestador_prox_vis_fecha_mm = "prox_vis_fecha_mm";
    public static final String visita_encuestador_prox_vis_fecha_aa = "prox_vis_fecha_aa";
    public static final String visita_encuestador_prox_vis_hor = "prox_vis_hor";
    public static final String visita_encuestador_prox_vis_min = "prox_vis_min";
    public static final String visita_encuestador_vis_resu = "vis_resu";
    public static final String visita_encuestador_vis_resu_esp = "vis_resu_esp";




    public static final String SQL_CREATE_TABLA_VISITA_ENCUESTADOR =
            "CREATE TABLE " + tablavisitasencuestador + "(" +
                    visita_encuestador_id  + " TEXT PRIMARY KEY," +
                    visita_encuestador_id_vivienda + " TEXT," +
                    visita_encuestador_id_hogar + " TEXT," +
                    visita_encuestador_numero + " TEXT," +
                    visita_encuestador_vis_fecha_dd + " TEXT," +
                    visita_encuestador_vis_fecha_mm + " TEXT," +
                    visita_encuestador_vis_fecha_aa + " TEXT," +
                    visita_encuestador_vis_hor_ini + " TEXT," +
                    visita_encuestador_vis_min_ini + " TEXT," +
                    visita_encuestador_vis_hor_fin + " TEXT," +
                    visita_encuestador_vis_min_fin + " TEXT," +
                    visita_encuestador_prox_vis_fecha_dd + " TEXT," +
                    visita_encuestador_prox_vis_fecha_mm + " TEXT," +
                    visita_encuestador_prox_vis_fecha_aa + " TEXT," +
                    visita_encuestador_prox_vis_hor + " TEXT," +
                    visita_encuestador_prox_vis_min + " TEXT," +
                    visita_encuestador_vis_resu + " TEXT," +
                    visita_encuestador_vis_resu_esp + " TEXT" + ");"
            ;

    /**
     * TABLA RESULTADO ENCUESTADOR
     * */

    public static final String resultado_encuestador_id = "_id";
    public static final String resultado_encuestador_id_vivienda = "id_vivienda";
    public static final String resultado_encuestador_vis_resultado_final = "vis_resultado_final";
    public static final String resultado_encuestador_vis_fecha_final_dd = "vis_fecha_final_dd";
    public static final String resultado_encuestador_vis_fecha_final_mm = "vis_fecha_final_mm";
    public static final String resultado_encuestador_vis_fecha_final_aa = "vis_fecha_final_aa";





    public static final String SQL_CREATE_TABLA_RESULTADO_ENCUESTADOR =
            "CREATE TABLE " + tablaresultadoencuestador + "(" +
                    resultado_encuestador_id  + " TEXT PRIMARY KEY," +
                    resultado_encuestador_id_vivienda + " TEXT," +
                    resultado_encuestador_vis_resultado_final + " TEXT," +
                    resultado_encuestador_vis_fecha_final_dd + " TEXT," +
                    resultado_encuestador_vis_fecha_final_mm + " TEXT," +
                    resultado_encuestador_vis_fecha_final_aa + " TEXT" + ");"
            ;

    /**
     * TABLA VISITA SUPERVISOR
     * */

    public static String visita_supervisor_id = "_id";
    public static String visita_supervisor_id_vivienda = "id_vivienda";
    public static String visita_supervisor_id_hogar = "id_hogar";
    public static String visita_supervisor_numero = "numero";
    public static String visita_supervisor_vis_fecha_dd = "vis_fecha_dd";
    public static String visita_supervisor_vis_fecha_mm = "vis_fecha_mm";
    public static String visita_supervisor_vis_fecha_aa = "vis_fecha_aa";
    public static String visita_supervisor_vis_hor_ini = "vis_hor_ini";
    public static String visita_supervisor_vis_min_ini = "vis_min_ini";
    public static String visita_supervisor_vis_hor_fin = "vis_hor_fin";
    public static String visita_supervisor_vis_min_fin = "vis_min_fin";
    public static String visita_supervisor_vis_resu = "vis_resu";
    public static String visita_supervisor_vis_resu_esp = "vis_resu_esp";

    public static final String SQL_CREATE_TABLA_VISITA_SUPERVISOR =
            "CREATE TABLE " + tablavisitassupervisor + "(" +
                    visita_supervisor_id  + " TEXT PRIMARY KEY," +
                    visita_supervisor_id_vivienda + " TEXT," +
                    visita_supervisor_id_hogar + " TEXT," +
                    visita_supervisor_numero + " TEXT," +
                    visita_supervisor_vis_fecha_dd + " TEXT," +
                    visita_supervisor_vis_fecha_mm + " TEXT," +
                    visita_supervisor_vis_fecha_aa + " TEXT," +
                    visita_supervisor_vis_hor_ini + " TEXT," +
                    visita_supervisor_vis_min_ini + " TEXT," +
                    visita_supervisor_vis_hor_fin + " TEXT," +
                    visita_supervisor_vis_min_fin + " TEXT," +
                    visita_supervisor_vis_resu + " TEXT," +
                    visita_supervisor_vis_resu_esp + " TEXT" + ");"
            ;

    /**
     * TABLA RESULTADO SUPERVISOR
     * */

    public static final String resultado_supervisor_id = "_id";
    public static final String resultado_supervisor_id_vivienda = "id_vivienda";
    public static final String resultado_supervisor_vis_resultado_final = "vis_resultado_final";
    public static final String resultado_supervisor_vis_fecha_final_dd = "vis_fecha_final_dd";
    public static final String resultado_supervisor_vis_fecha_final_mm = "vis_fecha_final_mm";
    public static final String resultado_supervisor_vis_fecha_final_aa = "vis_fecha_final_aa";


    public static final String SQL_CREATE_TABLA_RESULTADO_SUPERVISOR =
            "CREATE TABLE " + tablaresultadosupervisor + "(" +
                    resultado_supervisor_id  + " TEXT PRIMARY KEY," +
                    resultado_supervisor_id_vivienda + " TEXT," +
                    resultado_supervisor_vis_resultado_final + " TEXT," +
                    resultado_supervisor_vis_fecha_final_dd + " TEXT," +
                    resultado_supervisor_vis_fecha_final_mm + " TEXT," +
                    resultado_supervisor_vis_fecha_final_aa + " TEXT" + ");"
            ;


    /**
     * TABLA FUNCIONARIOS
     * */

    public static final String funcionarios_id = "_id";
    public static final String funcionarios_dni_encu = "dni_encu";
    public static final String funcionarios_dni_sup = "dni_sup";
    public static final String funcionarios_dni_coord = "dni_coor";
    public static final String funcionarios_nombre_encu = "nombre_encu";
    public static final String funcionarios_nombre_sup = "nombre_sup";
    public static final String funcionarios_nombre_coord = "nombre_coord";

    public static final String SQL_CREATE_TABLA_FUNCIONARIOS =
            "CREATE TABLE " + tablafuncionarios + "(" +
                    funcionarios_id  + " TEXT PRIMARY KEY ," +
                    funcionarios_dni_encu + " TEXT," +
                    funcionarios_dni_sup  + " TEXT," +
                    funcionarios_dni_coord  + " TEXT," +
                    funcionarios_nombre_encu  + " TEXT," +
                    funcionarios_nombre_sup  + " TEXT," +
                    funcionarios_nombre_coord + " TEXT" + ");"
            ;


    /**
     * TABLA MODULO 1 VIVIENDA
     * */
    public static final String modulo1_v_id = "_id";
    public static final String modulo1_v_c1_p101 = "c1_p101";
    public static final String modulo1_v_c1_p101_o = "c1_p101_o";
    public static final String modulo1_v_c1_p102 = "c1_p102";
    public static final String modulo1_v_c1_p102_o = "c1_p102_o";
    public static final String modulo1_v_c1_p103 = "c1_p103";
    public static final String modulo1_v_c1_p103_o = "c1_p103_o";
    public static final String modulo1_v_c1_p104 = "c1_p104";
    public static final String modulo1_v_c1_p104_o = "c1_p104_o";
    public static final String modulo1_v_c1_p105 = "c1_p105";
    public static final String modulo1_v_c1_p106 = "c1_p106";
    public static final String modulo1_v_c1_p107 = "c1_p107";
    public static final String modulo1_v_COB100A = "COB100A";
    //nuevos
    public static final String seccion_a_p101 = "P101";
    public static final String seccion_a_p101_o = "P101_O";
    public static final String seccion_a_p102 = "P102";
    public static final String seccion_a_p102_o = "P102_O";
    public static final String seccion_a_p103 = "P103";
    public static final String seccion_a_p103_o = "P103_O";
    public static final String seccion_a_p103a = "P103A";
    public static final String seccion_a_p104 = "P104";
    public static final String seccion_a_p104_o = "P104_O";
    public static final String seccion_a_p105_1 = "P105_1";
    public static final String seccion_a_p105_2 = "P105_2";
    public static final String seccion_a_p105_3 = "P105_3";
    public static final String seccion_a_p105_4 = "P105_4";
    public static final String seccion_a_p105_5 = "P105_5";
    public static final String seccion_a_p105_5_o = "P105_5_O";
    public static final String seccion_a_p105_6 = "P105_6";
    public static final String seccion_a_p106_1 = "P106_1";
    public static final String seccion_a_p106_2 = "P106_2";
    public static final String seccion_a_p106_3 = "P106_3";
    public static final String seccion_a_p106_4 = "P106_4";
    public static final String seccion_a_p106_5 = "P106_5";
    public static final String seccion_a_p106_6 = "P106_6";
    public static final String seccion_a_p106_7 = "P106_7";
    public static final String seccion_a_p106_8 = "P106_8";
    public static final String seccion_a_p106_9 = "P106_9";
    public static final String seccion_a_p106_10 = "P106_10";
    public static final String seccion_a_p106_11 = "P106_11";
    public static final String seccion_a_p106_11_o = "P106_11_O";
    public static final String seccion_a_p106_12 = "P106_12";
    public static final String seccion_a_p107 = "P107";
    public static final String seccion_a_p108_1 = "P108_1";
    public static final String seccion_a_p108_2 = "P108_2";
    public static final String seccion_a_p108_3 = "P108_3";
    public static final String seccion_a_p108_4 = "P108_4";
    public static final String seccion_a_p108_5 = "P108_5";
    public static final String seccion_a_p108_6 = "P108_6";
    public static final String seccion_a_p108_7 = "P108_7";
    public static final String seccion_a_p108_8 = "P108_8";
    public static final String seccion_a_p108_9 = "P108_9";
    public static final String seccion_a_p108_10 = "P108_10";
    public static final String seccion_a_p108_11 = "P108_11";
    public static final String seccion_a_p108_12 = "P108_12";
    public static final String seccion_a_p108_12_o = "P108_12_O";
    public static final String seccion_a_p109_1 = "P109_1";
    public static final String seccion_a_p110_1 = "P110_1";
    public static final String seccion_a_p109_2 = "P109_2";
    public static final String seccion_a_p110_2 = "P110_2";
    public static final String seccion_a_p109_3 = "P109_3";
    public static final String seccion_a_p110_3 = "P110_3";
    public static final String seccion_a_p109_4 = "P109_4";
    public static final String seccion_a_p110_4 = "P110_4";
    public static final String seccion_a_p109_5 = "P109_5";
    public static final String seccion_a_p110_5 = "P110_5";
    public static final String seccion_a_p109_6 = "P109_6";
    public static final String seccion_a_p110_6 = "P110_6";
    public static final String seccion_a_p109_7 = "P109_7";
    public static final String seccion_a_p110_7 = "P110_7";
    public static final String seccion_a_p109_8 = "P109_8";
    public static final String seccion_a_p110_8 = "P110_8";
    public static final String seccion_a_p109_9 = "P109_9";
    public static final String seccion_a_p109_9_o = "P109_9_O";
    public static final String seccion_a_p110_9 = "P110_9";
    public static final String seccion_a_p109_10 = "P109_10";
    public static final String seccion_a_p109_10_o = "P109_10_O";
    public static final String seccion_a_p110_10 = "P110_10";
    public static final String seccion_a_obs_a = "OBS_A";

    /* Columnas TB_SECCION_H */
    public static final String seccion_h_id = "ID";
    public static final String seccion_h_id_vivienda = "ID_VIVIENDA";
    public static final String seccion_h_numero_hogar = "NUMERO_HOGAR";
    public static final String seccion_h_numero_residente = "NUMERO_RESIDENTE";
    public static final String seccion_h_inf_300 = "INF_300";
    public static final String seccion_h_p341_1 = "P341_1";
    public static final String seccion_h_p341_2 = "P341_2";
    public static final String seccion_h_p341_3 = "P341_3";
    public static final String seccion_h_p341_4 = "P341_4";
    public static final String seccion_h_p342 = "P342";
    public static final String seccion_h_p342_o = "P342_O";
    public static final String seccion_h_p342a = "P342A";
    public static final String seccion_h_p342b = "P342B";
    public static final String seccion_h_p342b_o = "P342B_O";
    public static final String seccion_h_p343 = "P343";
    public static final String seccion_h_id_p344 = "ID_P344";
    public static final String seccion_h_id_p344_item = "ID_P344_ITEM";
    public static final String seccion_h_id_p344_item_o = "ID_P344_ITEM_O";
    public static final String seccion_h_p344_1 = "P344_1";
    public static final String seccion_h_p345_1_1 = "P345_1_1";
    public static final String seccion_h_p345_1_2 = "P345_1_2";
    public static final String seccion_h_p345_1_3 = "P345_1_3";
    public static final String seccion_h_p345_1_4 = "P345_1_4";
    public static final String seccion_h_p345_1_5 = "P345_1_5";
    public static final String seccion_h_p345_1_6 = "P345_1_6";
    public static final String seccion_h_p345_1_7 = "P345_1_7";
    public static final String seccion_h_p345_1_8 = "P345_1_8";
    public static final String seccion_h_p345_1_8_o = "P345_1_8_O";
    public static final String seccion_h_p345_1_9 = "P345_1_9";
    public static final String seccion_h_p344_2 = "P344_2";
    public static final String seccion_h_p345_2_1 = "P345_2_1";
    public static final String seccion_h_p345_2_2 = "P345_2_2";
    public static final String seccion_h_p345_2_3 = "P345_2_3";
    public static final String seccion_h_p345_2_4 = "P345_2_4";
    public static final String seccion_h_p345_2_5 = "P345_2_5";
    public static final String seccion_h_p345_2_6 = "P345_2_6";
    public static final String seccion_h_p345_2_7 = "P345_2_7";
    public static final String seccion_h_p345_2_8 = "P345_2_8";
    public static final String seccion_h_p345_2_8_o = "P345_2_8_O";
    public static final String seccion_h_p345_2_9 = "P345_2_9";
    public static final String seccion_h_p344_3 = "P344_3";
    public static final String seccion_h_p345_3_1 = "P345_3_1";
    public static final String seccion_h_p345_3_2 = "P345_3_2";
    public static final String seccion_h_p345_3_3 = "P345_3_3";
    public static final String seccion_h_p345_3_4 = "P345_3_4";
    public static final String seccion_h_p345_3_5 = "P345_3_5";
    public static final String seccion_h_p345_3_6 = "P345_3_6";
    public static final String seccion_h_p345_3_7 = "P345_3_7";
    public static final String seccion_h_p345_3_8 = "P345_3_8";
    public static final String seccion_h_p345_3_8_o = "P345_3_8_O";
    public static final String seccion_h_p345_3_9 = "P345_3_9";
    public static final String seccion_h_p344_4 = "P344_4";
    public static final String seccion_h_p344_4_o = "P344_4_O";
    public static final String seccion_h_p345_4_1 = "P345_4_1";
    public static final String seccion_h_p345_4_2 = "P345_4_2";
    public static final String seccion_h_p345_4_3 = "P345_4_3";
    public static final String seccion_h_p345_4_4 = "P345_4_4";
    public static final String seccion_h_p345_4_5 = "P345_4_5";
    public static final String seccion_h_p345_4_6 = "P345_4_6";
    public static final String seccion_h_p345_4_7 = "P345_4_7";
    public static final String seccion_h_p345_4_8 = "P345_4_8";
    public static final String seccion_h_p345_4_8_o = "P345_4_8_O";
    public static final String seccion_h_p345_4_9 = "P345_4_9";
    public static final String seccion_h_p346_1 = "P346_1";
    public static final String seccion_h_p346_2 = "P346_2";
    public static final String seccion_h_p346_3 = "P346_3";
    public static final String seccion_h_p346_4 = "P346_4";
    public static final String seccion_h_p346_5 = "P346_5";
    public static final String seccion_h_p346_6 = "P346_6";
    public static final String seccion_h_p346_6_o = "P346_6_O";
    public static final String seccion_h_obs_h = "OBS_H";

    /* Columnas TB_SECCION_I */
    public static final String seccion_i_id = "ID";
    public static final String seccion_i_inf_300 = "INF_300";
    public static final String seccion_i_informante="id_informante";
    public static final String seccion_i_id_vivienda = "ID_VIVIENDA";
    public static final String seccion_i_numero_hogar = "NUMERO_HOGAR";
    public static final String seccion_i_p347_1 = "P347_1";
    public static final String seccion_i_p347_2 = "P347_2";
    public static final String seccion_i_p347_3 = "P347_3";
    public static final String seccion_i_p347_4 = "P347_4";
    public static final String seccion_i_p347_5 = "P347_5";
    public static final String seccion_i_p348_1 = "P348_1";
    public static final String seccion_i_p348_2 = "P348_2";
    public static final String seccion_i_p348_3 = "P348_3";
    public static final String seccion_i_p348_4 = "P348_4";
    public static final String seccion_i_p348_5 = "P348_5";
    public static final String seccion_i_p348_6 = "P348_6";
    public static final String seccion_i_p348_7 = "P348_7";
    public static final String seccion_i_p348_8 = "P348_8";
    public static final String seccion_i_p348_9 = "P348_9";
    public static final String seccion_i_p348_10 = "P348_10";
    public static final String seccion_i_p348_11 = "P348_11";
    public static final String seccion_i_p348_11_o = "P348_11_O";
    public static final String seccion_i_p348_12 = "P348_12";
    public static final String seccion_i_p348_13 = "P348_13";
    public static final String seccion_i_p348_14 = "P348_14";
    public static final String seccion_i_p348_i = "P348_I";
    public static final String seccion_i_p349 = "P349";
    public static final String seccion_i_p350_1 = "P350_1";
    public static final String seccion_i_p350_2 = "P350_2";
    public static final String seccion_i_p350_3 = "P350_3";
    public static final String seccion_i_p350_4 = "P350_4";
    public static final String seccion_i_p350_5 = "P350_5";
    public static final String seccion_i_p350_5_o = "P350_5_O";
    public static final String seccion_i_p351 = "P351";
    public static final String seccion_i_p351a_1 = "P351A_1";
    public static final String seccion_i_p351a_2 = "P351A_2";
    public static final String seccion_i_p351a_3 = "P351A_3";
    public static final String seccion_i_p351a_4 = "P351A_4";
    public static final String seccion_i_p351a_5 = "P351A_5";
    public static final String seccion_i_p351a_5_o = "P351A_5_O";
    public static final String seccion_i_p352 = "P352";
    public static final String seccion_i_p352_o = "P352_O";
    public static final String seccion_i_p353_1 = "P353_1";
    public static final String seccion_i_p353_2 = "P353_2";
    public static final String seccion_i_p353_3 = "P353_3";
    public static final String seccion_i_p353_4 = "P353_4";
    public static final String seccion_i_p353_5 = "P353_5";
    public static final String seccion_i_p353_6 = "P353_6";
    public static final String seccion_i_p353_6_o = "P353_6_O";
    public static final String seccion_i_p354_1 = "P354_1";
    public static final String seccion_i_p355_1 = "P355_1";
    public static final String seccion_i_p355_1_o = "P355_1_O";
    public static final String seccion_i_p354_2 = "P354_2";
    public static final String seccion_i_p355_2 = "P355_2";
    public static final String seccion_i_p355_2_o = "P355_2_O";
    public static final String seccion_i_p354_3 = "P354_3";
    public static final String seccion_i_p355_3 = "P355_3";
    public static final String seccion_i_p355_3_o = "P355_3_O";
    public static final String seccion_i_p354_4 = "P354_4";
    public static final String seccion_i_p355_4 = "P355_4";
    public static final String seccion_i_p355_4_o = "P355_4_O";
    public static final String seccion_i_p354_5 = "P354_5";
    public static final String seccion_i_p355_5 = "P355_5";
    public static final String seccion_i_p355_5_o = "P355_5_O";
    public static final String seccion_i_p354_6 = "P354_6";
    public static final String seccion_i_p354_6_o = "P354_6_O";
    public static final String seccion_i_p355_6 = "P355_6";
    public static final String seccion_i_p355_6_o = "P355_6_O";
    public static final String seccion_i_p356 = "P356";
    public static final String seccion_i_p357_1 = "P357_1";
    public static final String seccion_i_p357_2 = "P357_2";
    public static final String seccion_i_p357_3 = "P357_3";
    public static final String seccion_i_p357_4 = "P357_4";
    public static final String seccion_i_p357_5 = "P357_5";
    public static final String seccion_i_p357_6 = "P357_6";
    public static final String seccion_i_p357_7 = "P357_7";
    public static final String seccion_i_p357_8 = "P357_8";
    public static final String seccion_i_p357_8_o = "P357_8_O";
    public static final String seccion_i_p357_9 = "P357_9";
    public static final String seccion_i_p358 = "P358";
    public static final String seccion_i_p359 = "P359";
    public static final String seccion_i_obs_i = "OBS_I";


    /**
     * TABLA MODULO 1 HOGAR
     * */
    public static final String modulo1_h_id = "_id";
    public static final String modulo1_h_idVivienda = "id_vivienda";
    //public static final String modulo1_h_c1_p108 = "c1_p108";
    // public static final String modulo1_h_c1_p108_o = "c1_p108_o";
    //public static final String modulo1_h_c1_p109 = "c1_p109";
    ////public static final String modulo1_h_c1_p109_o = "c1_p109_o";
    //public static final String modulo1_h_c1_p110 = "c1_p110";
    // public static final String modulo1_h_c1_p110_o = "c1_p110_o";
    // public static final String modulo1_h_c1_p111 = "c1_p111";
    // public static final String modulo1_h_c1_p111_o = "c1_p111_o";
    // public static final String modulo1_h_c1_p112 = "c1_p112";
    //public static final String modulo1_h_c1_p112_o = "c1_p112_o";
    // public static final String modulo1_h_c1_p113_1 = "c1_p113_1";
    //public static final String modulo1_h_c1_p113_2 = "c1_p113_2";
    //public static final String modulo1_h_c1_p113_3 = "c1_p113_3";
    //public static final String modulo1_h_c1_p113_4 = "c1_p113_4";
    // public static final String modulo1_h_c1_p113_5 = "c1_p113_5";
    //public static final String modulo1_h_c1_p113_6 = "c1_p113_6";
    //public static final String modulo1_h_c1_p113_7 = "c1_p113_7";
    //public static final String modulo1_h_c1_p113_8 = "c1_p113_8";
    //public static final String modulo1_h_c1_p113_9 = "c1_p113_9";
    //public static final String modulo1_h_c1_p113_7_o = "c1_p113_7_o";
    //public static final String modulo1_h_c1_p113_8_o = "c1_p113_8_o";
    //public static final String modulo1_h_c1_p113_9_o = "c1_p113_9_o";
    public static final String modulo1_h_COB100B = "COB100B";

    public static final String SQL_CREATE_TABLA_MODULO1V =
            "CREATE TABLE " + tablamodulo1v + "(" +
                    modulo1_v_id  + " TEXT PRIMARY KEY," +
                    //// modulo1_v_c1_p101 + " TEXT," +
                    //modulo1_v_c1_p101_o + " TEXT," +
                    //modulo1_v_c1_p102 + " TEXT," +
                    //modulo1_v_c1_p102_o + " TEXT," +
                    // modulo1_v_c1_p103 + " TEXT," +
                    //modulo1_v_c1_p103_o + " TEXT," +
                    //modulo1_v_c1_p104 + " TEXT," +
                    // modulo1_v_c1_p104_o + " TEXT," +
                    //modulo1_v_c1_p105 + " TEXT," +
                    //modulo1_v_c1_p106 + " TEXT," +
                    //modulo1_v_c1_p107 + " TEXT," +
                    seccion_a_p101 + " TEXT," +
                    seccion_a_p101_o + " TEXT," +
                    seccion_a_p102 + " TEXT," +
                    seccion_a_p102_o + " TEXT," +
                    seccion_a_p103 + " TEXT," +
                    seccion_a_p103_o + " TEXT," +
                    seccion_a_p103a + " TEXT," +
                    seccion_a_p104 + " TEXT," +
                    seccion_a_p104_o + " TEXT," +
                    seccion_a_p105_1 + " TEXT," +
                    seccion_a_p105_2 + " TEXT," +
                    seccion_a_p105_3 + " TEXT," +
                    seccion_a_p105_4 + " TEXT," +
                    seccion_a_p105_5 + " TEXT," +
                    seccion_a_p105_5_o + " TEXT," +
                    seccion_a_p105_6 + " TEXT," +
                    seccion_a_p106_1 + " TEXT," +
                    seccion_a_p106_2 + " TEXT," +
                    seccion_a_p106_3 + " TEXT," +
                    seccion_a_p106_4 + " TEXT," +
                    seccion_a_p106_5 + " TEXT," +
                    seccion_a_p106_6 + " TEXT," +
                    seccion_a_p106_7 + " TEXT," +
                    seccion_a_p106_8 + " TEXT," +
                    seccion_a_p106_9 + " TEXT," +
                    seccion_a_p106_10 + " TEXT," +
                    seccion_a_p106_11 + " TEXT," +
                    seccion_a_p106_11_o + " TEXT," +
                    seccion_a_p106_12 + " TEXT," +
                    modulo1_v_COB100A + " TEXT" + ");"
            ;

    public static final String SQL_CREATE_TABLA_MODULO1H =
            "CREATE TABLE " + tablamodulo1h + "(" +
                    modulo1_h_id  + " TEXT PRIMARY KEY," +
                    modulo1_h_idVivienda + " TEXT," +
                    //    modulo1_h_c1_p108 + " TEXT," +
                    //modulo1_h_c1_p108_o + " TEXT," +
                    //modulo1_h_c1_p109 + " TEXT," +
                    //modulo1_h_c1_p109_o + " TEXT," +
                    //modulo1_h_c1_p110 + " TEXT," +
                    //modulo1_h_c1_p110_o + " TEXT," +
                    //modulo1_h_c1_p111 + " TEXT," +
                    //modulo1_h_c1_p111_o + " TEXT," +
                    //modulo1_h_c1_p112 + " TEXT," +
                    //modulo1_h_c1_p112_o + " TEXT," +
                    //modulo1_h_c1_p113_1 + " TEXT," +
                    //modulo1_h_c1_p113_2 + " TEXT," +
                    //modulo1_h_c1_p113_3 + " TEXT," +
                    //modulo1_h_c1_p113_4 + " TEXT," +
                    //modulo1_h_c1_p113_5 + " TEXT," +
                    //modulo1_h_c1_p113_6 + " TEXT," +
                    // modulo1_h_c1_p113_7 + " TEXT," +
                    //modulo1_h_c1_p113_7_o + " TEXT," +
                    //modulo1_h_c1_p113_8 + " TEXT," +
                    //modulo1_h_c1_p113_8_o + " TEXT," +
                    //modulo1_h_c1_p113_9 + " TEXT," +
                    //modulo1_h_c1_p113_9_o + " TEXT," +
                    seccion_a_p107 + " TEXT," +
                    seccion_a_p108_1 + " TEXT," +
                    seccion_a_p108_2 + " TEXT," +
                    seccion_a_p108_3 + " TEXT," +
                    seccion_a_p108_4 + " TEXT," +
                    seccion_a_p108_5 + " TEXT," +
                    seccion_a_p108_6 + " TEXT," +
                    seccion_a_p108_7 + " TEXT," +
                    seccion_a_p108_8 + " TEXT," +
                    seccion_a_p108_9 + " TEXT," +
                    seccion_a_p108_10 + " TEXT," +
                    seccion_a_p108_11 + " TEXT," +
                    seccion_a_p108_12 + " TEXT," +
                    seccion_a_p108_12_o + " TEXT," +
                    seccion_a_p109_1 + " TEXT," +
                    seccion_a_p110_1 + " TEXT," +
                    seccion_a_p109_2 + " TEXT," +
                    seccion_a_p110_2 + " TEXT," +
                    seccion_a_p109_3 + " TEXT," +
                    seccion_a_p110_3 + " TEXT," +
                    seccion_a_p109_4 + " TEXT," +
                    seccion_a_p110_4 + " TEXT," +
                    seccion_a_p109_5 + " TEXT," +
                    seccion_a_p110_5 + " TEXT," +
                    seccion_a_p109_6 + " TEXT," +
                    seccion_a_p110_6 + " TEXT," +
                    seccion_a_p109_7 + " TEXT," +
                    seccion_a_p110_7 + " TEXT," +
                    seccion_a_p109_8 + " TEXT," +
                    seccion_a_p110_8 + " TEXT," +
                    seccion_a_p109_9 + " TEXT," +
                    seccion_a_p109_9_o + " TEXT," +
                    seccion_a_p110_9 + " TEXT," +
                    seccion_a_p109_10 + " TEXT," +
                    seccion_a_p109_10_o + " TEXT," +
                    seccion_a_p110_10 + " TEXT," +
                    seccion_a_obs_a + " TEXT," +
                    modulo1_h_COB100B + " TEXT" + ");"
            ;


    /**
     * TABLA RESIDENTES
     * */
    public static final String residentes_id = "_id";
    public static final String residentes_id_informante = "id_informante";
    public static final String residentes_id_hogar = "id_hogar";
    public static final String residentes_id_vivienda = "id_vivienda";
    public static final String residentes_numero = "numero";
    public static final String residentes_c2_p202 = "c2_p202";
    public static final String residentes_c2_p203 = "c2_p203";
    public static final String residentes_c2_p204 = "c2_p204";
    public static final String residentes_c2_p205_a = "c2_p205_a";
    public static final String residentes_c2_p205_m = "c2_p205_m";
    public static final String residentes_c2_p206 = "c2_p206";
    public static final String residentes_c2_p207 = "c2_p207";
    public static final String residentes_COB200 = "COB200";
    public static final String residentes_encuestado_cobertura = "encuestado_cobertura";




    public static final String SQL_CREATE_TABLA_MODULO2 =
            "CREATE TABLE " + tablaresidentes + "(" +
                    residentes_id + " TEXT PRIMARY KEY," +
                    residentes_id_informante + " TEXT," +
                    residentes_id_hogar + " TEXT," +
                    residentes_id_vivienda + " TEXT," +
                    residentes_numero + " TEXT," +
                    residentes_c2_p202 + " TEXT," +
                    residentes_c2_p203 + " TEXT," +
                    residentes_c2_p204 + " TEXT," +
                    residentes_c2_p205_a + " TEXT," +
                    residentes_c2_p205_m + " TEXT," +
                    residentes_c2_p206 + " TEXT," +
                    residentes_c2_p207 + " TEXT," +
                    residentes_encuestado_cobertura + " TEXT," +
                    residentes_COB200 + " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 3
     * */
    public static final String modulo3_id = "_id";
    public static final String modulo3_id_informante = "idInformante";
    public static final String modulo3_id_hogar = "id_hogar";
    public static final String modulo3_id_vivienda = "id_vivienda";
    public static final String modulo3_c3_p301_d = "c3_p301_d";
    public static final String modulo3_c3_p301_m = "c3_p301_m";
    public static final String modulo3_c3_p301_a = "c3_p301_a";
    public static final String modulo3_c3_p302 = "c3_p302";
    public static final String modulo3_c3_p303_m = "c3_p303_m";
    public static final String modulo3_c3_p303_a = "c3_p303_a";
    public static final String modulo3_c3_p303_no_nacio = "c3_p303_no_nacio";
    public static final String modulo3_c3_p304 = "c3_p304";
    public static final String modulo3_c3_p305 = "c3_p305";
    public static final String modulo3_c3_p305_o = "c3_p305_o";
    public static final String modulo3_c3_p306 = "c3_p306";
    public static final String modulo3_c3_p306_o = "c3_p306_o";
    public static final String modulo3_c3_p307_d = "c3_p307_d";
    public static final String modulo3_c3_p307_m = "c3_p307_m";
    public static final String modulo3_c3_p307_a = "c3_p307_a";
    public static final String modulo3_c3_p308_e = "c3_p308_e";
    public static final String modulo3_c3_p308_m = "c3_p308_m";
    public static final String modulo3_c3_p308_e_seleccion = "c3_p308_e_seleccion";
    public static final String modulo3_c3_p308_m_seleccion = "c3_p308_m_seleccion";
    public static final String modulo3_c3_p310_1 = "c3_p310_1";
    public static final String modulo3_c3_p310_2 = "c3_p310_2";
    public static final String modulo3_c3_p310_3 = "c3_p310_3";
    public static final String modulo3_c3_p310_4 = "c3_p310_4";
    public static final String modulo3_c3_p311 = "c3_p311";
    public static final String modulo3_c3_p312_dist = "c3_p312_dist";
    public static final String modulo3_c3_p312_prov = "c3_p312_prov";
    public static final String modulo3_c3_p312_dep = "c3_p312_dep";
    public static final String modulo3_c3_p313 = "c3_p313";
    public static final String modulo3_c3_p314 = "c3_p314";
    public static final String modulo3_c3_p314_o = "c3_p314_o";
    public static final String modulo3_c3_p315_1 = "c3_p315_1";
    public static final String modulo3_c3_p315_2 = "c3_p315_2";
    public static final String modulo3_c3_p315_3 = "c3_p315_3";
    public static final String modulo3_c3_p315_4 = "c3_p315_4";
    public static final String modulo3_c3_p315_5 = "c3_p315_5";
    public static final String modulo3_c3_p315_6 = "c3_p315_6";
    public static final String modulo3_c3_p315_7 = "c3_p315_7";
    public static final String modulo3_c3_p315_8 = "c3_p315_8";
    public static final String modulo3_c3_p315_9 = "c3_p315_9";
    public static final String modulo3_c3_p315_10 = "c3_p315_10";
    public static final String modulo3_c3_p315_10_o = "c3_p315_10_o";
    public static final String modulo3_c3_p316 = "c3_p316";
    public static final String modulo3_c3_p316_o = "c3_p316_o";
    public static final String modulo3_c3_p317 = "c3_p317";
    public static final String modulo3_c3_p318 = "c3_p318";
    public static final String modulo3_obs_cap3 = "obs_cap3";
    public static final String modulo3_COB300 = "COB300";

    //agregado//
    public static final String seccion_c_p301_nivel = "P301_NIVEL";
    public static final String seccion_c_p301_anio = "P301_ANIO";
    public static final String seccion_c_p301_grado = "P301_GRADO";
    public static final String seccion_c_p301a = "P301A";
    public static final String seccion_c_p302 = "P302";
    public static final String seccion_c_p303_1 = "P303_1";
    public static final String seccion_c_p303_2 = "P303_2";
    public static final String seccion_c_p303_3 = "P303_3";
    public static final String seccion_c_p303_4 = "P303_4";
    public static final String seccion_c_p303_5 = "P303_5";
    public static final String seccion_c_p303_6 = "P303_6";
    public static final String seccion_c_p303_7 = "P303_7";
    public static final String seccion_c_p303_8 = "P303_8";
    public static final String seccion_c_p303_9 = "P303_9";
    public static final String seccion_c_p303_10 = "P303_10";
    public static final String seccion_c_p303_11 = "P303_11";
    public static final String seccion_c_p303_11_o = "P303_11_O";
    public static final String seccion_c_p304 = "P304";
    public static final String seccion_c_p304_c = "P304_C";
    public static final String seccion_c_p304_n = "P304_N";
    public static final String seccion_c_p304a_1 = "P304A_1";
    public static final String seccion_c_p304b_1 = "P304B_1";
    public static final String seccion_c_p304a_2 = "P304A_2";
    public static final String seccion_c_p304b_2 = "P304B_2";
    public static final String seccion_c_p305_1 = "P305_1";
    public static final String seccion_c_p305_1a = "P305_1A";
    public static final String seccion_c_p305_2 = "P305_2";
    public static final String seccion_c_p305_2a = "P305_2A";
    public static final String seccion_c_p305_3 = "P305_3";
    public static final String seccion_c_p305_3a = "P305_3A";
    public static final String seccion_c_p305_4 = "P305_4";
    public static final String seccion_c_p305_4_o = "P305_4_O";
    public static final String seccion_c_p305_4a = "P305_4A";
    public static final String seccion_c_p306_1 = "P306_1";
    public static final String seccion_c_p306_2 = "P306_2";
    public static final String seccion_c_obs_c = "OBS_C";




    public static final String SQL_CREATE_TABLA_MODULO3 =
            "CREATE TABLE " + tablamodulo3 + "(" +
                    modulo3_id  + " TEXT PRIMARY KEY," +
                    modulo3_id_informante + " TEXT," +
                    modulo3_id_hogar + " TEXT," +
                    modulo3_id_vivienda + " TEXT," +
                    /*
                     modulo3_c3_p301_d  + " TEXT," +
                    modulo3_c3_p301_m  + " TEXT," +
                    modulo3_c3_p301_a  + " TEXT," +
                    modulo3_c3_p302  + " TEXT," +
                    modulo3_c3_p303_no_nacio + " TEXT," +
                    modulo3_c3_p303_a + " TEXT," +
                    modulo3_c3_p303_m + " TEXT," +
                    modulo3_c3_p304  + " TEXT," +
                    modulo3_c3_p305  + " TEXT," +
                    modulo3_c3_p305_o  + " TEXT," +
                    modulo3_c3_p306  + " TEXT," +
                    modulo3_c3_p306_o  + " TEXT," +
                    modulo3_c3_p307_d  + " TEXT," +
                    modulo3_c3_p307_m  + " TEXT," +
                    modulo3_c3_p307_a  + " TEXT," +
                    modulo3_c3_p308_e  + " TEXT," +
                    modulo3_c3_p308_m  + " TEXT," +
                    modulo3_c3_p308_e_seleccion  + " TEXT," +
                    modulo3_c3_p308_m_seleccion  + " TEXT," +
                    modulo3_c3_p310_1  + " TEXT," +
                    modulo3_c3_p310_2  + " TEXT," +
                    modulo3_c3_p310_3  + " TEXT," +
                    modulo3_c3_p310_4  + " TEXT," +
                    modulo3_c3_p311  + " TEXT," +
                    modulo3_c3_p312_dist  + " TEXT," +
                    modulo3_c3_p312_prov  + " TEXT," +
                    modulo3_c3_p312_dep  + " TEXT," +
                    modulo3_c3_p313  + " TEXT," +
                    modulo3_c3_p314  + " TEXT," +
                    modulo3_c3_p314_o  + " TEXT," +
                    modulo3_c3_p315_1  + " TEXT," +
                    modulo3_c3_p315_2  + " TEXT," +
                    modulo3_c3_p315_3  + " TEXT," +
                    modulo3_c3_p315_4  + " TEXT," +
                    modulo3_c3_p315_5  + " TEXT," +
                    modulo3_c3_p315_6  + " TEXT," +
                    modulo3_c3_p315_7  + " TEXT," +
                    modulo3_c3_p315_8  + " TEXT," +
                    modulo3_c3_p315_9  + " TEXT," +
                    modulo3_c3_p315_10  + " TEXT," +
                    modulo3_c3_p315_10_o  + " TEXT," +
                    modulo3_c3_p316  + " TEXT," +
                    modulo3_c3_p316_o  + " TEXT," +
                    modulo3_c3_p317  + " TEXT," +
                    modulo3_c3_p318  + " TEXT," +
                    modulo3_obs_cap3  + " TEXT," +
                     */

                    //agregado//
                    seccion_c_p301_nivel  + " TEXT," +
                    seccion_c_p301_anio  + " TEXT," +
                    seccion_c_p301_grado  + " TEXT," +
                    seccion_c_p301a  + " TEXT," +
                    seccion_c_p302  + " TEXT," +
                    seccion_c_p303_1  + " TEXT," +
                    seccion_c_p303_2  + " TEXT," +
                    seccion_c_p303_3  + " TEXT," +
                    seccion_c_p303_4  + " TEXT," +
                    seccion_c_p303_5  + " TEXT," +
                    seccion_c_p303_6  + " TEXT," +
                    seccion_c_p303_7  + " TEXT," +
                    seccion_c_p303_8  + " TEXT," +
                    seccion_c_p303_9  + " TEXT," +
                    seccion_c_p303_10  + " TEXT," +
                    seccion_c_p303_11  + " TEXT," +
                    seccion_c_p303_11_o  + " TEXT," +

                    seccion_c_p304  + " TEXT," +
                    seccion_c_p304_c  + " TEXT," +
                    seccion_c_p304_n  + " TEXT," +
                    seccion_c_p304a_1  + " TEXT," +
                    seccion_c_p304b_1  + " TEXT," +
                    seccion_c_p304a_2  + " TEXT," +
                    seccion_c_p304b_2  + " TEXT," +
                    seccion_c_p305_1  + " TEXT," +
                    seccion_c_p305_1a  + " TEXT," +
                    seccion_c_p305_2  + " TEXT," +
                    seccion_c_p305_2a  + " TEXT," +
                    seccion_c_p305_3  + " TEXT," +
                    seccion_c_p305_3a  + " TEXT," +
                    seccion_c_p305_4  + " TEXT," +
                    seccion_c_p305_4_o  + " TEXT," +
                    seccion_c_p305_4a  + " TEXT," +
                    seccion_c_p306_1  + " TEXT," +
                    seccion_c_p306_2  + " TEXT," +
                    seccion_c_obs_c  + " TEXT," +

                    modulo3_obs_cap3  + " TEXT," +

                    modulo3_COB300 + " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 3 PREGUNTA 309 - RUTAS
     * */
    public static final String modulo3_p309_id = "_id";
    public static final String modulo3_p309_id_encuestado = "id_encuestado";
    public static final String modulo3_p309_id_vivienda = "id_vivienda";
    public static final String modulo3_p309_numero = "numero";
    public static final String modulo3_c3_p309_p = "c3_p309_p";
    public static final String modulo3_c3_p309_p_nom = "c3_p309_p_nom";
    public static final String modulo3_c3_p309_c = "c3_p309_c";
    public static final String modulo3_c3_p309_mod = "c3_p309_mod";
    public static final String modulo3_c3_p309_m = "c3_p309_m";
    public static final String modulo3_c3_p309_m_cod = "c3_p309_m_cod";
    public static final String modulo3_c3_p309_a = "c3_p309_a";
    public static final String modulo3_c3_p309_a_cod = "c3_p309_a_cod";

    public static final String SQL_CREATE_TABLA_MODULO3_P309_RUTAS =
            "CREATE TABLE " + tablam3p309rutas + "(" +
                    modulo3_p309_id  + " TEXT PRIMARY KEY," +
                    modulo3_p309_id_encuestado + " TEXT," +
                    modulo3_p309_id_vivienda + " TEXT," +
                    modulo3_p309_numero  + " TEXT," +
                    modulo3_c3_p309_p  + " TEXT," +
                    modulo3_c3_p309_p_nom  + " TEXT," +
                    modulo3_c3_p309_c  + " TEXT," +
                    modulo3_c3_p309_mod  + " TEXT," +
                    modulo3_c3_p309_m  + " TEXT," +
                    modulo3_c3_p309_m_cod  + " TEXT," +
                    modulo3_c3_p309_a  + " TEXT," +
                    modulo3_c3_p309_a_cod + " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 3 PREGUNTA 318 - PERSONAS
     * */
    public static final String modulo3_p318_id = "_id";
    public static final String modulo3_p318_idEncuestado = "id_encuestado";
    public static final String modulo3_p318_id_vivienda = "id_vivienda";
    public static final String modulo3_p318_numero = "numero";
    public static final String modulo3_c3_p318_f = "c3_p318_f";
    public static final String modulo3_c3_p318_s = "c3_p318_s";
    public static final String modulo3_c3_p318_e = "c3_p318_e";
    public static final String modulo3_c3_p318_p = "c3_p318_p";

    public static final String SQL_CREATE_TABLA_MODULO3_P318_PERSONAS =
            "CREATE TABLE " + tablam3p318personas + "(" +
                    modulo3_p318_id  + " TEXT PRIMARY KEY," +
                    modulo3_p318_idEncuestado + " TEXT," +
                    modulo3_p318_id_vivienda + " TEXT," +
                    modulo3_p318_numero + " TEXT," +
                    modulo3_c3_p318_f  + " TEXT," +
                    modulo3_c3_p318_s  + " TEXT," +
                    modulo3_c3_p318_e  + " TEXT," +
                    modulo3_c3_p318_p + " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 4
     * */
    public static final String modulo4_id = "_id";
    public static final String modulo4_id_informante = "id_informante";
    public static final String modulo4_id_hogar = "id_hogar";
    public static final String modulo4_id_vivienda = "id_vivienda";
    /*
     public static final String modulo4_c4_p401_1 = "c4_p401_1";
    public static final String modulo4_c4_p401_2 = "c4_p401_2";
    public static final String modulo4_c4_p401_3 = "c4_p401_3";
    public static final String modulo4_c4_p401_4 = "c4_p401_4";
    public static final String modulo4_c4_p401_5 = "c4_p401_5";
    public static final String modulo4_c4_p401_o = "c4_p401_o";
    public static final String modulo4_c4_p402 = "c4_p402";
    public static final String modulo4_c4_p403_1 = "c4_p403_1";
    public static final String modulo4_c4_p403_2 = "c4_p403_2";
    public static final String modulo4_c4_p403_3 = "c4_p403_3";
    public static final String modulo4_c4_p403_4 = "c4_p403_4";
    public static final String modulo4_c4_p403_5 = "c4_p403_5";
    public static final String modulo4_c4_p403_6 = "c4_p403_6";
    public static final String modulo4_c4_p403_7 = "c4_p403_7";
    public static final String modulo4_c4_p403_8 = "c4_p403_8";
    public static final String modulo4_c4_p403_9 = "c4_p403_9";
    public static final String modulo4_c4_p403_10 = "c4_p403_10";
    public static final String modulo4_c4_p403_11 = "c4_p403_11";
    public static final String modulo4_c4_p403_12 = "c4_p403_12";
    public static final String modulo4_c4_p403_13 = "c4_p403_13";
    public static final String modulo4_c4_p403_14 = "c4_p403_14";
    public static final String modulo4_c4_p403_o = "c4_p403_o";
    public static final String modulo4_c4_p404 = "c4_p404";
    public static final String modulo4_c4_p405_1 = "c4_p405_1";
    public static final String modulo4_c4_p405_2 = "c4_p405_2";
    public static final String modulo4_c4_p405_3 = "c4_p405_3";
    public static final String modulo4_c4_p405_4 = "c4_p405_4";
    public static final String modulo4_c4_p405_5 = "c4_p405_5";
    public static final String modulo4_c4_p405_6 = "c4_p405_6";
    public static final String modulo4_c4_p405_7 = "c4_p405_7";
    public static final String modulo4_c4_p406_1 = "c4_p406_1";
    public static final String modulo4_c4_p406_2 = "c4_p406_2";
    public static final String modulo4_c4_p406_3 = "c4_p406_3";
    public static final String modulo4_c4_p406_4 = "c4_p406_4";
    public static final String modulo4_c4_p406_5 = "c4_p406_5";
    public static final String modulo4_c4_p406_6 = "c4_p406_6";
    public static final String modulo4_c4_p406_7 = "c4_p406_7";
    public static final String modulo4_c4_p406_8 = "c4_p406_8";
    public static final String modulo4_c4_p406_o = "c4_p406_o";
    public static final String modulo4_c4_p407_1 = "c4_p407_1";
    public static final String modulo4_c4_p407_2 = "c4_p407_2";
    public static final String modulo4_c4_p407_3 = "c4_p407_3";
    public static final String modulo4_c4_p407_4 = "c4_p407_4";
    public static final String modulo4_c4_p407_5 = "c4_p407_5";
    public static final String modulo4_c4_p407_6 = "c4_p407_6";
    public static final String modulo4_c4_p407_7 = "c4_p407_7";
    public static final String modulo4_c4_p407_8 = "c4_p407_8";
    public static final String modulo4_c4_p407_9 = "c4_p407_9";
    public static final String modulo4_c4_p407_10 = "c4_p407_10";
    public static final String modulo4_c4_p407_11 = "c4_p407_11";
    public static final String modulo4_c4_p407_12 = "c4_p407_12";
    public static final String modulo4_c4_p407_13 = "c4_p407_13";
    public static final String modulo4_c4_p407_o = "c4_p407_o";
    public static final String modulo4_c4_p408_1 = "c4_p408_1";
    public static final String modulo4_c4_p408_2 = "c4_p408_2";
    public static final String modulo4_c4_p408_3 = "c4_p408_3";
    public static final String modulo4_c4_p408_4 = "c4_p408_4";
    public static final String modulo4_c4_p408_5 = "c4_p408_5";
    public static final String modulo4_c4_p408_6 = "c4_p408_6";
    public static final String modulo4_c4_p409 = "c4_p409";
    public static final String modulo4_c4_p410 = "c4_p410";
    public static final String modulo4_c4_p411_1 = "c4_p411_1";
    public static final String modulo4_c4_p411_2 = "c4_p411_2";
    public static final String modulo4_c4_p411_3 = "c4_p411_3";
    public static final String modulo4_c4_p411_4 = "c4_p411_4";
    public static final String modulo4_c4_p411_5 = "c4_p411_5";
    public static final String modulo4_c4_p411_6 = "c4_p411_6";
    public static final String modulo4_c4_p411_7 = "c4_p411_7";
    public static final String modulo4_c4_p411_8 = "c4_p411_8";
    public static final String modulo4_c4_p411_9 = "c4_p411_9";
    public static final String modulo4_c4_p411_10 = "c4_p411_10";
    public static final String modulo4_c4_p411_11 = "c4_p411_11";
    public static final String modulo4_c4_p411_12 = "c4_p411_12";
    public static final String modulo4_c4_p411_13 = "c4_p411_13";
    public static final String modulo4_c4_p411_14 = "c4_p411_14";
    public static final String modulo4_c4_p411_o = "c4_p411_o";
    public static final String modulo4_c4_p412 = "c4_p412";
    public static final String modulo4_c4_p413 = "c4_p413";
    public static final String modulo4_c4_p414 = "c4_p414";
    public static final String modulo4_c4_p415 = "c4_p415";
    public static final String modulo4_c4_p416_1 = "c4_p416_1";
    public static final String modulo4_c4_p416_2 = "c4_p416_2";
    public static final String modulo4_c4_p416_3 = "c4_p416_3";
    public static final String modulo4_c4_p416_4 = "c4_p416_4";
    public static final String modulo4_c4_p416_5 = "c4_p416_5";
    public static final String modulo4_c4_p416_6 = "c4_p416_6";
    public static final String modulo4_c4_p416_7 = "c4_p416_7";
    public static final String modulo4_c4_p416_8 = "c4_p416_8";
    public static final String modulo4_c4_p416_o = "c4_p416_o";
    public static final String modulo4_obs_cap4 = "obs_cap4";
     */
    public static final String modulo4_COB400 = "COB400";
    public static final String seccion_d_inf_300 = "INF_300";
    public static final String seccion_d_p307 = "P307";
    public static final String seccion_d_p308a = "P308A";
    public static final String seccion_d_p308a_cod = "P308A_COD";
    public static final String seccion_d_p308b = "P308B";
    public static final String seccion_d_p308b_cod = "P308B_COD";
    public static final String seccion_d_p308c = "P308C";
    public static final String seccion_d_p308c_cod = "P308C_COD";
    public static final String seccion_d_p308d = "P308D";
    public static final String seccion_d_p308d_cod = "P308D_COD";
    public static final String seccion_d_p309 = "P309";
    public static final String seccion_d_p310 = "P310";
    public static final String seccion_d_p311 = "P311";
    public static final String seccion_d_p312_1 = "P312_1";
    public static final String seccion_d_p312_2 = "P312_2";
    public static final String seccion_d_p312_3 = "P312_3";
    public static final String seccion_d_p312_4 = "P312_4";
    public static final String seccion_d_p312_5 = "P312_5";
    public static final String seccion_d_p312_6 = "P312_6";
    public static final String seccion_d_p312_7 = "P312_7";
    public static final String seccion_d_p312_8 = "P312_8";
    public static final String seccion_d_p312_9 = "P312_9";
    public static final String seccion_d_p312_10 = "P312_10";
    public static final String seccion_d_p312_11 = "P312_11";
    public static final String seccion_d_p312_11_o = "P312_11_O";
    public static final String seccion_d_p313 = "P313";
    public static final String seccion_d_p314 = "P314";
    public static final String seccion_d_p314a = "P314A";
    public static final String seccion_d_p314b = "P314B";
    public static final String seccion_d_obs_d = "OBS_D";




    public static final String SQL_CREATE_TABLA_MODULO4 =
            "CREATE TABLE " + tablamodulo4 + "(" +
                    modulo4_id  + " TEXT PRIMARY KEY," +
                    modulo4_id_informante + " TEXT," +
                    modulo4_id_hogar + " TEXT," +
                    modulo4_id_vivienda + " TEXT," +
                    /*
                       modulo4_c4_p401_1  +  " TEXT," +
                    modulo4_c4_p401_2  +  " TEXT," +
                    modulo4_c4_p401_3  +  " TEXT," +
                    modulo4_c4_p401_4  +  " TEXT," +
                    modulo4_c4_p401_5  +  " TEXT," +
                    modulo4_c4_p401_o  +  " TEXT," +
                    modulo4_c4_p402  +  " TEXT," +
                    modulo4_c4_p403_1  +  " TEXT," +
                    modulo4_c4_p403_2  +  " TEXT," +
                    modulo4_c4_p403_3  +  " TEXT," +
                    modulo4_c4_p403_4  +  " TEXT," +
                    modulo4_c4_p403_5  +  " TEXT," +
                    modulo4_c4_p403_6  +  " TEXT," +
                    modulo4_c4_p403_7  +  " TEXT," +
                    modulo4_c4_p403_8  +  " TEXT," +
                    modulo4_c4_p403_9  +  " TEXT," +
                    modulo4_c4_p403_10  +  " TEXT," +
                    modulo4_c4_p403_11  +  " TEXT," +
                    modulo4_c4_p403_12  +  " TEXT," +
                    modulo4_c4_p403_13  +  " TEXT," +
                    modulo4_c4_p403_14  +  " TEXT," +
                    modulo4_c4_p403_o  +  " TEXT," +
                    modulo4_c4_p404  +  " TEXT," +
                    modulo4_c4_p405_1  +  " TEXT," +
                    modulo4_c4_p405_2  +  " TEXT," +
                    modulo4_c4_p405_3  +  " TEXT," +
                    modulo4_c4_p405_4  +  " TEXT," +
                    modulo4_c4_p405_5  +  " TEXT," +
                    modulo4_c4_p405_6  +  " TEXT," +
                    modulo4_c4_p405_7  +  " TEXT," +
                    modulo4_c4_p406_1  +  " TEXT," +
                    modulo4_c4_p406_2  +  " TEXT," +
                    modulo4_c4_p406_3  +  " TEXT," +
                    modulo4_c4_p406_4  +  " TEXT," +
                    modulo4_c4_p406_5  +  " TEXT," +
                    modulo4_c4_p406_6  +  " TEXT," +
                    modulo4_c4_p406_7  +  " TEXT," +
                    modulo4_c4_p406_8  +  " TEXT," +
                    modulo4_c4_p406_o  +  " TEXT," +
                    modulo4_c4_p407_1  +  " TEXT," +
                    modulo4_c4_p407_2  +  " TEXT," +
                    modulo4_c4_p407_3  +  " TEXT," +
                    modulo4_c4_p407_4  +  " TEXT," +
                    modulo4_c4_p407_5  +  " TEXT," +
                    modulo4_c4_p407_6  +  " TEXT," +
                    modulo4_c4_p407_7  +  " TEXT," +
                    modulo4_c4_p407_8  +  " TEXT," +
                    modulo4_c4_p407_9  +  " TEXT," +
                    modulo4_c4_p407_10  +  " TEXT," +
                    modulo4_c4_p407_11  +  " TEXT," +
                    modulo4_c4_p407_12  +  " TEXT," +
                    modulo4_c4_p407_13  +  " TEXT," +
                    modulo4_c4_p407_o  +  " TEXT," +
                    modulo4_c4_p408_1  +  " TEXT," +
                    modulo4_c4_p408_2  +  " TEXT," +
                    modulo4_c4_p408_3  +  " TEXT," +
                    modulo4_c4_p408_4  +  " TEXT," +
                    modulo4_c4_p408_5  +  " TEXT," +
                    modulo4_c4_p408_6  +  " TEXT," +
                    modulo4_c4_p409  +  " TEXT," +
                    modulo4_c4_p410  +  " TEXT," +
                    modulo4_c4_p411_1  +  " TEXT," +
                    modulo4_c4_p411_2  +  " TEXT," +
                    modulo4_c4_p411_3  +  " TEXT," +
                    modulo4_c4_p411_4  +  " TEXT," +
                    modulo4_c4_p411_5  +  " TEXT," +
                    modulo4_c4_p411_6  +  " TEXT," +
                    modulo4_c4_p411_7  +  " TEXT," +
                    modulo4_c4_p411_8  +  " TEXT," +
                    modulo4_c4_p411_9  +  " TEXT," +
                    modulo4_c4_p411_10  +  " TEXT," +
                    modulo4_c4_p411_11  +  " TEXT," +
                    modulo4_c4_p411_12  +  " TEXT," +
                    modulo4_c4_p411_13  +  " TEXT," +
                    modulo4_c4_p411_14  +  " TEXT," +
                    modulo4_c4_p411_o  +  " TEXT," +
                    modulo4_c4_p412  +  " TEXT," +
                    modulo4_c4_p413  +  " TEXT," +
                    modulo4_c4_p414  +  " TEXT," +
                    modulo4_c4_p415  +  " TEXT," +
                    modulo4_c4_p416_1  +  " TEXT," +
                    modulo4_c4_p416_2  +  " TEXT," +
                    modulo4_c4_p416_3  +  " TEXT," +
                    modulo4_c4_p416_4  +  " TEXT," +
                    modulo4_c4_p416_5  +  " TEXT," +
                    modulo4_c4_p416_6  +  " TEXT," +
                    modulo4_c4_p416_7  +  " TEXT," +
                    modulo4_c4_p416_8  +  " TEXT," +
                    modulo4_c4_p416_o  +  " TEXT," +
                    modulo4_obs_cap4  +  " TEXT," +
                     */
                    seccion_d_inf_300 + " TEXT," +
                    seccion_d_p307 + " TEXT," +
                    seccion_d_p308a + " TEXT," +
                    seccion_d_p308a_cod + " TEXT," +
                    seccion_d_p308b + " TEXT," +
                    seccion_d_p308b_cod + " TEXT," +
                    seccion_d_p308c + " TEXT," +
                    seccion_d_p308c_cod + " TEXT," +
                    seccion_d_p308d + " TEXT," +
                    seccion_d_p308d_cod + " TEXT," +
                    seccion_d_p309 + " TEXT," +
                    seccion_d_p310 + " TEXT," +
                    seccion_d_p311 + " TEXT," +
                    seccion_d_p312_1 + " TEXT," +
                    seccion_d_p312_2 + " TEXT," +
                    seccion_d_p312_3 + " TEXT," +
                    seccion_d_p312_4 + " TEXT," +
                    seccion_d_p312_5 + " TEXT," +
                    seccion_d_p312_6 + " TEXT," +
                    seccion_d_p312_7 + " TEXT," +
                    seccion_d_p312_8 + " TEXT," +
                    seccion_d_p312_9 + " TEXT," +
                    seccion_d_p312_10 + " TEXT," +
                    seccion_d_p312_11 + " TEXT," +
                    seccion_d_p312_11_o + " TEXT," +
                    seccion_d_p313 + " TEXT," +
                    seccion_d_p314 + " TEXT," +
                    seccion_d_p314a + " TEXT," +
                    seccion_d_p314b + " TEXT," +
                    seccion_d_obs_d + " TEXT," +
                    modulo4_COB400  +  " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 5
     * */
    public static final String modulo5_id = "_id";
    public static final String modulo5_id_informante = "id_informante";
    public static final String modulo5_id_hogar = "id_hogar";
    public static final String modulo5_id_vivienda = "id_vivienda";
    /*
    public static final String modulo5_c5_p501 = "c5_p501";
    public static final String modulo5_c5_p502_c = "c5_p502_c";
    public static final String modulo5_c5_p502_eleccion = "c5_p502_eleccion";
    public static final String modulo5_c5_p502 = "c5_p502";
    public static final String modulo5_c5_p502_o = "c5_p502_o";
    public static final String modulo5_c5_p503 = "c5_p503";
    public static final String modulo5_c5_p504 = "c5_p504";
    public static final String modulo5_c5_p505 = "c5_p505";
    public static final String modulo5_c5_p506_1 = "c5_p506_1";
    public static final String modulo5_c5_p506_2 = "c5_p506_2";
    public static final String modulo5_c5_p506_3 = "c5_p506_3";
    public static final String modulo5_c5_p506_4 = "c5_p506_4";
    public static final String modulo5_c5_p507 = "c5_p507";
    public static final String modulo5_c5_p507_dist = "c5_p507_dist";
    public static final String modulo5_c5_p507_prov = "c5_p507_prov";
    public static final String modulo5_c5_p507_dep = "c5_p507_dep";
    public static final String modulo5_c5_p508_1 = "c5_p508_1";
    public static final String modulo5_c5_p508_2 = "c5_p508_2";
    public static final String modulo5_c5_p508_3 = "c5_p508_3";
    public static final String modulo5_c5_p508_4 = "c5_p508_4";
    public static final String modulo5_c5_p508_5 = "c5_p508_5";
    public static final String modulo5_c5_p508_6 = "c5_p508_6";
    public static final String modulo5_c5_p508_7 = "c5_p508_7";
    public static final String modulo5_c5_p508_8 = "c5_p508_8";
    public static final String modulo5_c5_p508_9 = "c5_p508_9";
    public static final String modulo5_c5_p508_10 = "c5_p508_10";
    public static final String modulo5_c5_p508_11 = "c5_p508_11";
    public static final String modulo5_c5_p508_o = "c5_p508_o";
    public static final String modulo5_c5_p509 = "c5_p509";
    public static final String modulo5_c5_p510 = "c5_p510";
    public static final String modulo5_c5_p511 = "c5_p511";
    public static final String modulo5_c5_p511_o = "c5_p511_o";
    public static final String modulo5_c5_p512 = "c5_p512";
    public static final String modulo5_c5_p512_o = "c5_p512_o";
    public static final String modulo5_c5_p513 = "c5_p513";
    public static final String modulo5_c5_p513_o = "c5_p513_o";
    public static final String modulo5_obs_cap5 = "obs_cap5";
     */
    public static final String modulo5_COB500 = "COB500";
    //neuvos
    public static final String seccion_e_inf_300 = "INF_300";
    public static final String seccion_e_p315_1 = "P315_1";
    public static final String seccion_e_p315_2 = "P315_2";
    public static final String seccion_e_p315_3 = "P315_3";
    public static final String seccion_e_p315_4 = "P315_4";
    public static final String seccion_e_p315_5 = "P315_5";
    public static final String seccion_e_p315_6 = "P315_6";
    public static final String seccion_e_p315_7 = "P315_7";
    public static final String seccion_e_p315_8 = "P315_8";
    public static final String seccion_e_p315_9 = "P315_9";
    public static final String seccion_e_p315_10 = "P315_10";
    public static final String seccion_e_p315_11 = "P315_11";
    public static final String seccion_e_p315_11_o = "P315_11_O";
    public static final String seccion_e_p316 = "P316";
    public static final String seccion_e_p316_1 = "P316_1";
    public static final String seccion_e_p316a_1 = "P316A_1";
    public static final String seccion_e_p316a_2 = "P316A_2";
    public static final String seccion_e_p316a_3 = "P316A_3";
    public static final String seccion_e_p316a_4 = "P316A_4";
    public static final String seccion_e_p316a_5 = "P316A_5";
    public static final String seccion_e_p316a_6 = "P316A_6";
    public static final String seccion_e_p316a_7 = "P316A_7";
    public static final String seccion_e_p316a_8 = "P316A_8";
    public static final String seccion_e_p317 = "P317";
    public static final String seccion_e_p318 = "P318";
    public static final String seccion_e_p319 = "P319";
    public static final String seccion_e_p319a = "P319A";
    public static final String seccion_e_p319a_o = "P319A_O";
    public static final String seccion_e_p320 = "P320";
    public static final String seccion_e_p320_o = "P320_O";
    public static final String seccion_e_p321 = "P321";
    public static final String seccion_e_p322_m = "P322_M";
    public static final String seccion_e_p322_a = "P322_A";
    public static final String seccion_e_p322_n = "P322_N";
    public static final String seccion_e_p323_m = "P323_M";
    public static final String seccion_e_p323_a = "P323_A";
    public static final String seccion_e_p323_t = "P323_T";
    public static final String seccion_e_p323_n = "P323_N";
    public static final String seccion_e_p324 = "P324";
    public static final String seccion_e_p324a = "P324A";
    public static final String seccion_e_p324a_cod = "P324A_COD";
    public static final String seccion_e_p324b = "P324B";
    public static final String seccion_e_p324b_cod = "P324B_COD";
    public static final String seccion_e_p324c = "P324C";
    public static final String seccion_e_p324c_cod = "P324C_COD";
    public static final String seccion_e_p324d = "P324D";
    public static final String seccion_e_p324d_cod = "P324D_COD";
    public static final String seccion_e_p325_d = "P325_D";
    public static final String seccion_e_p325_h = "P325_H";
    public static final String seccion_e_obs_e = "OBS_E";




    public static final String SQL_CREATE_TABLA_MODULO5 =
            "CREATE TABLE " + tablamodulo5 + "(" +
                    modulo5_id  + " TEXT PRIMARY KEY," +
                    modulo5_id_informante + " TEXT," +
                    modulo5_id_hogar + " TEXT," +
                    modulo5_id_vivienda + " TEXT," +
                    /*
                    modulo5_c5_p501  +  " TEXT," +
                    modulo5_c5_p502_c  +  " TEXT," +
                    modulo5_c5_p502  +  " TEXT," +
                    modulo5_c5_p502_eleccion  +  " TEXT," +
                    modulo5_c5_p502_o  +  " TEXT," +
                    modulo5_c5_p503  +  " TEXT," +
                    modulo5_c5_p504  +  " TEXT," +
                    modulo5_c5_p505  +  " TEXT," +
                    modulo5_c5_p506_1  +  " TEXT," +
                    modulo5_c5_p506_2  +  " TEXT," +
                    modulo5_c5_p506_3  +  " TEXT," +
                    modulo5_c5_p506_4  +  " TEXT," +
                    modulo5_c5_p507  +  " TEXT," +
                    modulo5_c5_p507_dist  +  " TEXT," +
                    modulo5_c5_p507_prov  +  " TEXT," +
                    modulo5_c5_p507_dep  +  " TEXT," +
                    modulo5_c5_p508_1  +  " TEXT," +
                    modulo5_c5_p508_2  +  " TEXT," +
                    modulo5_c5_p508_3  +  " TEXT," +
                    modulo5_c5_p508_4  +  " TEXT," +
                    modulo5_c5_p508_5  +  " TEXT," +
                    modulo5_c5_p508_6  +  " TEXT," +
                    modulo5_c5_p508_7  +  " TEXT," +
                    modulo5_c5_p508_8  +  " TEXT," +
                    modulo5_c5_p508_9  +  " TEXT," +
                    modulo5_c5_p508_10  +  " TEXT," +
                    modulo5_c5_p508_11  +  " TEXT," +
                    modulo5_c5_p508_o  +  " TEXT," +
                    modulo5_c5_p509  +  " TEXT," +
                    modulo5_c5_p510  +  " TEXT," +
                    modulo5_c5_p511  +  " TEXT," +
                    modulo5_c5_p511_o  +  " TEXT," +
                    modulo5_c5_p512  +  " TEXT," +
                    modulo5_c5_p512_o  +  " TEXT," +
                    modulo5_c5_p513  +  " TEXT," +
                    modulo5_c5_p513_o  +  " TEXT," +
                    modulo5_obs_cap5  +  " TEXT," +
                     */
                    seccion_e_inf_300 + " TEXT," +
                    seccion_e_p315_1 + " TEXT," +
                    seccion_e_p315_2 + " TEXT," +
                    seccion_e_p315_3 + " TEXT," +
                    seccion_e_p315_4 + " TEXT," +
                    seccion_e_p315_5 + " TEXT," +
                    seccion_e_p315_6 + " TEXT," +
                    seccion_e_p315_7 + " TEXT," +
                    seccion_e_p315_8 + " TEXT," +
                    seccion_e_p315_9 + " TEXT," +
                    seccion_e_p315_10 + " TEXT," +
                    seccion_e_p315_11 + " TEXT," +
                    seccion_e_p315_11_o + " TEXT," +
                    seccion_e_p316 + " TEXT," +
                    seccion_e_p316_1 + " TEXT," +
                    seccion_e_p316a_1 + " TEXT," +
                    seccion_e_p316a_2 + " TEXT," +
                    seccion_e_p316a_3 + " TEXT," +
                    seccion_e_p316a_4 + " TEXT," +
                    seccion_e_p316a_5 + " TEXT," +
                    seccion_e_p316a_6 + " TEXT," +
                    seccion_e_p316a_7 + " TEXT," +
                    seccion_e_p316a_8 + " TEXT," +
                    seccion_e_p317 + " TEXT," +
                    seccion_e_p318 + " TEXT," +
                    seccion_e_p319 + " TEXT," +
                    seccion_e_p319a + " TEXT," +
                    seccion_e_p319a_o + " TEXT," +
                    seccion_e_p320 + " TEXT," +
                    seccion_e_p320_o + " TEXT," +
                    seccion_e_p321 + " TEXT," +
                    seccion_e_p322_m + " TEXT," +
                    seccion_e_p322_a + " TEXT," +
                    seccion_e_p322_n + " TEXT," +
                    seccion_e_p323_m + " TEXT," +
                    seccion_e_p323_a + " TEXT," +
                    seccion_e_p323_t + " TEXT," +
                    seccion_e_p323_n + " TEXT," +
                    seccion_e_p324 + " TEXT," +
                    seccion_e_p324a + " TEXT," +
                    seccion_e_p324a_cod + " TEXT," +
                    seccion_e_p324b + " TEXT," +
                    seccion_e_p324b_cod + " TEXT," +
                    seccion_e_p324c + " TEXT," +
                    seccion_e_p324c_cod + " TEXT," +
                    seccion_e_p324d + " TEXT," +
                    seccion_e_p324d_cod + " TEXT," +
                    seccion_e_p325_d + " TEXT," +
                    seccion_e_p325_h + " TEXT," +
                    seccion_e_obs_e + " TEXT," +
                    modulo5_COB500  +  " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 6
     * */
    public static final String modulo6_id = "_id";
    public static final String modulo6_id_informante = "id_informante";
    public static final String modulo6_id_hogar = "id_hogar";
    public static final String modulo6_id_vivienda = "id_vivienda";
    /*
    public static final String modulo6_c6_p601 = "c6_p601";
    public static final String modulo6_c6_p602 = "c6_p602";
    public static final String modulo6_c6_p603 = "c6_p603";
    public static final String modulo6_c6_p604_1 = "c6_p604_1";
    public static final String modulo6_c6_p604_2 = "c6_p604_2";
    public static final String modulo6_c6_p604_3 = "c6_p604_3";
    public static final String modulo6_c6_p604_4 = "c6_p604_4";
    public static final String modulo6_c6_p604_5 = "c6_p604_5";
    public static final String modulo6_c6_p604_6 = "c6_p604_6";
    public static final String modulo6_c6_p604_7 = "c6_p604_7";
    public static final String modulo6_c6_p604_8 = "c6_p604_8";
    public static final String modulo6_c6_p604_9 = "c6_p604_9";
    public static final String modulo6_c6_p604_10 = "c6_p604_10";
    public static final String modulo6_c6_p604_11 = "c6_p604_11";
    public static final String modulo6_c6_p604_o = "c6_p604_o";
    public static final String modulo6_c6_p605 = "c6_p605";
    public static final String modulo6_c6_p606 = "c6_p606";
    public static final String modulo6_c6_p607 = "c6_p607";
    public static final String modulo6_c6_p608 = "c6_p608";
    public static final String modulo6_c6_p608_o = "c6_p608_o";
    public static final String modulo6_c6_p609 = "c6_p609";
    public static final String modulo6_c6_p610_pd = "c6_p610_pd";
    public static final String modulo6_c6_p610_pl = "c6_p610_pl";
    public static final String modulo6_c6_p610_pm = "c6_p610_pm";
    public static final String modulo6_c6_p610_pmi = "c6_p610_pmi";
    public static final String modulo6_c6_p610_pj = "c6_p610_pj";
    public static final String modulo6_c6_p610_pv = "c6_p610_pv";
    public static final String modulo6_c6_p610_ps = "c6_p610_ps";
    public static final String modulo6_c6_p610_pt = "c6_p610_pt";
    public static final String modulo6_c6_p610_sd = "c6_p610_sd";
    public static final String modulo6_c6_p610_sl = "c6_p610_sl";
    public static final String modulo6_c6_p610_sm = "c6_p610_sm";
    public static final String modulo6_c6_p610_smi = "c6_p610_smi";
    public static final String modulo6_c6_p610_sj = "c6_p610_sj";
    public static final String modulo6_c6_p610_sv = "c6_p610_sv";
    public static final String modulo6_c6_p610_ss = "c6_p610_ss";
    public static final String modulo6_c6_p610_st = "c6_p610_st";
    public static final String modulo6_c6_p610_t = "c6_p610_t";
    public static final String modulo6_c6_p611 = "c6_p611";
    public static final String modulo6_c6_p611a = "c6_p611a";
    public static final String modulo6_c6_p611b = "c6_p611b";
    public static final String modulo6_c6_p612 = "c6_p612";
    public static final String modulo6_c6_p612_nro = "c6_p612_nro";
    public static final String modulo6_c6_p613 = "c6_p613";
    public static final String modulo6_c6_p614_mon = "c6_p614_mon";
    public static final String modulo6_c6_p614_esp = "c6_p614_esp";
    public static final String modulo6_c6_p615_mon = "c6_p615_mon";
    public static final String modulo6_c6_p615_esp = "c6_p615_esp";
    public static final String modulo6_c6_p616_mon = "c6_p616_mon";
    public static final String modulo6_c6_p616_esp = "c6_p616_esp";
    public static final String modulo6_c6_p616_nas = "c6_p616_nas";
    public static final String modulo6_c6_p617 = "c6_p617";
    public static final String modulo6_c6_p617_dist = "c6_p617_dist";
    public static final String modulo6_c6_p617_prov = "c6_p617_prov";
    public static final String modulo6_c6_p617_dep = "c6_p617_dep";
    public static final String modulo6_c6_p625_1 = "c6_p625_1";
    public static final String modulo6_c6_p625_2 = "c6_p625_2";
    public static final String modulo6_c6_p625_3 = "c6_p625_3";
    public static final String modulo6_c6_p625_4 = "c6_p625_4";
    public static final String modulo6_c6_p625_5 = "c6_p625_5";
    public static final String modulo6_c6_p625_6 = "c6_p625_6";
    public static final String modulo6_c6_p625_o = "c6_p625_o";
    public static final String modulo6_c6_p618 = "c6_p618";
    public static final String modulo6_c6_p619 = "c6_p619";
    public static final String modulo6_c6_p619_o = "c6_p619_o";
    public static final String modulo6_c6_p620 = "c6_p620";
    public static final String modulo6_c6_p621 = "c6_p621";
    public static final String modulo6_c6_p622 = "c6_p622";
    public static final String modulo6_c6_p622_o = "c6_p622_o";
    public static final String modulo6_c6_p623 = "c6_p623";
    public static final String modulo6_c6_p623_o = "c6_p623_o";
    public static final String modulo6_c6_p624 = "c6_p624";
    public static final String modulo6_c6_p626 = "c6_p626";
    public static final String modulo6_c6_p627 = "c6_p627";
    public static final String modulo6_c6_p628 = "c6_p628";
    public static final String modulo6_c6_p629_1 = "c6_p629_1";
    public static final String modulo6_c6_p629_2 = "c6_p629_2";
    public static final String modulo6_c6_p629_3 = "c6_p629_3";
    public static final String modulo6_c6_p629_4 = "c6_p629_4";
    public static final String modulo6_c6_p629_o = "c6_p629_o";
    public static final String modulo6_c6_p629_1_f = "c6_p629_1_f";
    public static final String modulo6_c6_p629_1_m = "c6_p629_1_m";
    public static final String modulo6_c6_p629_2_f = "c6_p629_2_f";
    public static final String modulo6_c6_p629_2_m = "c6_p629_2_m";
    public static final String modulo6_c6_p629_3_f = "c6_p629_3_f";
    public static final String modulo6_c6_p629_3_m = "c6_p629_3_m";
    public static final String modulo6_c6_p629_4_f = "c6_p629_4_f";
    public static final String modulo6_c6_p629_4_m = "c6_p629_4_m";
    public static final String modulo6_c6_p630_1 = "c6_p630_1";
    public static final String modulo6_c6_p630_1med = "c6_p630_1med";
    public static final String modulo6_c6_p630_1o = "c6_p630_1o";
    public static final String modulo6_c6_p630_1frec = "c6_p630_1frec";
    public static final String modulo6_c6_p630_1frec_o = "c6_p630_1frec_o";
    public static final String modulo6_c6_p630_1mont = "c6_p630_1mont";
    public static final String modulo6_c6_p630_2 = "c6_p630_2";
    public static final String modulo6_c6_p630_2med = "c6_p630_2med";
    public static final String modulo6_c6_p630_2o = "c6_p630_2o";
    public static final String modulo6_c6_p630_2frec = "c6_p630_2frec";
    public static final String modulo6_c6_p630_2frec_o = "c6_p630_2frec_o";
    public static final String modulo6_c6_p630_2mont = "c6_p630_2mont";
    public static final String modulo6_obs_cap6 = "obs_cap6";
     */
    public static final String modulo6_COB600 = "COB600";
    //NUEVOS
    public static final String seccion_f_p326 = "P326";
    public static final String seccion_f_p327 = "P327";
    public static final String seccion_f_p327_o = "P327_O";
    public static final String seccion_f_p328 = "P328";
    public static final String seccion_f_p329_1 = "P329_1";
    public static final String seccion_f_p329_2 = "P329_2";
    public static final String seccion_f_p329_3 = "P329_3";
    public static final String seccion_f_p329_4 = "P329_4";
    public static final String seccion_f_p329_5 = "P329_5";
    public static final String seccion_f_p329_6 = "P329_6";
    public static final String seccion_f_p329_7 = "P329_7";
    public static final String seccion_f_p329_8 = "P329_8";
    public static final String seccion_f_p329_8_o = "P329_8_O";
    public static final String seccion_f_p330 = "P330";
    public static final String seccion_f_p331 = "P331";
    public static final String seccion_f_p332 = "P332";
    public static final String seccion_f_p333_1 = "P333_1";
    public static final String seccion_f_p333_2 = "P333_2";
    public static final String seccion_f_p333_3 = "P333_3";
    public static final String seccion_f_p333_4 = "P333_4";
    public static final String seccion_f_p333_5 = "P333_5";
    public static final String seccion_f_p333_5_o = "P333_5_O";
    public static final String seccion_f_p334 = "P334";
    public static final String seccion_f_p335 = "P335";
    public static final String seccion_f_p336_1 = "P336_1";
    public static final String seccion_f_p337_1 = "P337_1";
    public static final String seccion_f_p336_2 = "P336_2";
    public static final String seccion_f_p337_2 = "P337_2";
    public static final String seccion_f_p336_3 = "P336_3";
    public static final String seccion_f_p337_3 = "P337_3";
    public static final String seccion_f_p336_4 = "P336_4";
    public static final String seccion_f_p337_4 = "P337_4";
    public static final String seccion_f_p336_5 = "P336_5";
    public static final String seccion_f_p337_5 = "P337_5";
    public static final String seccion_f_p336_6 = "P336_6";
    public static final String seccion_f_p337_6 = "P337_6";
    public static final String seccion_f_p336_7 = "P336_7";
    public static final String seccion_f_p336_7_o = "P336_7_O";
    public static final String seccion_f_p337_7 = "P337_7";
    public static final String seccion_f_p336_8 = "P336_8";
    public static final String seccion_f_p336_8_o = "P336_8_O";
    public static final String seccion_f_p337_8 = "P337_8";
    public static final String seccion_f_obs_f = "OBS_F";

    public static final String SQL_CREATE_TABLA_MODULO6 =
            "CREATE TABLE " + tablamodulo6 + "(" +
                    modulo6_id  + " TEXT PRIMARY KEY," +
                    modulo6_id_informante + " TEXT," +
                    modulo6_id_hogar + " TEXT," +
                    modulo6_id_vivienda + " TEXT," +
                    /*
                     modulo6_c6_p601  +  " TEXT," +
                    modulo6_c6_p602  +  " TEXT," +
                    modulo6_c6_p603  +  " TEXT," +
                    modulo6_c6_p604_1  +  " TEXT," +
                    modulo6_c6_p604_2  +  " TEXT," +
                    modulo6_c6_p604_3  +  " TEXT," +
                    modulo6_c6_p604_4  +  " TEXT," +
                    modulo6_c6_p604_5  +  " TEXT," +
                    modulo6_c6_p604_6  +  " TEXT," +
                    modulo6_c6_p604_7  +  " TEXT," +
                    modulo6_c6_p604_8  +  " TEXT," +
                    modulo6_c6_p604_9  +  " TEXT," +
                    modulo6_c6_p604_10  +  " TEXT," +
                    modulo6_c6_p604_11  +  " TEXT," +
                    modulo6_c6_p604_o  +  " TEXT," +
                    modulo6_c6_p605  +  " TEXT," +
                    modulo6_c6_p606  +  " TEXT," +
                    modulo6_c6_p607  +  " TEXT," +
                    modulo6_c6_p608  +  " TEXT," +
                    modulo6_c6_p608_o  +  " TEXT," +
                    modulo6_c6_p609  +  " TEXT," +
                    modulo6_c6_p610_pd  +  " TEXT," +
                    modulo6_c6_p610_pl  +  " TEXT," +
                    modulo6_c6_p610_pm  +  " TEXT," +
                    modulo6_c6_p610_pmi  +  " TEXT," +
                    modulo6_c6_p610_pj  +  " TEXT," +
                    modulo6_c6_p610_pv  +  " TEXT," +
                    modulo6_c6_p610_ps  +  " TEXT," +
                    modulo6_c6_p610_pt  +  " TEXT," +
                    modulo6_c6_p610_sd  +  " TEXT," +
                    modulo6_c6_p610_sl  +  " TEXT," +
                    modulo6_c6_p610_sm  +  " TEXT," +
                    modulo6_c6_p610_smi  +  " TEXT," +
                    modulo6_c6_p610_sj  +  " TEXT," +
                    modulo6_c6_p610_sv  +  " TEXT," +
                    modulo6_c6_p610_ss  +  " TEXT," +
                    modulo6_c6_p610_st  +  " TEXT," +
                    modulo6_c6_p610_t  +  " TEXT," +
                    modulo6_c6_p611  +  " TEXT," +
                    modulo6_c6_p611a  +  " TEXT," +
                    modulo6_c6_p611b  +  " TEXT," +
                    modulo6_c6_p612  +  " TEXT," +
                    modulo6_c6_p612_nro  +  " TEXT," +
                    modulo6_c6_p613  +  " TEXT," +
                    modulo6_c6_p614_mon  +  " TEXT," +
                    modulo6_c6_p614_esp  +  " TEXT," +
                    modulo6_c6_p615_mon  +  " TEXT," +
                    modulo6_c6_p615_esp  +  " TEXT," +
                    modulo6_c6_p616_mon  +  " TEXT," +
                    modulo6_c6_p616_esp  +  " TEXT," +
                    modulo6_c6_p616_nas  +  " TEXT," +
                    modulo6_c6_p617  +  " TEXT," +
                    modulo6_c6_p617_dist  +  " TEXT," +
                    modulo6_c6_p617_prov  +  " TEXT," +
                    modulo6_c6_p617_dep  +  " TEXT," +
                    modulo6_c6_p625_1  +  " TEXT," +
                    modulo6_c6_p625_2  +  " TEXT," +
                    modulo6_c6_p625_3  +  " TEXT," +
                    modulo6_c6_p625_4  +  " TEXT," +
                    modulo6_c6_p625_5  +  " TEXT," +
                    modulo6_c6_p625_6  +  " TEXT," +
                    modulo6_c6_p625_o  +  " TEXT," +
                    modulo6_c6_p618  +  " TEXT," +
                    modulo6_c6_p619  +  " TEXT," +
                    modulo6_c6_p619_o  +  " TEXT," +
                    modulo6_c6_p620  +  " TEXT," +
                    modulo6_c6_p621  +  " TEXT," +
                    modulo6_c6_p622  +  " TEXT," +
                    modulo6_c6_p622_o  +  " TEXT," +
                    modulo6_c6_p623  +  " TEXT," +
                    modulo6_c6_p623_o  +  " TEXT," +
                    modulo6_c6_p624  +  " TEXT," +
                    modulo6_c6_p626  +  " TEXT," +
                    modulo6_c6_p627  +  " TEXT," +
                    modulo6_c6_p628  +  " TEXT," +
                    modulo6_c6_p629_1  +  " TEXT," +
                    modulo6_c6_p629_2  +  " TEXT," +
                    modulo6_c6_p629_3  +  " TEXT," +
                    modulo6_c6_p629_4  +  " TEXT," +
                    modulo6_c6_p629_o  +  " TEXT," +
                    modulo6_c6_p629_1_f  +  " TEXT," +
                    modulo6_c6_p629_1_m  +  " TEXT," +
                    modulo6_c6_p629_2_f  +  " TEXT," +
                    modulo6_c6_p629_2_m  +  " TEXT," +
                    modulo6_c6_p629_3_f  +  " TEXT," +
                    modulo6_c6_p629_3_m  +  " TEXT," +
                    modulo6_c6_p629_4_f  +  " TEXT," +
                    modulo6_c6_p629_4_m  +  " TEXT," +
                    modulo6_c6_p630_1  +  " TEXT," +
                    modulo6_c6_p630_1med  +  " TEXT," +
                    modulo6_c6_p630_1o  +  " TEXT," +
                    modulo6_c6_p630_1frec  +  " TEXT," +
                    modulo6_c6_p630_1frec_o  +  " TEXT," +
                    modulo6_c6_p630_1mont  +  " TEXT," +
                    modulo6_c6_p630_2  +  " TEXT," +
                    modulo6_c6_p630_2med  +  " TEXT," +
                    modulo6_c6_p630_2o  +  " TEXT," +
                    modulo6_c6_p630_2frec  +  " TEXT," +
                    modulo6_c6_p630_2frec_o  +  " TEXT," +
                    modulo6_c6_p630_2mont  +  " TEXT," +
                    modulo6_obs_cap6  +  " TEXT," +
                     */
                    seccion_f_p326 + " TEXT," +
                    seccion_f_p327 + " TEXT," +
                    seccion_f_p327_o + " TEXT," +
                    seccion_f_p328 + " TEXT," +
                    seccion_f_p329_1 + " TEXT," +
                    seccion_f_p329_2 + " TEXT," +
                    seccion_f_p329_3 + " TEXT," +
                    seccion_f_p329_4 + " TEXT," +
                    seccion_f_p329_5 + " TEXT," +
                    seccion_f_p329_6 + " TEXT," +
                    seccion_f_p329_7 + " TEXT," +
                    seccion_f_p329_8 + " TEXT," +
                    seccion_f_p329_8_o + " TEXT," +
                    seccion_f_p330 + " TEXT," +
                    seccion_f_p331 + " TEXT," +
                    seccion_f_p332 + " TEXT," +
                    seccion_f_p333_1 + " TEXT," +
                    seccion_f_p333_2 + " TEXT," +
                    seccion_f_p333_3 + " TEXT," +
                    seccion_f_p333_4 + " TEXT," +
                    seccion_f_p333_5 + " TEXT," +
                    seccion_f_p333_5_o + " TEXT," +
                    seccion_f_p334 + " TEXT," +
                    seccion_f_p335 + " TEXT," +
                    seccion_f_p336_1 + " TEXT," +
                    seccion_f_p337_1 + " TEXT," +
                    seccion_f_p336_2 + " TEXT," +
                    seccion_f_p337_2 + " TEXT," +
                    seccion_f_p336_3 + " TEXT," +
                    seccion_f_p337_3 + " TEXT," +
                    seccion_f_p336_4 + " TEXT," +
                    seccion_f_p337_4 + " TEXT," +
                    seccion_f_p336_5 + " TEXT," +
                    seccion_f_p337_5 + " TEXT," +
                    seccion_f_p336_6 + " TEXT," +
                    seccion_f_p337_6 + " TEXT," +
                    seccion_f_p336_7 + " TEXT," +
                    seccion_f_p336_7_o + " TEXT," +
                    seccion_f_p337_7 + " TEXT," +
                    seccion_f_p336_8 + " TEXT," +
                    seccion_f_p336_8_o + " TEXT," +
                    seccion_f_p337_8 + " TEXT," +
                    seccion_f_obs_f + " TEXT," +
                    modulo6_COB600  +  " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 7
     * */
    public static final String modulo7_id = "_id";
    public static final String modulo7_id_informante = "id_informante";
    public static final String modulo7_id_hogar = "id_hogar";
    public static final String modulo7_id_vivienda = "id_vivienda";
    /*
    public static final String modulo7_c7_p701 = "c7_p701";
    public static final String modulo7_c7_p702_1 = "c7_p702_1";
    public static final String modulo7_c7_p702_2 = "c7_p702_2";
    public static final String modulo7_c7_p702_3 = "c7_p702_3";
    public static final String modulo7_c7_p702_4 = "c7_p702_4";
    public static final String modulo7_c7_p702_5 = "c7_p702_5";
    public static final String modulo7_c7_p702_6 = "c7_p702_6";
    public static final String modulo7_c7_p702_7 = "c7_p702_7";
    public static final String modulo7_c7_p702_8 = "c7_p702_8";
    public static final String modulo7_c7_p702_9 = "c7_p702_9";
    public static final String modulo7_c7_p702_10 = "c7_p702_10";
    public static final String modulo7_c7_p702_o = "c7_p702_o";
    public static final String modulo7_c7_p703 = "c7_p703";
    public static final String modulo7_c7_p704_1 = "c7_p704_1";
    public static final String modulo7_c7_p704_2 = "c7_p704_2";
    public static final String modulo7_c7_p704_3 = "c7_p704_3";
    public static final String modulo7_c7_p704_4 = "c7_p704_4";
    public static final String modulo7_c7_p704_5 = "c7_p704_5";
    public static final String modulo7_c7_p704_6 = "c7_p704_6";
    public static final String modulo7_c7_p704_o = "c7_p704_o";
    public static final String modulo7_c7_p705_1 = "c7_p705_1";
    public static final String modulo7_c7_p705_2 = "c7_p705_2";
    public static final String modulo7_c7_p705_3 = "c7_p705_3";
    public static final String modulo7_c7_p705_4 = "c7_p705_4";
    public static final String modulo7_c7_p705_5 = "c7_p705_5";
    public static final String modulo7_c7_p705_6 = "c7_p705_6";
    public static final String modulo7_c7_p705_7 = "c7_p705_7";
    public static final String modulo7_c7_p705_o = "c7_p705_o";
    public static final String modulo7_c7_p706 = "c7_p706";
    public static final String modulo7_c7_p707_1 = "c7_p707_1";
    public static final String modulo7_c7_p707_2 = "c7_p707_2";
    public static final String modulo7_c7_p707_3 = "c7_p707_3";
    public static final String modulo7_c7_p707_4 = "c7_p707_4";
    public static final String modulo7_c7_p707_5 = "c7_p707_5";
    public static final String modulo7_c7_p707_6 = "c7_p707_6";
    public static final String modulo7_c7_p707_7 = "c7_p707_7";
    public static final String modulo7_c7_p707_8 = "c7_p707_8";
    public static final String modulo7_c7_p707_9 = "c7_p707_9";
    public static final String modulo7_c7_p707_o = "c7_p707_o";
    public static final String modulo7_c7_p708_1 = "c7_p708_1";
    public static final String modulo7_c7_p708_2 = "c7_p708_2";
    public static final String modulo7_c7_p708_3 = "c7_p708_3";
    public static final String modulo7_c7_p708_4 = "c7_p708_4";
    public static final String modulo7_c7_p708_5 = "c7_p708_5";
    public static final String modulo7_c7_p709_1 = "c7_p709_1";
    public static final String modulo7_c7_p709_2 = "c7_p709_2";
    public static final String modulo7_c7_p709_3 = "c7_p709_3";
    public static final String modulo7_c7_p709_4 = "c7_p709_4";
    public static final String modulo7_c7_p709_5 = "c7_p709_5";
    public static final String modulo7_c7_p709_6 = "c7_p709_6";
    public static final String modulo7_c7_p709_7 = "c7_p709_7";
    public static final String modulo7_c7_p709_8 = "c7_p709_8";
    public static final String modulo7_c7_p709_9 = "c7_p709_9";
    public static final String modulo7_c7_p709_10 = "c7_p709_10";
    public static final String modulo7_c7_p709_o = "c7_p709_o";
    public static final String modulo7_obs_cap7 = "obs_cap7";
     */
    public static final String modulo7_COB700 = "COB700";
   // val seccion_g_id = "ID"
   // val seccion_g_id_vivienda = "ID_VIVIENDA"
   // val seccion_g_numero_hogar = "NUMERO_HOGAR"
   // val seccion_g_numero_residente = "NUMERO_RESIDENTE"
   public static final String seccion_g_inf_300 = "INF_300";
    public static final String seccion_g_p338_1 = "P338_1";
    public static final String seccion_g_p339_1 = "P339_1";
    public static final String seccion_g_p338_2 = "P338_2";
    public static final String seccion_g_p339_2 = "P339_2";
    public static final String seccion_g_p338_3 = "P338_3";
    public static final String seccion_g_p339_3 = "P339_3";
    public static final String seccion_g_p338_4 = "P338_4";
    public static final String seccion_g_p339_4 = "P339_4";
    public static final String seccion_g_p338_5 = "P338_5";
    public static final String seccion_g_p339_5 = "P339_5";
    public static final String seccion_g_p338_6 = "P338_6";
    public static final String seccion_g_p339_6 = "P339_6";
    public static final String seccion_g_p338_7 = "P338_7";
    public static final String seccion_g_p339_7 = "P339_7";
    public static final String seccion_g_p338_8 = "P338_8";
    public static final String seccion_g_p339_8 = "P339_8";
    public static final String seccion_g_p338_9 = "P338_9";
    public static final String seccion_g_p339_9 = "P339_9";
    public static final String seccion_g_p338_10 = "P338_10";
    public static final String seccion_g_p339_10 = "P339_10";
    public static final String seccion_g_p340_1 = "P340_1";
    public static final String seccion_g_p340_2 = "P340_2";
    public static final String seccion_g_p340_3 = "P340_3";
    public static final String seccion_g_p340_4 = "P340_4";
    public static final String seccion_g_p340_5 = "P340_5";
    public static final String seccion_g_p340_6 = "P340_6";
    public static final String seccion_g_p340_7 = "P340_7";
    public static final String seccion_g_p340_8 = "P340_8";
    public static final String seccion_g_p340_9 = "P340_9";
    public static final String seccion_g_p340_10 = "P340_10";
    public static final String seccion_g_p340_11 = "P340_11";
    public static final String seccion_g_p340_12 = "P340_12";
    public static final String seccion_g_p340_13 = "P340_13";
    public static final String seccion_g_p340_13_o = "P340_13_O";
    public static final String seccion_g_obs_g = "OBS_G";


    public static final String SQL_CREATE_TABLA_MODULO7 =
            "CREATE TABLE " + tablamodulo7 + "(" +
                    modulo7_id  + " TEXT PRIMARY KEY," +
                    modulo7_id_informante + " TEXT," +
                    modulo7_id_hogar + " TEXT," +
                    modulo7_id_vivienda + " TEXT," +
                    /*
                    modulo7_c7_p701  +  " TEXT," +
                    modulo7_c7_p702_1  +  " TEXT," +
                    modulo7_c7_p702_2  +  " TEXT," +
                    modulo7_c7_p702_3  +  " TEXT," +
                    modulo7_c7_p702_4  +  " TEXT," +
                    modulo7_c7_p702_5  +  " TEXT," +
                    modulo7_c7_p702_6  +  " TEXT," +
                    modulo7_c7_p702_7  +  " TEXT," +
                    modulo7_c7_p702_8  +  " TEXT," +
                    modulo7_c7_p702_9  +  " TEXT," +
                    modulo7_c7_p702_10  +  " TEXT," +
                    modulo7_c7_p702_o  +  " TEXT," +
                    modulo7_c7_p703  +  " TEXT," +
                    modulo7_c7_p704_1  +  " TEXT," +
                    modulo7_c7_p704_2  +  " TEXT," +
                    modulo7_c7_p704_3  +  " TEXT," +
                    modulo7_c7_p704_4  +  " TEXT," +
                    modulo7_c7_p704_5  +  " TEXT," +
                    modulo7_c7_p704_6  +  " TEXT," +
                    modulo7_c7_p704_o  +  " TEXT," +
                    modulo7_c7_p705_1  +  " TEXT," +
                    modulo7_c7_p705_2  +  " TEXT," +
                    modulo7_c7_p705_3  +  " TEXT," +
                    modulo7_c7_p705_4  +  " TEXT," +
                    modulo7_c7_p705_5  +  " TEXT," +
                    modulo7_c7_p705_6  +  " TEXT," +
                    modulo7_c7_p705_7  +  " TEXT," +
                    modulo7_c7_p705_o  +  " TEXT," +
                    modulo7_c7_p706  +  " TEXT," +
                    modulo7_c7_p707_1  +  " TEXT," +
                    modulo7_c7_p707_2  +  " TEXT," +
                    modulo7_c7_p707_3  +  " TEXT," +
                    modulo7_c7_p707_4  +  " TEXT," +
                    modulo7_c7_p707_5  +  " TEXT," +
                    modulo7_c7_p707_6  +  " TEXT," +
                    modulo7_c7_p707_7  +  " TEXT," +
                    modulo7_c7_p707_8  +  " TEXT," +
                    modulo7_c7_p707_9  +  " TEXT," +
                    modulo7_c7_p707_o  +  " TEXT," +
                    modulo7_c7_p708_1  +  " TEXT," +
                    modulo7_c7_p708_2  +  " TEXT," +
                    modulo7_c7_p708_3  +  " TEXT," +
                    modulo7_c7_p708_4  +  " TEXT," +
                    modulo7_c7_p708_5  +  " TEXT," +
                    modulo7_c7_p709_1  +  " TEXT," +
                    modulo7_c7_p709_2  +  " TEXT," +
                    modulo7_c7_p709_3  +  " TEXT," +
                    modulo7_c7_p709_4  +  " TEXT," +
                    modulo7_c7_p709_5  +  " TEXT," +
                    modulo7_c7_p709_6  +  " TEXT," +
                    modulo7_c7_p709_7  +  " TEXT," +
                    modulo7_c7_p709_8  +  " TEXT," +
                    modulo7_c7_p709_9  +  " TEXT," +
                    modulo7_c7_p709_10  +  " TEXT," +
                    modulo7_c7_p709_o  +  " TEXT," +
                    modulo7_obs_cap7  +  " TEXT," +
                     */
                    seccion_g_inf_300 + " TEXT," +
                    seccion_g_p338_1 + " TEXT," +
                    seccion_g_p339_1 + " TEXT," +
                    seccion_g_p338_2 + " TEXT," +
                    seccion_g_p339_2 + " TEXT," +
                    seccion_g_p338_3 + " TEXT," +
                    seccion_g_p339_3 + " TEXT," +
                    seccion_g_p338_4 + " TEXT," +
                    seccion_g_p339_4 + " TEXT," +
                    seccion_g_p338_5 + " TEXT," +
                    seccion_g_p339_5 + " TEXT," +
                    seccion_g_p338_6 + " TEXT," +
                    seccion_g_p339_6 + " TEXT," +
                    seccion_g_p338_7 + " TEXT," +
                    seccion_g_p339_7 + " TEXT," +
                    seccion_g_p338_8 + " TEXT," +
                    seccion_g_p339_8 + " TEXT," +
                    seccion_g_p338_9 + " TEXT," +
                    seccion_g_p339_9 + " TEXT," +
                    seccion_g_p338_10 + " TEXT," +
                    seccion_g_p339_10 + " TEXT," +
                    seccion_g_p340_1 + " TEXT," +
                    seccion_g_p340_2 + " TEXT," +
                    seccion_g_p340_3 + " TEXT," +
                    seccion_g_p340_4 + " TEXT," +
                    seccion_g_p340_5 + " TEXT," +
                    seccion_g_p340_6 + " TEXT," +
                    seccion_g_p340_7 + " TEXT," +
                    seccion_g_p340_8 + " TEXT," +
                    seccion_g_p340_9 + " TEXT," +
                    seccion_g_p340_10 + " TEXT," +
                    seccion_g_p340_11 + " TEXT," +
                    seccion_g_p340_12 + " TEXT," +
                    seccion_g_p340_13 + " TEXT," +
                    seccion_g_p340_13_o + " TEXT," +
                    seccion_g_obs_g + " TEXT," +
                    modulo7_COB700  +  " TEXT" + ");"
            ;

    /**
     * TABLA MODULO 8
     * */
    public static final String modulo8_id = "_id";
    public static final String modulo8_id_informante = "id_informante";
    public static final String modulo8_id_hogar = "id_hogar";
    public static final String modulo8_id_vivienda = "id_vivienda";
    /*
    public static final String modulo8_c8_p801 = "c8_p801";
    public static final String modulo8_c8_p802 = "c8_p802";
    public static final String modulo8_c8_p803 = "c8_p803";
    public static final String modulo8_c8_p804 = "c8_p804";
    public static final String modulo8_c8_p805_1 = "c8_p805_1";
    public static final String modulo8_c8_p805_2 = "c8_p805_2";
    public static final String modulo8_c8_p805_3 = "c8_p805_3";
    public static final String modulo8_c8_p805_4 = "c8_p805_4";
    public static final String modulo8_c8_p805_5 = "c8_p805_5";
    public static final String modulo8_c8_p806_1 = "c8_p806_1";
    public static final String modulo8_c8_p806_2 = "c8_p806_2";
    public static final String modulo8_c8_p806_3 = "c8_p806_3";
    public static final String modulo8_c8_p806_4 = "c8_p806_4";
    public static final String modulo8_c8_p806_5 = "c8_p806_5";
    public static final String modulo8_c8_p806_6 = "c8_p806_6";
    public static final String modulo8_c8_p807 = "c8_p807";
    public static final String modulo8_c8_p808_1 = "c8_p808_1";
    public static final String modulo8_c8_p808_2 = "c8_p808_2";
    public static final String modulo8_c8_p808_3 = "c8_p808_3";
    public static final String modulo8_c8_p808_4 = "c8_p808_4";
    public static final String modulo8_c8_p808_5 = "c8_p808_5";
    public static final String modulo8_c8_p808_6 = "c8_p808_6";
    public static final String modulo8_c8_p808_7 = "c8_p808_7";
    public static final String modulo8_c8_p808_8 = "c8_p808_8";
    public static final String modulo8_c8_p808_9 = "c8_p808_9";
    public static final String modulo8_c8_p808_10 = "c8_p808_10";
    public static final String modulo8_c8_p808_11 = "c8_p808_11";
    public static final String modulo8_c8_p808_12 = "c8_p808_12";
    public static final String modulo8_c8_p808_13 = "c8_p808_13";
    public static final String modulo8_c8_p808_o = "c8_p808_o";
    public static final String modulo8_c8_p809 = "c8_p809";
    public static final String modulo8_c8_p810_1 = "c8_p810_1";
    public static final String modulo8_c8_p810_2 = "c8_p810_2";
    public static final String modulo8_c8_p810_3 = "c8_p810_3";
    public static final String modulo8_c8_p810_4 = "c8_p810_4";
    public static final String modulo8_c8_p810_5 = "c8_p810_5";
    public static final String modulo8_c8_p810_6 = "c8_p810_6";
    public static final String modulo8_c8_p810_7 = "c8_p810_7";
    public static final String modulo8_c8_p810_8 = "c8_p810_8";
    public static final String modulo8_c8_p810_9 = "c8_p810_9";
    public static final String modulo8_c8_p810_10 = "c8_p810_10";
    public static final String modulo8_c8_p810_11 = "c8_p810_11";
    public static final String modulo8_c8_p810_12 = "c8_p810_12";
    public static final String modulo8_c8_p810_13 = "c8_p810_13";
    public static final String modulo8_c8_p810_o = "c8_p810_o";
    public static final String modulo8_c8_p811 = "c8_p811";
    public static final String modulo8_c8_p812 = "c8_p812";
    public static final String modulo8_c8_p813_1 = "c8_p813_1";
    public static final String modulo8_c8_p813_2 = "c8_p813_2";
    public static final String modulo8_c8_p813_3 = "c8_p813_3";
    public static final String modulo8_c8_p813_4 = "c8_p813_4";
    public static final String modulo8_c8_p813_5 = "c8_p813_5";
    public static final String modulo8_c8_p813_6 = "c8_p813_6";
    public static final String modulo8_c8_p813_7 = "c8_p813_7";
    public static final String modulo8_c8_p813_8 = "c8_p813_8";
    public static final String modulo8_c8_p813_9 = "c8_p813_9";
    public static final String modulo8_c8_p813_10 = "c8_p813_10";
    public static final String modulo8_c8_p813_11 = "c8_p813_11";
    public static final String modulo8_c8_p813_12 = "c8_p813_12";
    public static final String modulo8_c8_p813_13 = "c8_p813_13";
    public static final String modulo8_c8_p813_14 = "c8_p813_14";
    public static final String modulo8_c8_p813_o = "c8_p813_o";
    public static final String modulo8_c8_p814_1 = "c8_p814_1";
    public static final String modulo8_c8_p814_2 = "c8_p814_2";
    public static final String modulo8_c8_p814_3 = "c8_p814_3";
    public static final String modulo8_c8_p814_4 = "c8_p814_4";
    public static final String modulo8_c8_p814_5 = "c8_p814_5";
    public static final String modulo8_c8_p814_6 = "c8_p814_6";
    public static final String modulo8_c8_p814_7 = "c8_p814_7";
    public static final String modulo8_c8_p814_8 = "c8_p814_8";
    public static final String modulo8_c8_p815 = "c8_p815";
    public static final String modulo8_c8_p816_1 = "c8_p816_1";
    public static final String modulo8_c8_p816_2 = "c8_p816_2";
    public static final String modulo8_c8_p816_3 = "c8_p816_3";
    public static final String modulo8_c8_p816_4 = "c8_p816_4";
    public static final String modulo8_c8_p816_5 = "c8_p816_5";
    public static final String modulo8_c8_p816_6 = "c8_p816_6";
    public static final String modulo8_c8_p816_7 = "c8_p816_7";
    public static final String modulo8_c8_p816_8 = "c8_p816_8";
    public static final String modulo8_c8_p816_9 = "c8_p816_9";
    public static final String modulo8_c8_p816_10 = "c8_p816_10";
    public static final String modulo8_c8_p816_11 = "c8_p816_11";
    public static final String modulo8_c8_p816_12 = "c8_p816_12";
    public static final String modulo8_c8_p816_13 = "c8_p816_13";
    public static final String modulo8_c8_p816_o = "c8_p816_o";
    public static final String modulo8_c8_p817 = "c8_p817";
    public static final String modulo8_c8_p818 = "c8_p818";
    public static final String modulo8_c8_p819_1 = "c8_p819_1";
    public static final String modulo8_c8_p819_2 = "c8_p819_2";
    public static final String modulo8_c8_p819_3 = "c8_p819_3";
    public static final String modulo8_c8_p819_4 = "c8_p819_4";
    public static final String modulo8_c8_p819_5 = "c8_p819_5";
    public static final String modulo8_c8_p819_6 = "c8_p819_6";
    public static final String modulo8_c8_p819_7 = "c8_p819_7";
    public static final String modulo8_c8_p819_8 = "c8_p819_8";
    public static final String modulo8_c8_p819_9 = "c8_p819_9";
    public static final String modulo8_c8_p819_10 = "c8_p819_10";
    public static final String modulo8_c8_p819_11 = "c8_p819_11";
    public static final String modulo8_c8_p819_12 = "c8_p819_12";
    public static final String modulo8_c8_p819_13 = "c8_p819_13";
    public static final String modulo8_c8_p819_14 = "c8_p819_14";
    public static final String modulo8_c8_p819_o = "c8_p819_o";
    public static final String modulo8_c8_p820_1 = "c8_p820_1";
    public static final String modulo8_c8_p820_2 = "c8_p820_2";
    public static final String modulo8_c8_p820_3 = "c8_p820_3";
    public static final String modulo8_c8_p820_4 = "c8_p820_4";
    public static final String modulo8_c8_p820_5 = "c8_p820_5";
    public static final String modulo8_c8_p820_6 = "c8_p820_6";
    public static final String modulo8_c8_p820_7 = "c8_p820_7";
    public static final String modulo8_c8_p820_8 = "c8_p820_8";
    public static final String modulo8_c8_p820_9 = "c8_p820_9";
    public static final String modulo8_c8_p820_10 = "c8_p820_10";
    public static final String modulo8_c8_p820_11 = "c8_p820_11";
    public static final String modulo8_c8_p820_o = "c8_p820_o";
    public static final String modulo8_c8_p821_1 = "c8_p821_1";
    public static final String modulo8_c8_p821_2 = "c8_p821_2";
    public static final String modulo8_c8_p821_3 = "c8_p821_3";
    public static final String modulo8_c8_p821_4 = "c8_p821_4";
    public static final String modulo8_c8_p821_5 = "c8_p821_5";
    public static final String modulo8_c8_p821_6 = "c8_p821_6";
    public static final String modulo8_c8_p821_7 = "c8_p821_7";
    public static final String modulo8_c8_p821_8 = "c8_p821_8";
    public static final String modulo8_c8_p822 = "c8_p822";
    public static final String modulo8_c8_p823_1 = "c8_p823_1";
    public static final String modulo8_c8_p823_2 = "c8_p823_2";
    public static final String modulo8_c8_p823_3 = "c8_p823_3";
    public static final String modulo8_c8_p823_4 = "c8_p823_4";
    public static final String modulo8_c8_p823_5 = "c8_p823_5";
    public static final String modulo8_c8_p823_o = "c8_p823_o";
    public static final String modulo8_obs_cap8 = "obs_cap8";
    public static final String modulo8_email = "email";

     */
    public static final String modulo8_COB800 = "COB800";


    public static final String SQL_CREATE_TABLA_MODULO8 =
            "CREATE TABLE " + tablamodulo8 + "(" +
                    modulo8_id  + " TEXT PRIMARY KEY," +
                    modulo8_id_informante + " TEXT," +
                    modulo8_id_hogar + " TEXT," +
                    modulo8_id_vivienda + " TEXT," +
                    /*
                    modulo8_c8_p801  +  " TEXT," +
                    modulo8_c8_p802  +  " TEXT," +
                    modulo8_c8_p803  +  " TEXT," +
                    modulo8_c8_p804  +  " TEXT," +
                    modulo8_c8_p805_1  +  " TEXT," +
                    modulo8_c8_p805_2  +  " TEXT," +
                    modulo8_c8_p805_3  +  " TEXT," +
                    modulo8_c8_p805_4  +  " TEXT," +
                    modulo8_c8_p805_5  +  " TEXT," +
                    modulo8_c8_p806_1  +  " TEXT," +
                    modulo8_c8_p806_2  +  " TEXT," +
                    modulo8_c8_p806_3  +  " TEXT," +
                    modulo8_c8_p806_4  +  " TEXT," +
                    modulo8_c8_p806_5  +  " TEXT," +
                    modulo8_c8_p806_6  +  " TEXT," +
                    modulo8_c8_p807  +  " TEXT," +
                    modulo8_c8_p808_1  +  " TEXT," +
                    modulo8_c8_p808_2  +  " TEXT," +
                    modulo8_c8_p808_3  +  " TEXT," +
                    modulo8_c8_p808_4  +  " TEXT," +
                    modulo8_c8_p808_5  +  " TEXT," +
                    modulo8_c8_p808_6  +  " TEXT," +
                    modulo8_c8_p808_7  +  " TEXT," +
                    modulo8_c8_p808_8  +  " TEXT," +
                    modulo8_c8_p808_9  +  " TEXT," +
                    modulo8_c8_p808_10  +  " TEXT," +
                    modulo8_c8_p808_11  +  " TEXT," +
                    modulo8_c8_p808_12  +  " TEXT," +
                    modulo8_c8_p808_13  +  " TEXT," +
                    modulo8_c8_p808_o  +  " TEXT," +
                    modulo8_c8_p809  +  " TEXT," +
                    modulo8_c8_p810_1  +  " TEXT," +
                    modulo8_c8_p810_2  +  " TEXT," +
                    modulo8_c8_p810_3  +  " TEXT," +
                    modulo8_c8_p810_4  +  " TEXT," +
                    modulo8_c8_p810_5  +  " TEXT," +
                    modulo8_c8_p810_6  +  " TEXT," +
                    modulo8_c8_p810_7  +  " TEXT," +
                    modulo8_c8_p810_8  +  " TEXT," +
                    modulo8_c8_p810_9  +  " TEXT," +
                    modulo8_c8_p810_10  +  " TEXT," +
                    modulo8_c8_p810_11  +  " TEXT," +
                    modulo8_c8_p810_12  +  " TEXT," +
                    modulo8_c8_p810_13  +  " TEXT," +
                    modulo8_c8_p810_o  +  " TEXT," +
                    modulo8_c8_p811  +  " TEXT," +
                    modulo8_c8_p812  +  " TEXT," +
                    modulo8_c8_p813_1  +  " TEXT," +
                    modulo8_c8_p813_2  +  " TEXT," +
                    modulo8_c8_p813_3  +  " TEXT," +
                    modulo8_c8_p813_4  +  " TEXT," +
                    modulo8_c8_p813_5  +  " TEXT," +
                    modulo8_c8_p813_6  +  " TEXT," +
                    modulo8_c8_p813_7  +  " TEXT," +
                    modulo8_c8_p813_8  +  " TEXT," +
                    modulo8_c8_p813_9  +  " TEXT," +
                    modulo8_c8_p813_10  +  " TEXT," +
                    modulo8_c8_p813_11  +  " TEXT," +
                    modulo8_c8_p813_12  +  " TEXT," +
                    modulo8_c8_p813_13  +  " TEXT," +
                    modulo8_c8_p813_14  +  " TEXT," +
                    modulo8_c8_p813_o  +  " TEXT," +
                    modulo8_c8_p814_1  +  " TEXT," +
                    modulo8_c8_p814_2  +  " TEXT," +
                    modulo8_c8_p814_3  +  " TEXT," +
                    modulo8_c8_p814_4  +  " TEXT," +
                    modulo8_c8_p814_5  +  " TEXT," +
                    modulo8_c8_p814_6  +  " TEXT," +
                    modulo8_c8_p814_7  +  " TEXT," +
                    modulo8_c8_p814_8  +  " TEXT," +
                    modulo8_c8_p815  +  " TEXT," +
                    modulo8_c8_p816_1  +  " TEXT," +
                    modulo8_c8_p816_2  +  " TEXT," +
                    modulo8_c8_p816_3  +  " TEXT," +
                    modulo8_c8_p816_4  +  " TEXT," +
                    modulo8_c8_p816_5  +  " TEXT," +
                    modulo8_c8_p816_6  +  " TEXT," +
                    modulo8_c8_p816_7  +  " TEXT," +
                    modulo8_c8_p816_8  +  " TEXT," +
                    modulo8_c8_p816_9  +  " TEXT," +
                    modulo8_c8_p816_10  +  " TEXT," +
                    modulo8_c8_p816_11  +  " TEXT," +
                    modulo8_c8_p816_12  +  " TEXT," +
                    modulo8_c8_p816_13  +  " TEXT," +
                    modulo8_c8_p816_o  +  " TEXT," +
                    modulo8_c8_p817  +  " TEXT," +
                    modulo8_c8_p818  +  " TEXT," +
                    modulo8_c8_p819_1  +  " TEXT," +
                    modulo8_c8_p819_2  +  " TEXT," +
                    modulo8_c8_p819_3  +  " TEXT," +
                    modulo8_c8_p819_4  +  " TEXT," +
                    modulo8_c8_p819_5  +  " TEXT," +
                    modulo8_c8_p819_6  +  " TEXT," +
                    modulo8_c8_p819_7  +  " TEXT," +
                    modulo8_c8_p819_8  +  " TEXT," +
                    modulo8_c8_p819_9  +  " TEXT," +
                    modulo8_c8_p819_10  +  " TEXT," +
                    modulo8_c8_p819_11  +  " TEXT," +
                    modulo8_c8_p819_12  +  " TEXT," +
                    modulo8_c8_p819_13  +  " TEXT," +
                    modulo8_c8_p819_14  +  " TEXT," +
                    modulo8_c8_p819_o  +  " TEXT," +
                    modulo8_c8_p820_1  +  " TEXT," +
                    modulo8_c8_p820_2  +  " TEXT," +
                    modulo8_c8_p820_3  +  " TEXT," +
                    modulo8_c8_p820_4  +  " TEXT," +
                    modulo8_c8_p820_5  +  " TEXT," +
                    modulo8_c8_p820_6  +  " TEXT," +
                    modulo8_c8_p820_7  +  " TEXT," +
                    modulo8_c8_p820_8  +  " TEXT," +
                    modulo8_c8_p820_9  +  " TEXT," +
                    modulo8_c8_p820_10  +  " TEXT," +
                    modulo8_c8_p820_11  +  " TEXT," +
                    modulo8_c8_p820_o  +  " TEXT," +
                    modulo8_c8_p821_1  +  " TEXT," +
                    modulo8_c8_p821_2  +  " TEXT," +
                    modulo8_c8_p821_3  +  " TEXT," +
                    modulo8_c8_p821_4  +  " TEXT," +
                    modulo8_c8_p821_5  +  " TEXT," +
                    modulo8_c8_p821_6  +  " TEXT," +
                    modulo8_c8_p821_7  +  " TEXT," +
                    modulo8_c8_p821_8  +  " TEXT," +
                    modulo8_c8_p822  +  " TEXT," +
                    modulo8_c8_p823_1  +  " TEXT," +
                    modulo8_c8_p823_2  +  " TEXT," +
                    modulo8_c8_p823_3  +  " TEXT," +
                    modulo8_c8_p823_4  +  " TEXT," +
                    modulo8_c8_p823_5  +  " TEXT," +
                    modulo8_c8_p823_o  +  " TEXT," +
                    modulo8_obs_cap8  +  " TEXT," +
                    modulo8_email  +  " TEXT," +
                     */
                    seccion_h_inf_300 + " TEXT," +
                    seccion_h_p341_1 + " TEXT," +
                    seccion_h_p341_2 + " TEXT," +
                    seccion_h_p341_3 + " TEXT," +
                    seccion_h_p341_4 + " TEXT," +
                    seccion_h_p342 + " TEXT," +
                    seccion_h_p342_o + " TEXT," +
                    seccion_h_p342a + " TEXT," +
                    seccion_h_p342b + " TEXT," +
                    seccion_h_p342b_o + " TEXT," +
                    seccion_h_p343 + " TEXT," +
                    seccion_h_id_p344 + " TEXT," +
                    seccion_h_id_p344_item + " TEXT," +
                    seccion_h_id_p344_item_o + " TEXT," +
                    seccion_h_p344_1 + " TEXT," +
                    seccion_h_p345_1_1 + " TEXT," +
                    seccion_h_p345_1_2 + " TEXT," +
                    seccion_h_p345_1_3 + " TEXT," +
                    seccion_h_p345_1_4 + " TEXT," +
                    seccion_h_p345_1_5 + " TEXT," +
                    seccion_h_p345_1_6 + " TEXT," +
                    seccion_h_p345_1_7 + " TEXT," +
                    seccion_h_p345_1_8 + " TEXT," +
                    seccion_h_p345_1_8_o + " TEXT," +
                    seccion_h_p345_1_9 + " TEXT," +
                    seccion_h_p344_2 + " TEXT," +
                    seccion_h_p345_2_1 + " TEXT," +
                    seccion_h_p345_2_2 + " TEXT," +
                    seccion_h_p345_2_3 + " TEXT," +
                    seccion_h_p345_2_4 + " TEXT," +
                    seccion_h_p345_2_5 + " TEXT," +
                    seccion_h_p345_2_6 + " TEXT," +
                    seccion_h_p345_2_7 + " TEXT," +
                    seccion_h_p345_2_8 + " TEXT," +
                    seccion_h_p345_2_8_o + " TEXT," +
                    seccion_h_p345_2_9 + " TEXT," +
                    seccion_h_p344_3 + " TEXT," +
                    seccion_h_p345_3_1 + " TEXT," +
                    seccion_h_p345_3_2 + " TEXT," +
                    seccion_h_p345_3_3 + " TEXT," +
                    seccion_h_p345_3_4 + " TEXT," +
                    seccion_h_p345_3_5 + " TEXT," +
                    seccion_h_p345_3_6 + " TEXT," +
                    seccion_h_p345_3_7 + " TEXT," +
                    seccion_h_p345_3_8 + " TEXT," +
                    seccion_h_p345_3_8_o + " TEXT," +
                    seccion_h_p345_3_9 + " TEXT," +
                    seccion_h_p344_4 + " TEXT," +
                    seccion_h_p344_4_o + " TEXT," +
                    seccion_h_p345_4_1 + " TEXT," +
                    seccion_h_p345_4_2 + " TEXT," +
                    seccion_h_p345_4_3 + " TEXT," +
                    seccion_h_p345_4_4 + " TEXT," +
                    seccion_h_p345_4_5 + " TEXT," +
                    seccion_h_p345_4_6 + " TEXT," +
                    seccion_h_p345_4_7 + " TEXT," +
                    seccion_h_p345_4_8 + " TEXT," +
                    seccion_h_p345_4_8_o + " TEXT," +
                    seccion_h_p345_4_9 + " TEXT," +
                    seccion_h_p346_1 + " TEXT," +
                    seccion_h_p346_2 + " TEXT," +
                    seccion_h_p346_3 + " TEXT," +
                    seccion_h_p346_4 + " TEXT," +
                    seccion_h_p346_5 + " TEXT," +
                    seccion_h_p346_6 + " TEXT," +
                    seccion_h_p346_6_o + " TEXT," +
                    seccion_h_obs_h  +  " TEXT," +
                    modulo8_COB800  +  " TEXT" + ");";

    public static final String SQL_CREATE_TABLA_MODULO9 =
            "CREATE TABLE " + tablamodulo9 + "(" +
                    seccion_i_id + " TEXT PRIMARY KEY," +
                    seccion_i_inf_300 + " TEXT," +
                    seccion_i_informante + " TEXT," +
                    seccion_i_id_vivienda + " TEXT," +
                    seccion_i_numero_hogar + " TEXT," +
                    seccion_i_p347_1 + " TEXT," +
                    seccion_i_p347_2 + " TEXT," +
                    seccion_i_p347_3 + " TEXT," +
                    seccion_i_p347_4 + " TEXT," +
                    seccion_i_p347_5 + " TEXT," +
                    seccion_i_p348_1 + " TEXT," +
                    seccion_i_p348_2 + " TEXT," +
                    seccion_i_p348_3 + " TEXT," +
                    seccion_i_p348_4 + " TEXT," +
                    seccion_i_p348_5 + " TEXT," +
                    seccion_i_p348_6 + " TEXT," +
                    seccion_i_p348_7 + " TEXT," +
                    seccion_i_p348_8 + " TEXT," +
                    seccion_i_p348_9 + " TEXT," +
                    seccion_i_p348_10 + " TEXT," +
                    seccion_i_p348_11 + " TEXT," +
                    seccion_i_p348_11_o + " TEXT," +
                    seccion_i_p348_12 + " TEXT," +
                    seccion_i_p348_13 + " TEXT," +
                    seccion_i_p348_14 + " TEXT," +
                    seccion_i_p348_i + " TEXT," +
                    seccion_i_p349 + " TEXT," +
                    seccion_i_p350_1 + " TEXT," +
                    seccion_i_p350_2 + " TEXT," +
                    seccion_i_p350_3 + " TEXT," +
                    seccion_i_p350_4 + " TEXT," +
                    seccion_i_p350_5 + " TEXT," +
                    seccion_i_p350_5_o + " TEXT," +
                    seccion_i_p351 + " TEXT," +
                    seccion_i_p351a_1 + " TEXT," +
                    seccion_i_p351a_2 + " TEXT," +
                    seccion_i_p351a_3 + " TEXT," +
                    seccion_i_p351a_4 + " TEXT," +
                    seccion_i_p351a_5 + " TEXT," +
                    seccion_i_p351a_5_o + " TEXT," +
                    seccion_i_p352 + " TEXT," +
                    seccion_i_p352_o + " TEXT," +
                    seccion_i_p353_1 + " TEXT," +
                    seccion_i_p353_2 + " TEXT," +
                    seccion_i_p353_3 + " TEXT," +
                    seccion_i_p353_4 + " TEXT," +
                    seccion_i_p353_5 + " TEXT," +
                    seccion_i_p353_6 + " TEXT," +
                    seccion_i_p353_6_o + " TEXT," +
                    seccion_i_p354_1 + " TEXT," +
                    seccion_i_p355_1 + " TEXT," +
                    seccion_i_p355_1_o + " TEXT," +
                    seccion_i_p354_2 + " TEXT," +
                    seccion_i_p355_2 + " TEXT," +
                    seccion_i_p355_2_o + " TEXT," +
                    seccion_i_p354_3 + " TEXT," +
                    seccion_i_p355_3 + " TEXT," +
                    seccion_i_p355_3_o + " TEXT," +
                    seccion_i_p354_4 + " TEXT," +
                    seccion_i_p355_4 + " TEXT," +
                    seccion_i_p355_4_o + " TEXT," +
                    seccion_i_p354_5 + " TEXT," +
                    seccion_i_p355_5 + " TEXT," +
                    seccion_i_p355_5_o + " TEXT," +
                    seccion_i_p354_6 + " TEXT," +
                    seccion_i_p354_6_o + " TEXT," +
                    seccion_i_p355_6 + " TEXT," +
                    seccion_i_p355_6_o + " TEXT," +
                    seccion_i_p356 + " TEXT," +
                    seccion_i_p357_1 + " TEXT," +
                    seccion_i_p357_2 + " TEXT," +
                    seccion_i_p357_3 + " TEXT," +
                    seccion_i_p357_4 + " TEXT," +
                    seccion_i_p357_5 + " TEXT," +
                    seccion_i_p357_6 + " TEXT," +
                    seccion_i_p357_7 + " TEXT," +
                    seccion_i_p357_8 + " TEXT," +
                    seccion_i_p357_8_o + " TEXT," +
                    seccion_i_p357_9 + " TEXT," +
                    seccion_i_p358 + " TEXT," +
                    seccion_i_p359 + " TEXT," +
                    seccion_i_obs_i  +  " TEXT" + ");";


    /**
     * TABLA LAYOUTS
     * */
    public static final String layouts_id = "_id";
    public static final String layouts_id_vivienda = "id_vivienda";
    public static final String layouts_p301 = "p301";
    public static final String layouts_p302 = "p302";
    public static final String layouts_p303 = "p303";
    public static final String layouts_p304 = "p304";
    public static final String layouts_p305 = "p305";
    public static final String layouts_p306 = "p306";
    public static final String layouts_p307 = "p307";
    public static final String layouts_p308 = "p308";
    public static final String layouts_p309 = "p309";
    public static final String layouts_p310 = "p310";
    public static final String layouts_p311 = "p311";
    public static final String layouts_p312 = "p312";
    public static final String layouts_p313 = "p313";
    public static final String layouts_p314 = "p314";
    public static final String layouts_p315 = "p315";
    public static final String layouts_p316 = "p316";
    public static final String layouts_p317 = "p317";
    public static final String layouts_p318 = "p318";
    public static final String layouts_p401 = "p401";
    public static final String layouts_p402 = "p402";
    public static final String layouts_p403 = "p403";
    public static final String layouts_p404 = "p404";
    public static final String layouts_p405 = "p405";
    public static final String layouts_p406 = "p406";
    public static final String layouts_p407 = "p407";
    public static final String layouts_p408 = "p408";
    public static final String layouts_p409 = "p409";
    public static final String layouts_p410 = "p410";
    public static final String layouts_p411 = "p411";
    public static final String layouts_p412 = "p412";
    public static final String layouts_p413 = "p413";
    public static final String layouts_p414 = "p414";
    public static final String layouts_p415 = "p415";
    public static final String layouts_p416 = "p416";
    public static final String layouts_p501 = "p501";
    public static final String layouts_p502 = "p502";
    public static final String layouts_p503 = "p503";
    public static final String layouts_p504 = "p504";
    public static final String layouts_p505 = "p505";
    public static final String layouts_p506 = "p506";
    public static final String layouts_p507 = "p507";
    public static final String layouts_p508 = "p508";
    public static final String layouts_p509 = "p509";
    public static final String layouts_p510 = "p510";
    public static final String layouts_p511 = "p511";
    public static final String layouts_p512 = "p512";
    public static final String layouts_p513 = "p513";
    public static final String layouts_p601 = "p601";
    public static final String layouts_p602 = "p602";
    public static final String layouts_p603 = "p603";
    public static final String layouts_p604 = "p604";
    public static final String layouts_p605 = "p605";
    public static final String layouts_p606 = "p606";
    public static final String layouts_p607 = "p607";
    public static final String layouts_p608 = "p608";
    public static final String layouts_p609 = "p609";
    public static final String layouts_p610 = "p610";
    public static final String layouts_p611 = "p611";
    public static final String layouts_p611a = "p611a";
    public static final String layouts_p611b = "p611b";
    public static final String layouts_p612 = "p612";
    public static final String layouts_p613 = "p613";
    public static final String layouts_p614 = "p614";
    public static final String layouts_p615 = "p615";
    public static final String layouts_p616 = "p616";
    public static final String layouts_p617 = "p617";
    public static final String layouts_p618 = "p618";
    public static final String layouts_p619 = "p619";
    public static final String layouts_p620 = "p620";
    public static final String layouts_p621 = "p621";
    public static final String layouts_p622 = "p622";
    public static final String layouts_p623 = "p623";
    public static final String layouts_p624 = "p624";
    public static final String layouts_p625 = "p625";
    public static final String layouts_p626 = "p626";
    public static final String layouts_p627 = "p627";
    public static final String layouts_p628 = "p628";
    public static final String layouts_p629 = "p629";
    public static final String layouts_p630 = "p630";
    public static final String layouts_p701 = "p701";
    public static final String layouts_p702 = "p702";
    public static final String layouts_p703 = "p703";
    public static final String layouts_p704 = "p704";
    public static final String layouts_p705 = "p705";
    public static final String layouts_p706 = "p706";
    public static final String layouts_p707 = "p707";
    public static final String layouts_p708 = "p708";
    public static final String layouts_p709 = "p709";
    public static final String layouts_p801 = "p801";
    public static final String layouts_p802 = "p802";
    public static final String layouts_p803 = "p803";
    public static final String layouts_p804 = "p804";
    public static final String layouts_p805 = "p805";
    public static final String layouts_p806 = "p806";
    public static final String layouts_p807 = "p807";
    public static final String layouts_p808 = "p808";
    public static final String layouts_p809 = "p809";
    public static final String layouts_p810 = "p810";
    public static final String layouts_p811 = "p811";
    public static final String layouts_p812 = "p812";
    public static final String layouts_p813 = "p813";
    public static final String layouts_p814 = "p814";
    public static final String layouts_p815 = "p815";
    public static final String layouts_p816 = "p816";
    public static final String layouts_p817 = "p817";
    public static final String layouts_p818 = "p818";
    public static final String layouts_p819 = "p819";
    public static final String layouts_p820 = "p820";
    public static final String layouts_p821 = "p821";
    public static final String layouts_p822 = "p822";
    public static final String layouts_p823 = "p823";

    public static final String SQL_CREATE_TABLA_LAYOUTS =
            "CREATE TABLE " + tablalayouts + "(" +
                    layouts_id  + " TEXT PRIMARY KEY," +
                    layouts_id_vivienda + " TEXT," +
                    layouts_p301 + " TEXT," +
                    layouts_p302 + " TEXT," +
                    layouts_p303 + " TEXT," +
                    layouts_p304 + " TEXT," +
                    layouts_p305 + " TEXT," +
                    layouts_p306 + " TEXT," +
                    layouts_p307 + " TEXT," +
                    layouts_p308 + " TEXT," +
                    layouts_p309 + " TEXT," +
                    layouts_p310 + " TEXT," +
                    layouts_p311 + " TEXT," +
                    layouts_p312 + " TEXT," +
                    layouts_p313 + " TEXT," +
                    layouts_p314 + " TEXT," +
                    layouts_p315 + " TEXT," +
                    layouts_p316 + " TEXT," +
                    layouts_p317 + " TEXT," +
                    layouts_p318 + " TEXT," +
                    layouts_p401 + " TEXT," +
                    layouts_p402 + " TEXT," +
                    layouts_p403 + " TEXT," +
                    layouts_p404 + " TEXT," +
                    layouts_p405 + " TEXT," +
                    layouts_p406 + " TEXT," +
                    layouts_p407 + " TEXT," +
                    layouts_p408 + " TEXT," +
                    layouts_p409 + " TEXT," +
                    layouts_p410 + " TEXT," +
                    layouts_p411 + " TEXT," +
                    layouts_p412 + " TEXT," +
                    layouts_p413 + " TEXT," +
                    layouts_p414 + " TEXT," +
                    layouts_p415 + " TEXT," +
                    layouts_p416 + " TEXT," +
                    layouts_p501 + " TEXT," +
                    layouts_p502 + " TEXT," +
                    layouts_p503 + " TEXT," +
                    layouts_p504 + " TEXT," +
                    layouts_p505 + " TEXT," +
                    layouts_p506 + " TEXT," +
                    layouts_p507 + " TEXT," +
                    layouts_p508 + " TEXT," +
                    layouts_p509 + " TEXT," +
                    layouts_p510 + " TEXT," +
                    layouts_p511 + " TEXT," +
                    layouts_p512 + " TEXT," +
                    layouts_p513 + " TEXT," +
                    layouts_p601 + " TEXT," +
                    layouts_p602 + " TEXT," +
                    layouts_p603 + " TEXT," +
                    layouts_p604 + " TEXT," +
                    layouts_p605 + " TEXT," +
                    layouts_p606 + " TEXT," +
                    layouts_p607 + " TEXT," +
                    layouts_p608 + " TEXT," +
                    layouts_p609 + " TEXT," +
                    layouts_p610 + " TEXT," +
                    layouts_p611 + " TEXT," +
                    layouts_p611a + " TEXT," +
                    layouts_p611b + " TEXT," +
                    layouts_p612 + " TEXT," +
                    layouts_p613 + " TEXT," +
                    layouts_p614 + " TEXT," +
                    layouts_p615 + " TEXT," +
                    layouts_p616 + " TEXT," +
                    layouts_p617 + " TEXT," +
                    layouts_p618 + " TEXT," +
                    layouts_p619 + " TEXT," +
                    layouts_p620 + " TEXT," +
                    layouts_p621 + " TEXT," +
                    layouts_p622 + " TEXT," +
                    layouts_p623 + " TEXT," +
                    layouts_p624 + " TEXT," +
                    layouts_p625 + " TEXT," +
                    layouts_p626 + " TEXT," +
                    layouts_p627 + " TEXT," +
                    layouts_p628 + " TEXT," +
                    layouts_p629 + " TEXT," +
                    layouts_p630 + " TEXT," +
                    layouts_p701 + " TEXT," +
                    layouts_p702 + " TEXT," +
                    layouts_p703 + " TEXT," +
                    layouts_p704 + " TEXT," +
                    layouts_p705 + " TEXT," +
                    layouts_p706 + " TEXT," +
                    layouts_p707 + " TEXT," +
                    layouts_p708 + " TEXT," +
                    layouts_p709 + " TEXT," +
                    layouts_p801 + " TEXT," +
                    layouts_p802 + " TEXT," +
                    layouts_p803 + " TEXT," +
                    layouts_p804 + " TEXT," +
                    layouts_p805 + " TEXT," +
                    layouts_p806 + " TEXT," +
                    layouts_p807 + " TEXT," +
                    layouts_p808 + " TEXT," +
                    layouts_p809 + " TEXT," +
                    layouts_p810 + " TEXT," +
                    layouts_p811 + " TEXT," +
                    layouts_p812 + " TEXT," +
                    layouts_p813 + " TEXT," +
                    layouts_p814 + " TEXT," +
                    layouts_p815 + " TEXT," +
                    layouts_p816 + " TEXT," +
                    layouts_p817 + " TEXT," +
                    layouts_p818 + " TEXT," +
                    layouts_p819 + " TEXT," +
                    layouts_p820 + " TEXT," +
                    layouts_p821 + " TEXT," +
                    layouts_p822 + " TEXT," +
                    layouts_p823  +  " TEXT" + ");"
            ;

    /**
     * CLAUSULAS WHERE
     * */
    public static final String WHERE_CLAUSE_ID = "_id=?";
    public static final String WHERE_CLAUSE_NUMERO = "numero=?";
    public static final String WHERE_CLAUSE_ID_ENCUESTADO = "id_encuestado=?";
    public static final String WHERE_CLAUSE_VIVIENDA_ID = "id_vivienda=?";
    public static final String WHERE_CLAUSE_HOGAR_ID = "id_hogar=?";
    public static final String WHERE_CLAUSE_ANIO = "anio=?";
    public static final String WHERE_CLAUSE_MES = "mes=?";
    public static final String WHERE_CLAUSE_PERIODO = "periodo=?";
    public static final String WHERE_CLAUSE_ZONA = "zona=?";
    public static final String WHERE_CLAUSE_USUARIO_ID = "usuario_id=?";
    public static final String WHERE_CLAUSE_USUARIO_SUP_ID = "usuario_sup_id=?";
    public static final String WHERE_CLAUSE_USUARIO = "usuario=?";
    public static final String WHERE_CLAUSE_ESTADO_COD = municipios_cod_estado+"=?";
    public static final String WHERE_CLAUSE_DESCRIPCION = ubigeo_descripcion+"=?";
    public static final String WHERE_CLAUSE_EXISTE_MENOR = "id_hogar=? and c2_p205_a<18";


    /*VARIABLES Y TABLAS DE TRABAJO FORZOSO*/




//    public static final String[] COLUMNAS_NACIONAL = {
//            nacional_codigo,nacional_apepat,nacional_aplicacion,
//            nacional_aula, nacional_discapacidad, nacional_sede
//    };
}
