package com.example.ricindigus.trabajo2020.fragments.modulo1;


import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.ricindigus.trabajo2020.R;
import com.example.ricindigus.trabajo2020.modelo.Data;
import com.example.ricindigus.trabajo2020.modelo.SQLConstantes;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo1H;
import com.example.ricindigus.trabajo2020.modelo.pojos.Modulo1V;
import com.example.ricindigus.trabajo2020.util.FragmentPagina;
import com.example.ricindigus.trabajo2020.util.InputFilterSoloLetras;
import com.example.ricindigus.trabajo2020.util.NumericKeyBoardTransformationMethod;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentP108P113 extends FragmentPagina {

    String idHogar;
    String idVivienda;
    Context context;

    /*
      RadioGroup c1_p108_RadioGroup, c1_p109_RadioGroup, c1_p110_RadioGroup, c1_p111_RadioGroup, c1_p112_RadioGroup;
    RadioGroup c1_p113_1_RadioGroup, c1_p113_2_RadioGroup, c1_p113_3_RadioGroup, c1_p113_4_RadioGroup, c1_p113_5_RadioGroup,
            c1_p113_6_RadioGroup, c1_p113_7_RadioGroup, c1_p113_8_RadioGroup, c1_p113_9_RadioGroup;
    EditText c1_p108_o_EditText, c1_p109_o_EditText, c1_p110_o_EditText, c1_p111_o_EditText, c1_p112_o_EditText;
    EditText c1_p113_7o_EditText, c1_p113_8o_EditText, c1_p113_9o_EditText;

    LinearLayout m1_p108_linearlayout, m1_p109_linearlayout, m1_p110_linearlayout,
            m1_p111_linearlayout, m1_p112_linearlayout, m1_p113_linearlayout;


     */
    LinearLayout layout_a_p007,layout_a_p008,layout_a_p009_p010;
    LinearLayout layoutpre101,layoutpre102,layoutpre103,layoutpre104,layoutpre105,layoutpre106,layoutpre107,layoutpre108,layoutpre109,layoutpre110;

    //7
    RadioGroup radiogroup_a_P007;

    //8
    CheckBox checkbox_a_P008_1,checkbox_a_P008_2,checkbox_a_P008_3,checkbox_a_P008_4,checkbox_a_P008_5,checkbox_a_P008_6,checkbox_a_P008_7,checkbox_a_P008_8,checkbox_a_P008_9,checkbox_a_P008_10,checkbox_a_P008_11;

    CheckBox checkbox_a_P008_12;
    EditText seccion_a_p008_sp11_O;

    //9
    RadioGroup radiogroup_a_P009_1, radiogroup_a_P009_2,radiogroup_a_P009_3,radiogroup_a_P009_4,radiogroup_a_P009_5,radiogroup_a_P009_6,radiogroup_a_P009_7,radiogroup_a_P009_8,radiogroup_a_P009_9;
    EditText edittext_a_P009_09_O;

    RadioGroup radiogroup_a_P009_10;
    EditText edittext_a_P009_10_O;

    //10
    RadioGroup radiogroup_a_P010_1;
    RadioGroup radiogroup_a_P010_2;
    RadioGroup radiogroup_a_P010_3;
    RadioGroup radiogroup_a_P010_4;
    RadioGroup radiogroup_a_P010_5;
    RadioGroup radiogroup_a_P010_6;
    RadioGroup radiogroup_a_P010_7;
    RadioGroup radiogroup_a_P010_8;
    RadioGroup radiogroup_a_P010_9;
    RadioGroup radiogroup_a_P010_10;



    private int c1_p101=0;
    /*
    private int c1_p108;
    private String c1_p108_o;
    private int c1_p109;
    private String c1_p109_o;
    private int c1_p110;
    private String c1_p110_o;
    private int c1_p111;
    private String c1_p111_o;
    private int c1_p112;
    private String c1_p112_o;
    private int c1_p113_1;
    private int c1_p113_2;
    private int c1_p113_3;
    private int c1_p113_4;
    private int c1_p113_5;
    private int c1_p113_6;
    private int c1_p113_7;
    private int c1_p113_8;
    private int c1_p113_9;
    private String c1_p113_7_o;
    private String c1_p113_8_o;
    private String c1_p113_9_o;
     */


    //nuevos
    private int p107;
    private String p108_1;
    private String p108_2;
    private String p108_3;
    private String p108_4;
    private String p108_5;
    private String p108_6;
    private String p108_7;
    private String  p108_8;
    private String  p108_9;
    private String  p108_10;
    private String  p108_11;
    private String  p108_12;
    private String p108_12_o;
    private int  p109_1;
    private int  p110_1;
    private int  p109_2;
    private int  p110_2;
    private int  p109_3;
    private int  p110_3;
    private int   p109_4;
    private int  p110_4;
    private int  p109_5;
    private int  p110_5;
    private int p109_6;
    private int  p110_6;
    private int  p109_7;
    private int  p110_7;
    private int  p109_8;
    private int p110_8;
    private int p109_9;
    private String p109_9_o;
    private int  p110_9;
    private int p109_10;
    private String p109_10_o;
    private int  p110_10;
    private int obs_a;

    public FragmentP108P113() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public FragmentP108P113(String idHogar, String idVivienda, Context context) {
        this.idHogar = idHogar;
        this.idVivienda = idVivienda;

        Data data = new Data(context);
        data.open();
        Modulo1V modulo1V = data.getModulo1V(idVivienda);
       // if(!modulo1V.getC1_p101().equals("")) c1_p101 = Integer.parseInt(modulo1V.getC1_p101());

        data.close();
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_p108_p113, container, false);
       /*
       c1_p108_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_108_radiogroup_C1_P108);
        c1_p108_o_EditText = (EditText) rootView.findViewById(R.id.mod1_108_edittext_C1_P108_O);
        c1_p109_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_109_radiogroup_C1_P109);
        c1_p109_o_EditText = (EditText) rootView.findViewById(R.id.mod1_109_edittext_C1_P109_O);
        c1_p110_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_110_radiogroup_C1_P110);
        c1_p110_o_EditText = (EditText) rootView.findViewById(R.id.mod1_110_edittext_C1_P110_O);
        c1_p111_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_111_radiogroup_C1_P111);
        c1_p111_o_EditText = (EditText) rootView.findViewById(R.id.mod1_111_edittext_C1_P111_O);
        c1_p112_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_112_radiogroup_C1_P112);
        c1_p112_o_EditText = (EditText) rootView.findViewById(R.id.mod1_112_edittext_C1_P112_O);

        c1_p113_1_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_1);
        c1_p113_2_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_2);
        c1_p113_3_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_3);
        c1_p113_4_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_4);
        c1_p113_5_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_5);
        c1_p113_6_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_6);
        c1_p113_7_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_7);
        c1_p113_8_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_8);
        c1_p113_9_RadioGroup = (RadioGroup) rootView.findViewById(R.id.mod1_113_radiogroup_C1_P113_9);
        c1_p113_7o_EditText = (EditText) rootView.findViewById(R.id.mod1_113_edittext_C1_P113_7O);
        c1_p113_8o_EditText = (EditText) rootView.findViewById(R.id.mod1_113_edittext_C1_P113_8O);
        c1_p113_9o_EditText = (EditText) rootView.findViewById(R.id.mod1_113_edittext_C1_P113_9O);

        m1_p108_linearlayout = (LinearLayout) rootView.findViewById(R.id.layout_m1_p108);
        m1_p109_linearlayout = (LinearLayout) rootView.findViewById(R.id.layout_m1_p109);
        m1_p110_linearlayout = (LinearLayout) rootView.findViewById(R.id.layout_m1_p110);
        m1_p111_linearlayout = (LinearLayout) rootView.findViewById(R.id.layout_m1_p111);
        m1_p112_linearlayout = (LinearLayout) rootView.findViewById(R.id.layout_m1_p112);
        m1_p113_linearlayout = (LinearLayout) rootView.findViewById(R.id.layout_m1_p113);
        */



        //Layaut
        layout_a_p007= (LinearLayout) rootView.findViewById(R.id.layout_a_p007);
        layout_a_p008= (LinearLayout) rootView.findViewById(R.id.layout_a_p008);
        layout_a_p009_p010= (LinearLayout) rootView.findViewById(R.id.layout_a_p009_p010);

        //7
        radiogroup_a_P007 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P007);

        //8
        checkbox_a_P008_1 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_1);
        checkbox_a_P008_2 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_2);
        checkbox_a_P008_3 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_3);
        checkbox_a_P008_4 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_4);
        checkbox_a_P008_5 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_5);
        checkbox_a_P008_6 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_6);
        checkbox_a_P008_7 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_7);
        checkbox_a_P008_8 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_8);
        checkbox_a_P008_9 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_9);
        checkbox_a_P008_10 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_10);
        checkbox_a_P008_11 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_11);
        checkbox_a_P008_12 = (CheckBox)rootView.findViewById(R.id.checkbox_a_P008_12);
        seccion_a_p008_sp11_O = (EditText) rootView.findViewById(R.id.seccion_a_p008_sp11_O);

        //9
        radiogroup_a_P009_1 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_1);
        radiogroup_a_P009_2 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_2);
        radiogroup_a_P009_3 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_3);
        radiogroup_a_P009_4 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_4);
        radiogroup_a_P009_5 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_5);
        radiogroup_a_P009_6 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_6);
        radiogroup_a_P009_7 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_7);
        radiogroup_a_P009_8 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_8);

        radiogroup_a_P009_9 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_9);
        edittext_a_P009_09_O = (EditText) rootView.findViewById(R.id.edittext_a_P009_09_O);

        radiogroup_a_P009_10 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P009_10);
        edittext_a_P009_10_O = (EditText) rootView.findViewById(R.id.edittext_a_P009_10_O);

        //10
        radiogroup_a_P010_1 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_1);
        radiogroup_a_P010_2 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_2);
        radiogroup_a_P010_3 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_3);
        radiogroup_a_P010_4 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_4);
        radiogroup_a_P010_5 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_5);
        radiogroup_a_P010_6 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_6);
        radiogroup_a_P010_7 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_7);
        radiogroup_a_P010_8 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_8);
        radiogroup_a_P010_9 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_9);
        radiogroup_a_P010_10 = (RadioGroup) rootView.findViewById(R.id.radiogroup_a_P010_10);





        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //NUEVAS PREGUNTAS

        controlarChecked(checkbox_a_P008_12,seccion_a_p008_sp11_O);

        radiogroup_a_P009_9.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,1,edittext_a_P009_09_O);
            }
        });

        radiogroup_a_P009_10.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,1,edittext_a_P009_10_O);
            }
        });


        //FIN DE NUEVAS PREGUNTAS
/*
 c1_p108_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,6,c1_p108_o_EditText);
            }
        });
        c1_p109_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,5,c1_p109_o_EditText);
            }
        });
        c1_p110_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,8,c1_p110_o_EditText);
            }
        });
        c1_p111_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,4,c1_p111_o_EditText);
            }
        });
        c1_p112_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,8,c1_p112_o_EditText);
            }
        });
        c1_p113_7_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,1,c1_p113_7o_EditText);
            }
        });
        c1_p113_8_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,1,c1_p113_8o_EditText);
            }
        });
        c1_p113_9_RadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                controlarEspecifiqueRadio(group, checkedId,1,c1_p113_9o_EditText);
            }
        });

        configurarEditText(c1_p108_o_EditText,m1_p108_linearlayout,0,30);
        configurarEditText(c1_p109_o_EditText,m1_p109_linearlayout,0,30);
        configurarEditText(c1_p110_o_EditText,m1_p110_linearlayout,0,30);
        configurarEditText(c1_p111_o_EditText,m1_p111_linearlayout,0,30);
        configurarEditText(c1_p112_o_EditText,m1_p112_linearlayout,0,30);
        configurarEditText(c1_p113_7o_EditText,m1_p113_linearlayout,0,30);
        configurarEditText(c1_p113_8o_EditText,m1_p113_linearlayout,0,30);
        configurarEditText(c1_p113_9o_EditText,m1_p113_linearlayout,0,30);

 */

        cargarDatos();
    }

    @Override
    public void guardarDatos() {
        Data data = new Data(context);
        data.open();
        ContentValues contentValues = new ContentValues();
        // contentValues.put(SQLConstantes.modulo1_h_c1_p108, c1_p108 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p108_o, c1_p108_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p109, c1_p109 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p109_o, c1_p109_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p110, c1_p110 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p110_o, c1_p110_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p111, c1_p111 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p111_o, c1_p111_o);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p112, c1_p112 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p112_o, c1_p112_o);
        // contentValues.put(SQLConstantes.modulo1_h_c1_p113_1, c1_p113_1 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_2, c1_p113_2 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_3, c1_p113_3 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_4, c1_p113_4 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_5, c1_p113_5 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_6, c1_p113_6 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_7, c1_p113_7 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_8, c1_p113_8 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_9, c1_p113_9 + "");
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_7_o, c1_p113_7_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_8_o, c1_p113_8_o);
        //contentValues.put(SQLConstantes.modulo1_h_c1_p113_9_o, c1_p113_9_o);
        contentValues.put(SQLConstantes.modulo1_h_COB100B, "1");
        contentValues.put(SQLConstantes.seccion_a_p107, p107);
        contentValues.put(SQLConstantes.seccion_a_p108_1, p108_1);
        contentValues.put(SQLConstantes.seccion_a_p108_2, p108_2);
        contentValues.put(SQLConstantes.seccion_a_p108_3, p108_3);
        contentValues.put(SQLConstantes.seccion_a_p108_4, p108_4);
        contentValues.put(SQLConstantes.seccion_a_p108_5, p108_5);
        contentValues.put(SQLConstantes.seccion_a_p108_6, p108_6);
        contentValues.put(SQLConstantes.seccion_a_p108_7, p108_7);
        contentValues.put(SQLConstantes.seccion_a_p108_8, p108_8);
        contentValues.put(SQLConstantes.seccion_a_p108_9, p108_9);
        contentValues.put(SQLConstantes.seccion_a_p108_10, p108_10);
        contentValues.put(SQLConstantes.seccion_a_p108_11, p108_11);
        contentValues.put(SQLConstantes.seccion_a_p108_12, p108_12);
        contentValues.put(SQLConstantes.seccion_a_p108_12_o, p108_12_o);
        contentValues.put(SQLConstantes.seccion_a_p109_1, p109_1);
        contentValues.put(SQLConstantes.seccion_a_p109_2, p109_2);
        contentValues.put(SQLConstantes.seccion_a_p109_3, p109_3);
        contentValues.put(SQLConstantes.seccion_a_p109_4, p109_4);
        contentValues.put(SQLConstantes.seccion_a_p109_5, p109_5);
        contentValues.put(SQLConstantes.seccion_a_p109_6, p109_6);
        contentValues.put(SQLConstantes.seccion_a_p109_7, p109_7);
        contentValues.put(SQLConstantes.seccion_a_p109_8, p109_8);
        contentValues.put(SQLConstantes.seccion_a_p109_9, p109_9);
        contentValues.put(SQLConstantes.seccion_a_p109_9_o,p109_9_o);
        contentValues.put(SQLConstantes.seccion_a_p109_10,p109_10);
        contentValues.put(SQLConstantes.seccion_a_p109_10_o,p109_10_o);
        contentValues.put(SQLConstantes.seccion_a_p110_1,p110_1);
        contentValues.put(SQLConstantes.seccion_a_p110_2,p110_2);
        contentValues.put(SQLConstantes.seccion_a_p110_3,p110_3);
        contentValues.put(SQLConstantes.seccion_a_p110_4,p110_4);
        contentValues.put(SQLConstantes.seccion_a_p110_5,p110_5);
        contentValues.put(SQLConstantes.seccion_a_p110_6,p110_6);
        contentValues.put(SQLConstantes.seccion_a_p110_7,p110_7);
        contentValues.put(SQLConstantes.seccion_a_p110_8,p110_8);
        contentValues.put(SQLConstantes.seccion_a_p110_9,p110_9);
        contentValues.put(SQLConstantes.seccion_a_p110_10,p110_10);

        if(!data.existeElemento(getNombreTabla(),idHogar)){
            Modulo1H modulo1H = new Modulo1H();
            modulo1H.set_id(idHogar);
            modulo1H.setIdVivienda(idVivienda);
            data.insertarElemento(getNombreTabla(), modulo1H.toValues());
        }
        data.actualizarElemento(getNombreTabla(),contentValues,idHogar);
        data.close();
    }

    @Override
    public void llenarVariables() {

        //Nuevas preguntas

        p107 = radiogroup_a_P007.indexOfChild(radiogroup_a_P007.findViewById(radiogroup_a_P007.getCheckedRadioButtonId()));

        if(checkbox_a_P008_1.isChecked()) p108_1 = "1"; else p108_1 = "0";
        if(checkbox_a_P008_2.isChecked()) p108_2 = "1"; else p108_2 = "0";
        if(checkbox_a_P008_3.isChecked()) p108_3 = "1"; else p108_3 = "0";
        if(checkbox_a_P008_4.isChecked()) p108_4 = "1"; else p108_4 = "0";
        if(checkbox_a_P008_5.isChecked()) p108_5 = "1"; else p108_5 = "0";
        if(checkbox_a_P008_6.isChecked()) p108_6 = "1"; else p108_6 = "0";
        if(checkbox_a_P008_7.isChecked()) p108_7 = "1"; else p108_7 = "0";
        if(checkbox_a_P008_8.isChecked()) p108_8 = "1"; else p108_8 = "0";
        if(checkbox_a_P008_9.isChecked()) p108_9 = "1"; else p108_9 = "0";
        if(checkbox_a_P008_10.isChecked()) p108_10 = "1"; else p108_10 = "0";
        if(checkbox_a_P008_11.isChecked()) p108_11 = "1"; else p108_11 = "0";

        if(checkbox_a_P008_12.isChecked()) p108_12 = "1"; else p108_12 = "0";
        p108_12_o= seccion_a_p008_sp11_O.getText().toString();


        p109_1 = radiogroup_a_P009_1.indexOfChild(radiogroup_a_P009_1.findViewById(radiogroup_a_P009_1.getCheckedRadioButtonId()));
        p109_2 = radiogroup_a_P009_2.indexOfChild(radiogroup_a_P009_2.findViewById(radiogroup_a_P009_2.getCheckedRadioButtonId()));
        p109_3 = radiogroup_a_P009_3.indexOfChild(radiogroup_a_P009_3.findViewById(radiogroup_a_P009_3.getCheckedRadioButtonId()));
        p109_4 = radiogroup_a_P009_4.indexOfChild(radiogroup_a_P009_4.findViewById(radiogroup_a_P009_4.getCheckedRadioButtonId()));
        p109_5 = radiogroup_a_P009_5.indexOfChild(radiogroup_a_P009_5.findViewById(radiogroup_a_P009_5.getCheckedRadioButtonId()));
        p109_6 = radiogroup_a_P009_6.indexOfChild(radiogroup_a_P009_6.findViewById(radiogroup_a_P009_6.getCheckedRadioButtonId()));
        p109_7 = radiogroup_a_P009_7.indexOfChild(radiogroup_a_P009_7.findViewById(radiogroup_a_P009_7.getCheckedRadioButtonId()));
        p109_8 = radiogroup_a_P009_8.indexOfChild(radiogroup_a_P009_8.findViewById(radiogroup_a_P009_8.getCheckedRadioButtonId()));
        p109_9 = radiogroup_a_P009_9.indexOfChild(radiogroup_a_P009_9.findViewById(radiogroup_a_P009_9.getCheckedRadioButtonId()));
        p109_9_o = edittext_a_P009_09_O.getText().toString();
        p109_10 = radiogroup_a_P009_10.indexOfChild(radiogroup_a_P009_10.findViewById(radiogroup_a_P009_10.getCheckedRadioButtonId()));
        p109_10_o = edittext_a_P009_10_O.getText().toString();

        p110_1 = radiogroup_a_P010_1.indexOfChild(radiogroup_a_P010_1.findViewById(radiogroup_a_P010_1.getCheckedRadioButtonId()));
        p110_2 = radiogroup_a_P010_2.indexOfChild(radiogroup_a_P010_2.findViewById(radiogroup_a_P010_2.getCheckedRadioButtonId()));
        p110_3 = radiogroup_a_P010_3.indexOfChild(radiogroup_a_P010_3.findViewById(radiogroup_a_P010_3.getCheckedRadioButtonId()));
        p110_4 = radiogroup_a_P010_4.indexOfChild(radiogroup_a_P010_4.findViewById(radiogroup_a_P010_4.getCheckedRadioButtonId()));
        p110_5 = radiogroup_a_P010_5.indexOfChild(radiogroup_a_P010_5.findViewById(radiogroup_a_P010_5.getCheckedRadioButtonId()));
        p110_6 = radiogroup_a_P010_6.indexOfChild(radiogroup_a_P010_6.findViewById(radiogroup_a_P010_6.getCheckedRadioButtonId()));
        p110_7 = radiogroup_a_P010_7.indexOfChild(radiogroup_a_P010_7.findViewById(radiogroup_a_P010_7.getCheckedRadioButtonId()));
        p110_8 = radiogroup_a_P010_8.indexOfChild(radiogroup_a_P010_8.findViewById(radiogroup_a_P010_8.getCheckedRadioButtonId()));
        p110_9 = radiogroup_a_P010_9.indexOfChild(radiogroup_a_P010_9.findViewById(radiogroup_a_P010_9.getCheckedRadioButtonId()));
        p110_10 = radiogroup_a_P010_10.indexOfChild(radiogroup_a_P010_10.findViewById(radiogroup_a_P010_10.getCheckedRadioButtonId()));


        //Fin de nuevas preguntas

        /*
        c1_p108 = c1_p108_RadioGroup.indexOfChild(c1_p108_RadioGroup.findViewById(c1_p108_RadioGroup.getCheckedRadioButtonId()));
        c1_p108_o = c1_p108_o_EditText.getText().toString();
        c1_p109 = c1_p109_RadioGroup.indexOfChild(c1_p109_RadioGroup.findViewById(c1_p109_RadioGroup.getCheckedRadioButtonId()));
        c1_p109_o = c1_p109_o_EditText.getText().toString();
        c1_p110 = c1_p110_RadioGroup.indexOfChild(c1_p110_RadioGroup.findViewById(c1_p110_RadioGroup.getCheckedRadioButtonId()));
        c1_p110_o = c1_p110_o_EditText.getText().toString();
        c1_p111 = c1_p111_RadioGroup.indexOfChild(c1_p111_RadioGroup.findViewById(c1_p111_RadioGroup.getCheckedRadioButtonId()));
        c1_p111_o = c1_p111_o_EditText.getText().toString();
        c1_p112 = c1_p112_RadioGroup.indexOfChild(c1_p112_RadioGroup.findViewById(c1_p112_RadioGroup.getCheckedRadioButtonId()));
        c1_p112_o = c1_p112_o_EditText.getText().toString();

        c1_p113_1 = c1_p113_1_RadioGroup.indexOfChild(c1_p113_1_RadioGroup.findViewById(c1_p113_1_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_2 = c1_p113_2_RadioGroup.indexOfChild(c1_p113_2_RadioGroup.findViewById(c1_p113_2_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_3 = c1_p113_3_RadioGroup.indexOfChild(c1_p113_3_RadioGroup.findViewById(c1_p113_3_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_4 = c1_p113_4_RadioGroup.indexOfChild(c1_p113_4_RadioGroup.findViewById(c1_p113_4_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_5 = c1_p113_5_RadioGroup.indexOfChild(c1_p113_5_RadioGroup.findViewById(c1_p113_5_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_6 = c1_p113_6_RadioGroup.indexOfChild(c1_p113_6_RadioGroup.findViewById(c1_p113_6_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_7 = c1_p113_7_RadioGroup.indexOfChild(c1_p113_7_RadioGroup.findViewById(c1_p113_7_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_8 = c1_p113_8_RadioGroup.indexOfChild(c1_p113_8_RadioGroup.findViewById(c1_p113_8_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_9 = c1_p113_9_RadioGroup.indexOfChild(c1_p113_9_RadioGroup.findViewById(c1_p113_9_RadioGroup.getCheckedRadioButtonId()));
        c1_p113_7_o = c1_p113_7o_EditText.getText().toString();
        c1_p113_8_o = c1_p113_8o_EditText.getText().toString();
        c1_p113_9_o = c1_p113_9o_EditText.getText().toString();
         */

    }

    @Override
    public void cargarDatos() {
        Data data = new Data(context);
        data.open();
        if (data.existeElemento(getNombreTabla(),idHogar)){
            Modulo1H modulo1H = data.getModulo1H(idHogar);

            //Nuevas Preguntas

            if (!modulo1H.getP107().equals("-1"))
                ((RadioButton) radiogroup_a_P007.getChildAt(Integer.parseInt(modulo1H.getP107()))).setChecked(true);

            if(modulo1H.getP108_1().equals("1")) checkbox_a_P008_1.setChecked(true);
            if(modulo1H.getP108_1().equals("0")) checkbox_a_P008_1.setChecked(false);
            if(modulo1H.getP108_2().equals("1")) checkbox_a_P008_2.setChecked(true);
            if(modulo1H.getP108_2().equals("0")) checkbox_a_P008_2.setChecked(false);
            if(modulo1H.getP108_3().equals("1")) checkbox_a_P008_3.setChecked(true);
            if(modulo1H.getP108_3().equals("0")) checkbox_a_P008_3.setChecked(false);
            if(modulo1H.getP108_4().equals("1")) checkbox_a_P008_4.setChecked(true);
            if(modulo1H.getP108_4().equals("0")) checkbox_a_P008_4.setChecked(false);
            if(modulo1H.getP108_5().equals("1")) checkbox_a_P008_5.setChecked(true);
            if(modulo1H.getP108_5().equals("0")) checkbox_a_P008_5.setChecked(false);
            if(modulo1H.getP108_6().equals("1")) checkbox_a_P008_6.setChecked(true);
            if(modulo1H.getP108_6().equals("0")) checkbox_a_P008_6.setChecked(false);
            if(modulo1H.getP108_7().equals("1")) checkbox_a_P008_7.setChecked(true);
            if(modulo1H.getP108_7().equals("0")) checkbox_a_P008_7.setChecked(false);
            if(modulo1H.getP108_8().equals("1")) checkbox_a_P008_8.setChecked(true);
            if(modulo1H.getP108_8().equals("0")) checkbox_a_P008_8.setChecked(false);
            if(modulo1H.getP108_9().equals("1")) checkbox_a_P008_9.setChecked(true);
            if(modulo1H.getP108_9().equals("0")) checkbox_a_P008_9.setChecked(false);
            if(modulo1H.getP108_10().equals("1")) checkbox_a_P008_10.setChecked(true);
            if(modulo1H.getP108_10().equals("0")) checkbox_a_P008_10.setChecked(false);
            if(modulo1H.getP108_11().equals("1")) checkbox_a_P008_11.setChecked(true);
            if(modulo1H.getP108_11().equals("0")) checkbox_a_P008_11.setChecked(false);
            if(modulo1H.getP108_12().equals("1")) checkbox_a_P008_12.setChecked(true);
            if(modulo1H.getP108_12().equals("0")) checkbox_a_P008_12.setChecked(false);
            seccion_a_p008_sp11_O.setText(modulo1H.getP108_12_o());

            if (!modulo1H.getP109_1().equals("-1"))
                ((RadioButton) radiogroup_a_P009_1.getChildAt(Integer.parseInt(modulo1H.getP109_1()))).setChecked(true);

            if (!modulo1H.getP109_2().equals("-1"))
                ((RadioButton) radiogroup_a_P009_2.getChildAt(Integer.parseInt(modulo1H.getP109_2()))).setChecked(true);

            if (!modulo1H.getP109_3().equals("-1"))
                ((RadioButton) radiogroup_a_P009_3.getChildAt(Integer.parseInt(modulo1H.getP109_3()))).setChecked(true);

            if (!modulo1H.getP109_4().equals("-1"))
                ((RadioButton) radiogroup_a_P009_4.getChildAt(Integer.parseInt(modulo1H.getP109_4()))).setChecked(true);

            if (!modulo1H.getP109_5().equals("-1"))
                ((RadioButton) radiogroup_a_P009_5.getChildAt(Integer.parseInt(modulo1H.getP109_5()))).setChecked(true);

            if (!modulo1H.getP109_6().equals("-1"))
                ((RadioButton) radiogroup_a_P009_6.getChildAt(Integer.parseInt(modulo1H.getP109_6()))).setChecked(true);

            if (!modulo1H.getP109_7().equals("-1"))
                ((RadioButton) radiogroup_a_P009_7.getChildAt(Integer.parseInt(modulo1H.getP109_7()))).setChecked(true);

            if (!modulo1H.getP109_8().equals("-1"))
                ((RadioButton) radiogroup_a_P009_8.getChildAt(Integer.parseInt(modulo1H.getP109_8()))).setChecked(true);

            if (!modulo1H.getP109_9().equals("-1"))
                ((RadioButton) radiogroup_a_P009_9.getChildAt(Integer.parseInt(modulo1H.getP109_9()))).setChecked(true);
            edittext_a_P009_09_O.setText(modulo1H.getP109_9_o());

            if (!modulo1H.getP109_10().equals("-1"))
                ((RadioButton) radiogroup_a_P009_10.getChildAt(Integer.parseInt(modulo1H.getP109_10()))).setChecked(true);
            edittext_a_P009_10_O.setText(modulo1H.getP109_10_o());

            if (!modulo1H.getP110_1().equals("-1"))
                ((RadioButton) radiogroup_a_P010_1.getChildAt(Integer.parseInt(modulo1H.getP110_1()))).setChecked(true);

            if (!modulo1H.getP110_2().equals("-1"))
                ((RadioButton) radiogroup_a_P010_2.getChildAt(Integer.parseInt(modulo1H.getP110_2()))).setChecked(true);

            if (!modulo1H.getP110_3().equals("-1"))
                ((RadioButton) radiogroup_a_P010_3.getChildAt(Integer.parseInt(modulo1H.getP110_3()))).setChecked(true);

            if (!modulo1H.getP110_4().equals("-1"))
                ((RadioButton) radiogroup_a_P010_4.getChildAt(Integer.parseInt(modulo1H.getP110_4()))).setChecked(true);

            if (!modulo1H.getP110_5().equals("-1"))
                ((RadioButton) radiogroup_a_P010_5.getChildAt(Integer.parseInt(modulo1H.getP110_5()))).setChecked(true);

            if (!modulo1H.getP110_6().equals("-1"))
                ((RadioButton) radiogroup_a_P010_6.getChildAt(Integer.parseInt(modulo1H.getP110_6()))).setChecked(true);

            if (!modulo1H.getP110_7().equals("-1"))
                ((RadioButton) radiogroup_a_P010_7.getChildAt(Integer.parseInt(modulo1H.getP110_7()))).setChecked(true);

            if (!modulo1H.getP110_8().equals("-1"))
                ((RadioButton) radiogroup_a_P010_8.getChildAt(Integer.parseInt(modulo1H.getP110_8()))).setChecked(true);

            if (!modulo1H.getP110_9().equals("-1"))
                ((RadioButton) radiogroup_a_P010_9.getChildAt(Integer.parseInt(modulo1H.getP110_9()))).setChecked(true);

            if (!modulo1H.getP110_10().equals("-1"))
                ((RadioButton) radiogroup_a_P010_10.getChildAt(Integer.parseInt(modulo1H.getP110_10()))).setChecked(true);



            //Fin de Nuevas Preguntas

            /*

             if(!modulo1H.getC1_p108().equals("-1") && !modulo1H.getC1_p108().equals(""))((RadioButton)c1_p108_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p108()))).setChecked(true);
            c1_p108_o_EditText.setText(modulo1H.getC1_p108_o());

            if(!modulo1H.getC1_p109().equals("-1") && !modulo1H.getC1_p109().equals(""))((RadioButton)c1_p109_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p109()))).setChecked(true);
            c1_p109_o_EditText.setText(modulo1H.getC1_p109_o());

            if(!modulo1H.getC1_p110().equals("-1") && !modulo1H.getC1_p110().equals(""))((RadioButton)c1_p110_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p110()))).setChecked(true);
            c1_p110_o_EditText.setText(modulo1H.getC1_p110_o());

            if(!modulo1H.getC1_p111().equals("-1") && !modulo1H.getC1_p111().equals(""))((RadioButton)c1_p111_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p111()))).setChecked(true);
            c1_p111_o_EditText.setText(modulo1H.getC1_p111_o());

            if(!modulo1H.getC1_p112().equals("-1") && !modulo1H.getC1_p112().equals(""))((RadioButton)c1_p112_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p112()))).setChecked(true);
            c1_p112_o_EditText.setText(modulo1H.getC1_p112_o());

            if(!modulo1H.getC1_p113_1().equals("-1") && !modulo1H.getC1_p113_1().equals(""))((RadioButton)c1_p113_1_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_1()))).setChecked(true);
            if(!modulo1H.getC1_p113_2().equals("-1") && !modulo1H.getC1_p113_2().equals(""))((RadioButton)c1_p113_2_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_2()))).setChecked(true);
            if(!modulo1H.getC1_p113_3().equals("-1") && !modulo1H.getC1_p113_3().equals(""))((RadioButton)c1_p113_3_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_3()))).setChecked(true);
            if(!modulo1H.getC1_p113_4().equals("-1") && !modulo1H.getC1_p113_4().equals(""))((RadioButton)c1_p113_4_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_4()))).setChecked(true);
            if(!modulo1H.getC1_p113_5().equals("-1") && !modulo1H.getC1_p113_5().equals(""))((RadioButton)c1_p113_5_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_5()))).setChecked(true);
            if(!modulo1H.getC1_p113_6().equals("-1") && !modulo1H.getC1_p113_6().equals(""))((RadioButton)c1_p113_6_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_6()))).setChecked(true);
            if(!modulo1H.getC1_p113_7().equals("-1") && !modulo1H.getC1_p113_7().equals(""))((RadioButton)c1_p113_7_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_7()))).setChecked(true);
            if(!modulo1H.getC1_p113_8().equals("-1") && !modulo1H.getC1_p113_8().equals(""))((RadioButton)c1_p113_8_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_8()))).setChecked(true);
            if(!modulo1H.getC1_p113_9().equals("-1") && !modulo1H.getC1_p113_9().equals(""))((RadioButton)c1_p113_9_RadioGroup.getChildAt(Integer.parseInt(modulo1H.getC1_p113_9()))).setChecked(true);

            c1_p113_7o_EditText.setText(modulo1H.getC1_p113_7_o());
            c1_p113_8o_EditText.setText(modulo1H.getC1_p113_8_o());
            c1_p113_9o_EditText.setText(modulo1H.getC1_p113_9_o());
             */

        }
        data.close();
    }

    @Override
    public void llenarVista() {

    }

    @Override
    public boolean validarDatos() {
        llenarVariables();

        //NUEVAS PREGUNTAS

        if (p107 == -1){mostrarMensaje("PREGUNTA 07: DEBE MARCAR UNA OPCIÓN"); return false;}


        if (p108_1.equals("0") && p108_2.equals("0") && p108_3.equals("0") && p108_4.equals("0") && p108_5.equals("0") && p108_6.equals("0")
                && p108_7.equals("0") && p108_8.equals("0") && p108_9.equals("0")
                && p108_10.equals("0") && p108_11.equals("0") && p108_12.equals("0")) {
            mostrarMensaje("PREGUNTA 08: DEBE SELECCIONAR ALGUNA OPCION");return false;
        }else{
            if (p108_12.equals("1")){
                if (p108_12_o.trim().equals("")){
                    mostrarMensaje("PREGUNTA 08: DEBE ESPECIFICAR");return false;
                }

            }
        }

        if (p109_1 == -1){mostrarMensaje("PREGUNTA 09_1: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p109_2 == -1){mostrarMensaje("PREGUNTA 09_2: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p109_3 == -1){mostrarMensaje("PREGUNTA 09_3: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p109_4 == -1){mostrarMensaje("PREGUNTA 09_4: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p109_5 == -1){mostrarMensaje("PREGUNTA 09_5: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p109_6 == -1){mostrarMensaje("PREGUNTA 09_6: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p109_7 == -1){mostrarMensaje("PREGUNTA 09_7: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p109_8 == -1){mostrarMensaje("PREGUNTA 09_8: DEBE MARCAR UNA OPCIÓN"); return false;}

        if (p109_9 == -1){mostrarMensaje("PREGUNTA 09_9: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (p109_9 == 1){
                if (p109_9_o.trim().equals("")){mostrarMensaje("PREGUNTA 09_9: DEBE ESPECIFICAR");return false;}
            }
        }

        if (p109_10 == -1){mostrarMensaje("PREGUNTA 09_10: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (p109_10 == 1){
                if (p109_10_o.trim().equals("")){mostrarMensaje("PREGUNTA 09_10: DEBE ESPECIFICAR");return false;}
            }
        }

        if (p110_1 == -1){mostrarMensaje("PREGUNTA 10_1: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_2 == -1){mostrarMensaje("PREGUNTA 10_2: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_3 == -1){mostrarMensaje("PREGUNTA 10_3: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_4 == -1){mostrarMensaje("PREGUNTA 10_4: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_5 == -1){mostrarMensaje("PREGUNTA 10_5: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_6 == -1){mostrarMensaje("PREGUNTA 10_6: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_7 == -1){mostrarMensaje("PREGUNTA 10_7: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_8 == -1){mostrarMensaje("PREGUNTA 10_8: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_9 == -1){mostrarMensaje("PREGUNTA 10_9: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (p110_10 == -1){mostrarMensaje("PREGUNTA 10_10: DEBE MARCAR UNA OPCIÓN"); return false;}



        //FIN DE NUEVAS PREGUNTAS

        /*
         if (c1_p108 == -1){mostrarMensaje("PREGUNTA 108: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p108 == 6){
                if (c1_p108_o.trim().equals("")){mostrarMensaje("PREGUNTA 108: DEBE ESPECIFICAR");return false;}
            }
        }

        if (c1_p109 == -1){mostrarMensaje("PREGUNTA 109: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p109 == 5){
                if (c1_p109_o.trim().equals("")){mostrarMensaje("PREGUNTA 109: DEBE ESPECIFICAR");return false;}
            }
        }

        if (c1_p110 == -1){mostrarMensaje("PREGUNTA 110: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p110 == 8){
                if (c1_p110_o.trim().equals("")){mostrarMensaje("PREGUNTA 110: DEBE ESPECIFICAR");return false;}
            }
            if(c1_p101==5 && !((c1_p110==4) || c1_p110==8)){
                mostrarMensaje("PREGUNTA 110: LA PROCEDENCIA DEL ABASTECIMIENTO DE AGUA EN SU HOGAR NO CONCUERDA CON TIPO DE VIVIENDA");
            }
        }

        if (c1_p111 == -1){mostrarMensaje("PREGUNTA 111: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p111 == 4){
                if (c1_p111_o.trim().equals("")){mostrarMensaje("PREGUNTA 111: DEBE ESPECIFICAR");return false;}
            }
        }

        if (c1_p112 == -1){mostrarMensaje("PREGUNTA 112: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p112 == 8){
                if (c1_p112_o.trim().equals("")){mostrarMensaje("PREGUNTA 112: DEBE ESPECIFICAR");return false;}
            }
        }

        if (c1_p113_1 == -1){mostrarMensaje("PREGUNTA 113-1: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (c1_p113_2 == -1){mostrarMensaje("PREGUNTA 113-2: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (c1_p113_3 == -1){mostrarMensaje("PREGUNTA 113-3: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (c1_p113_4 == -1){mostrarMensaje("PREGUNTA 113-4: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (c1_p113_5 == -1){mostrarMensaje("PREGUNTA 113-5: DEBE MARCAR UNA OPCIÓN"); return false;}
        if (c1_p113_6 == -1){mostrarMensaje("PREGUNTA 113-6: DEBE MARCAR UNA OPCIÓN"); return false;}

        if (c1_p113_7 == -1){mostrarMensaje("PREGUNTA 113-7: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p113_7 == 1){
                if (c1_p113_7_o.trim().equals("")){mostrarMensaje("PREGUNTA 113-7: DEBE ESPECIFICAR");return false;}
            }
        }
        if (c1_p113_8 == -1){mostrarMensaje("PREGUNTA 113-8: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p113_8 == 1){
                if (c1_p113_8_o.trim().equals("")){mostrarMensaje("PREGUNTA 113-8: DEBE ESPECIFICAR");return false;}
            }
        }
        if (c1_p113_9 == -1){mostrarMensaje("PREGUNTA 113-9: DEBE MARCAR UNA OPCIÓN"); return false;}
        else{
            if (c1_p113_9 == 1){
                if (c1_p113_9_o.trim().equals("")){mostrarMensaje("PREGUNTA 113-9: DEBE ESPECIFICAR");return false;}
            }
        }


         */


        return true;
    }

    @Override
    public String getNombreTabla() {
        return SQLConstantes.tablamodulo1h;
    }

    public void mostrarMensaje(String m){
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(m);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void controlarEspecifiqueRadio(RadioGroup group, int checkedId, int opcionEsp, EditText editTextEspecifique) {
        int seleccionado = group.indexOfChild(group.findViewById(checkedId));
        if(seleccionado == opcionEsp){
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_enabled);
            editTextEspecifique.setEnabled(true);
        }else{
            editTextEspecifique.setText("");
            editTextEspecifique.setBackgroundResource(R.drawable.input_text_disabled);
            editTextEspecifique.setEnabled(false);
        }
    }

    private void configurarEditText(final EditText editText, final View view, int tipo,int longitud){
        switch (tipo){
            case 0:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud), new InputFilterSoloLetras()});break;
            case 1:editText.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(longitud)});break;
            case 2:editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(longitud)});
                editText.setTransformationMethod(new NumericKeyBoardTransformationMethod());break;
        }

        editText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ocultarTeclado(editText);
                    view.requestFocus();
                    return true;
                }
                return false;
            }
        });
    }

    public void ocultarTeclado(View view){
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void mostrarTeclado(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void controlarChecked(CheckBox checkBox,final EditText editText){
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    editText.setBackgroundResource(R.drawable.input_text_enabled);
                    editText.setEnabled(true);
                }else{
                    editText.setText("");
                    editText.setBackgroundResource(R.drawable.input_text_disabled);
                    editText.setEnabled(false);
                }
            }
        });
    }
}
